package org.interinfo.model.jcvc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.db.*;
import jcx.lib.*;

public class JCVCPermission{
	Properties pConfig = new Properties();
	Vector vLog = new Vector();
	String configPath = "config/main.cfg";
	String id = "";
	String account = null;
	String password = null;
	String permissionPath = "";
	Boolean isAdded = false;
	String addAccount = null;
	String addPassword = null;

	public JCVCPermission setId(String id) throws Exception{
		this.id = id;
		return this;
	}

	public JCVCPermission setAccount(String account) throws Exception{
		this.account = account;
		return this;
	}

	public JCVCPermission setPassword(String password) throws Exception{
		this.password = password;
		return this;
	}

	public JCVCPermission setConfigPath(String configPath) throws Exception{
		this.configPath = configPath;
		return this;
	}

	public JCVCPermission setPermissionPath(String permissionPath) throws Exception{
		this.permissionPath = permissionPath;
		return this;
	}

	public boolean getIsAdded() throws Exception{
		return isAdded;
	}

	public String getAddAccount() throws Exception{
		return addAccount;
	}

	public String getAddPassword() throws Exception{
		return addPassword;
	}

	public boolean loadConfig() throws Exception{
		File f = new File(configPath);
		if(!f.isFile()){
			return false;
		}

		try{
			pConfig.load(new InputStreamReader(new FileInputStream(configPath) , "GBK"));
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean checkPermission() throws Exception{
		if(pConfig == null){
			return false;
		}

		String configAccount = (String)pConfig.get("interinfo.pam.jcvc.user");
		String configPassword = (String)pConfig.get("interinfo.pam.jcvc.password");

		if(configAccount == null || configPassword == null || !configAccount.equals(account) || !configPassword.equals(password)){
			return false;
		}
		return true;
	}

	public void addUserPath(String path) throws Exception{
		String userAccount = (String)pConfig.get("jcvc.user." + id);

		if(userAccount == null){
			application_Account();
		}

		String userPath = (String)pConfig.get("jcvc.user." + id + ".path");
		if(userPath == null){
			userPath = "";
		}

		boolean bContains = false;

		String[] arrUserPath = userPath.split(";");
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < arrUserPath.length; i++ ){
			String iPath = arrUserPath[i].trim();
			if(iPath.length() == 0){
				continue;
			}
			if(iPath.equals(path)){
				bContains = true;
			}
			sb.append(iPath);
			sb.append(";");
		}

		if(!bContains){
			sb.append(path);
			sb.append(";");
		}

		String value = sb.toString();
		pConfig.put("jcvc.user." + id +  ".path" , value);
	}

	public void application_Account() throws Exception{
		String password = getPassword(12);
		pConfig.put("jcvc.user." + id , password);
		addAccount = id;
		addPassword = password;
		isAdded = true;
	}

	public void delUserPath(String path) throws Exception{
		String userPath = (String)pConfig.get("jcvc.user." + id + ".path");
		if(userPath == null){
			userPath = "";
		}

		String[] arrUserPath = userPath.split(";");
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < arrUserPath.length; i++ ){
			String iPath = arrUserPath[i].trim();
			if(iPath.length() == 0 || iPath.equals(path)){
				continue;
			}
			sb.append(iPath);
			sb.append(";");
		}

		String value = sb.toString();

		if(value.length() > 0){
			pConfig.put("jcvc.user." + id +  ".path" , value);
		}else{
			pConfig.remove("jcvc.user." + id +  ".path");
		}
	}

	public boolean save() throws Exception{		
		boolean result = true;
		FileInputStream fileInputStream = new FileInputStream(configPath);
		InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		Vector vConfig = new Vector();
		String line = bufferedReader.readLine();
		while(line != null){
			if(line != null && !line.startsWith("jcvc.user")){
				vConfig.add(line);
			}
			line = bufferedReader.readLine();
		}

		Integer index = null;
		
		for(int i = 0 ; i < vConfig.size(); i++){
			String iLine = (String)vConfig.get(i);
	
			int iSep = iLine.indexOf("=");
			if(iSep != -1){
				String iKey = iLine.substring(0 , iSep);
				String iValue = iLine.substring(iSep + 1 , iLine.length());
				String iConfigValue = (String)pConfig.get(iKey);

				if((String)pConfig.get(iKey) == null){
					continue;
				}else{
					pConfig.remove(iKey);
					iLine = iKey + "=" + iConfigValue;
				}
			}
			if("##@@JCVC_USER_PERMISSION@@##".equals(iLine)){
				index = i + 1;
			}
		}
		if(index == null){
			vConfig.add("##@@JCVC_USER_PERMISSION@@##");
			index = vConfig.size();
		}

		Vector vUser = new Vector();
		Enumeration e = pConfig.propertyNames();
		while(e.hasMoreElements()){
			String key = (String)e.nextElement();
			String value = (String)pConfig.get(key);
			String[] arrKey = convert.separStr(key , ".");
			if(key.startsWith("jcvc.user") && arrKey.length == 3){
				
				String add_User = arrKey[2].trim();
				if(!vUser.contains(add_User)){
					vUser.add(add_User);
				}
			}
		}

		Collections.sort(vUser);
		Vector vUserData = new Vector();
		for(int i = 0 ; i < vUser.size(); i++ ){
			String iUser = (String)vUser.get(i);
			String iUserPermission = (String)pConfig.get("jcvc.user." + iUser);
			if(iUserPermission == null){
				continue;
			}else{
				vUserData.add("jcvc.user." + iUser + "=" + iUserPermission);
			}
			

			String iUserPath = (String)pConfig.get("jcvc.user." + iUser + ".path");
			if(iUserPath != null){
				vUserData.add("jcvc.user." + iUser + ".path" + "=" + iUserPath); 
			}
		}

		for(int i = 0 ; i < vUserData.size(); i++){
			String iData = (String)vUserData.get(i);
			vConfig.insertElementAt(iData , index + i); 
		}


		StringBuffer sb = new StringBuffer();

		for(int i = 0 ; i < vConfig.size(); i++ ){
			String iConfigData = (String)vConfig.get(i);
			sb.append(iConfigData);
			sb.append("\r\n");
		}
		FileOutputStream fileOutputStream  = new FileOutputStream(configPath);
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream)); 

		try{
			bufferedWriter.write(sb.toString());
			bufferedWriter.flush();				 // 把快取區內容壓入檔案
        }catch(Exception error){
			result = false;
			error.printStackTrace();
        }finally{
			bufferedWriter.close();
        }
		
		return result;
	}

	public String getPassword(int num_count){
		StringBuffer sbRule = new StringBuffer("0123456789abcdefg");
		StringBuffer sbPassword = new StringBuffer();
		Random rNUM = new Random();
		int ruleRange = sbRule.length();
		for(int i = 0; i < num_count; i++) {
			sbPassword.append(sbRule.charAt(rNUM.nextInt(ruleRange)));
		}
		return sbPassword.toString();
	}
}

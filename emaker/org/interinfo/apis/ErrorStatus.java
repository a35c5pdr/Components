package org.interinfo.apis;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonInclude;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorStatus extends APIStatus{
	private Exception exception;
	public ErrorStatus(String resultCode, String resultMessage){
		setResultMessage(resultMessage);
		setResultCode(resultCode);
	}
	public ErrorStatus(Exception exception){
		setResultMessage(exception.toString());
		setResultCode("-1");
		this.exception = exception;
	}
	public ErrorStatus(String resultCode , String resultMessage , Exception exception){
		setResultMessage(resultMessage);
		setResultCode(resultCode);
		this.exception = exception;
	}
}

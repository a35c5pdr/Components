package org.interinfo.apis;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.lang.reflect.Field;
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class APIStatus{
	private String resultMessage = "";
	private String resultCode = "";
}

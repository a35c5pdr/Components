package org.interinfo.apis;

import java.io.*;
import java.util.*;
import java.lang.reflect.Field;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class APIFormat implements Serializable{
	private String resultCode = "";
	private String resultMessage = "";

	
	public void setResult(String errorCode)throws Exception{
		Field field = this.getClass().getField("msg" + errorCode);
		String message = (String)field.get(this);
		setResultMessage(message);
	}
}

package org.interinfo.apis;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SuccessStatus extends APIStatus{
	public SuccessStatus(String resultCode , String resultMessage){
		setResultMessage(resultMessage);
		setResultCode(resultCode);
	}
}

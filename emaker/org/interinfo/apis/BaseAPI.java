package org.interinfo.apis;
import java.io.*;
import java.util.*;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.lang.reflect.Field;

import jcx.util.*;
import jcx.servlet.Servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
public abstract class BaseAPI extends Servlet {
	private Class requestBodyClass = null;
	private Class responseBodyClass = null;
	private Class requestHeaderClass = null;
	private Class responseHeaderClass = null;

	private RequestBody requestBody = null;
	private ResponseBody responseBody = null;
	private RequestHeader requestHeader = null;
	private ResponseHeader responseHeader = null;

	protected ObjectMapper objectMapper = null;

	private String normalResponse = "text/json";
	protected String xMethod = null;

	//	定義api servlet 邏輯執行動作與輸出結果
	public abstract void api_init() throws Exception;
	//	定義api servlet 邏輯執行動作與輸出結果
	public abstract void api_action() throws Exception;
	//	定義api servlet 
	public abstract boolean checkRight() throws Exception;
	//	定義api HTTP Method 
	public abstract String[] apiMethod() throws Exception;
	//	定義api package路徑
	public abstract String apiPackage() throws Exception;
	//	定義api 認證金鑰方式
	public abstract boolean checkAuthorization() throws Exception;
	//	定義api 認證HTTP Method
	public abstract boolean checkMethod() throws Exception;

	public RequestBody getRequestBody(){
		return this.requestBody;
	}

	public RequestHeader getRequestHeader(){
		return this.requestHeader;
	}

	public ResponseBody getResponseBody(){
		return this.responseBody;
	}

	public ResponseHeader getResponseHeader(){
		return this.responseHeader;
	}
	
	private SuccessStatus successStatus = null;
	public void setSuccessStatus(SuccessStatus successStatus){
		this.successStatus = successStatus;
	}
	private FailedStatus failedStatus = null;
	public void setFailStatus(FailedStatus failedStatus){
		this.failedStatus = failedStatus;
	}
	private ErrorStatus errorStatus = null;
	public void setErrorStatus(ErrorStatus errorStatus){
		this.errorStatus = errorStatus;
	}
	public void setException(Exception e){
		this.errorStatus = new ErrorStatus(e);
	}

	protected Hashtable htBody = null;
	
	public void run1()throws Exception{
		PrintStream out = getOutputStream();
		APIResponse apiResponse = new APIResponse();
		StringBuffer sbLog = new StringBuffer();

		try{
			out = new PrintStream(out , true , "utf8");
			api_init();

			String requestBodyJson = null;
			String requestHeaderJson = null;
			
			if(!checkAuthorization()){ // 驗證金鑰
				return;
			}

			if(!checkMethod()){ //驗證HTTP Method
				return;
			}

			// 獲取父類
			Class<?> parentClass = BaseAPI.class.getSuperclass();

			// 獲取父類參數
			Field[] fields = parentClass.getDeclaredFields();

			// 檢查父類參數
			boolean hasReq = false;
			boolean hasResp = false;
			for(int i = 0 ; i < fields.length ; i++){
				Field field = fields[i];
				if("req".equals(field.getName()) ) {
					hasReq = true;
				} else if("resp".equals(field.getName()) ) {
					hasResp = true;
				}
				if(hasReq && hasResp){
					break;
				}
			}
			sbLog.append("hasReq:" + hasReq).append("\r\n");
			sbLog.append("hasResp:" + hasResp).append("\r\n");

			//如果沒有 req 跟 resp 則自己做 http Response Header
			boolean bReqNull = false;
			if(hasReq && hasResp){
				if(req == null && resp == null){
					bReqNull = true;
				}
			} else {
				bReqNull = true;
			}
			if(bReqNull){
				String advanceHeader = "";
				
				try{
					advanceHeader = getAdvanceHeader();
					if(advanceHeader.length() > 0){
						advanceHeader += "\r\n";
					}
				}catch(Exception e){
					sbLog.append( e.getMessage() ).append("\r\n");
					throw e;
				}finally{
					 // 獲取當前時間的 ZonedDateTime 對象，並設置時區為 GMT
					ZonedDateTime now = ZonedDateTime.now(ZoneId.of("GMT"));

					// 使用 DateTimeFormatter 格式化 ZonedDateTime 對象為 GMT 字符串
					String gmtString = DateTimeFormatter.RFC_1123_DATE_TIME.withLocale(Locale.US).format(now);

					//製作 Response Headers
					out.print("HTTP/1.0 200 OK\r\n");
					out.print("Server: Java Composer Server 2.1\r\n");
					out.print("Date: "+ gmtString +"\r\n" );
					out.print("Content-type: "+ this.normalResponse +"\r\n");
				
					//將config的header放入
					out.print( advanceHeader );
					
					//同源政策
					out.print("Access-Control-Allow-Credentials: "+ String.valueOf(true) +"\r\n");
					out.print("Access-Control-Allow-Headers: Origin, Methods, Content-Type, Authorization\r\n");
					out.print("Access-Control-Allow-Methods: *\r\n");
					out.print("Access-Control-Allow-Origin: *\r\n");
				
					out.print("Accept-ranges: bytes\r\n");
					out.print("Connection: close\r\n\r\n");
					//製作 Response Headers 結束
				}

				Hashtable htForm = super.getFormData();
				requestBodyJson = getParameter("requestBody");
				if(requestBodyJson == null || requestBodyJson.trim().length() == 0){
					requestBodyJson = (String)htForm.get("requestBody");
				}
				if(requestBodyJson != null){
					requestBodyJson = new String(requestBodyJson.getBytes("ISO8859-1") , "UTF-8");
				}

				requestHeaderJson = getParameter("requestHeader");
				if(requestHeaderJson == null || requestBodyJson.trim().length() == 0){
					requestHeaderJson = (String)htForm.get("requestHeader");
				}
				if(requestHeaderJson != null){
					requestHeaderJson = new String(requestHeaderJson.getBytes("ISO8859-1") , "UTF-8");
				}
			}else{
				String ContentType = req.getContentType();

				//同源政策
				resp.setHeader("Access-Control-Allow-Credentials" , String.valueOf(true));
				resp.setHeader("Access-Control-Allow-Headers" , "Origin, Methods, Content-Type, Authorization");
				resp.setHeader("Access-Control-Allow-Methods" , "*");
				resp.setHeader("Access-Control-Allow-Origin" , "*");

				xMethod = req.getHeader("X-Method");
				if(xMethod == null){
					xMethod = "form-data";
				}
				try{
					if("form-data".equals(xMethod)){
						requestBodyJson = getParameter("requestBody");
						requestBodyJson = new String(requestBodyJson.getBytes("ISO8859-1") , "UTF-8");

						requestHeaderJson = getParameter("requestHeader");
						requestHeaderJson = new String(requestHeaderJson.getBytes("ISO8859-1") , "UTF-8");
					}else if("raw".equals(xMethod)){
						String body = null;
						body = getBody();
						if(body != null && body.length() > 0){
								htBody = (Hashtable)convert.fromJSON(body);
							if(htBody.containsKey("requestBody")){
								requestBodyJson = convert.toJSON(htBody.get("requestBody"));
							}

							if(htBody.containsKey("requestHeader")){
								requestHeaderJson = convert.toJSON(htBody.get("requestHeader"));
							}
						}
					}
				}catch(Exception e){
					throw e;
				}

				returnNormalResponse(this.normalResponse);
			}

			if(requestBodyClass == null){
				requestBodyClass = RequestBody.class;
			}

			if(responseBodyClass == null){
				responseBodyClass = ResponseBody.class;
			}

			if(requestHeaderClass == null){
				requestHeaderClass = RequestHeader.class;
			}

			if(responseHeaderClass == null){
				responseHeaderClass = ResponseHeader.class;
			}

			requestBody = (RequestBody)convertData(requestBodyJson , requestBodyClass);
			requestHeader = (RequestHeader)convertData(requestHeaderJson , requestHeaderClass);

			//驗證登入
			boolean bRight = checkRight();
			if(bRight){
				api_action();
			}

			if(failedStatus != null){
				responseHeader.setResultMessage(failedStatus.getResultMessage());
				responseHeader.setResultCode(failedStatus.getResultCode());
			}else if(errorStatus != null){
				responseHeader.setResultMessage(errorStatus.getResultMessage());
				responseHeader.setResultCode(errorStatus.getResultCode());
			}else if(successStatus != null){
				responseHeader.setResultMessage(successStatus.getResultMessage());
				responseHeader.setResultCode(successStatus.getResultCode());
			}else{
				successStatus = new SuccessStatus("0" , "OK");
				responseHeader.setResultMessage(successStatus.getResultMessage());
				responseHeader.setResultCode(successStatus.getResultCode());
			}

			sbLog.append("requestHeader:" + requestHeaderJson).append("\r\n");
			sbLog.append("requestBody:" + requestBodyJson).append("\r\n");
		}catch(Exception e){
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			sbLog.append( sw.toString() ).append("\r\n");

			failedStatus = new FailedStatus("406" , "系統錯誤");
		}finally{
			if(this.responseHeader == null || this.responseBody == null){
				setResponseHeader(new ResponseHeader());
				setResponseBody(new ResponseBody());
			}

			if(failedStatus != null){
				responseHeader.setResultMessage(failedStatus.getResultMessage());
				responseHeader.setResultCode(failedStatus.getResultCode());
			}

			apiResponse.setResponseHeader(getResponseHeader());
			apiResponse.setResponseBody(getResponseBody());

			objectMapper = new ObjectMapper();
			String responseBodyJson = objectMapper.writeValueAsString(apiResponse);

			out.println(responseBodyJson);

			//紀錄Log
			convert.log("BaseAPI",sbLog.toString());
		}
	}

	public void setResponseBody(ResponseBody responseBody){
		this.responseBody = responseBody;
	}
	
	public void setResponseHeader(ResponseHeader responseHeader){
		this.responseHeader = responseHeader;
	}

	public void setRequestBodyClass(Class requestBodyClass){
		this.requestBodyClass = requestBodyClass;
	}
	
	public void setResponseBodyClass(Class responseBodyClass){
		this.responseBodyClass = responseBodyClass;
	}

	public void setRequestHeaderClass(Class requestHeaderClass){
		this.requestHeaderClass = requestHeaderClass;
	}
	
	public void setResponseHeaderClass(Class responseHeaderClass){
		this.responseHeaderClass = responseHeaderClass;
	}

	private Object convertData(String json , Class instance)throws Exception{
	
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, instance);
	}

	private String getBody() throws Exception{
		String result = "";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream inputStream = null;
		try{
			if(checkAuthorization() ){
				inputStream = req.getInputStream();
			}
			byte[] buffer = new byte[1024];
			int len;
			while((len = inputStream.read(buffer)) != -1){
				baos.write(buffer , 0 , len);
			}

			result = new String(baos.toByteArray() , "UTF-8");
		}catch(IOException e){
			throw e;
		}finally{
			baos.close();
			inputStream.close();
		}
		return result;
	}

	private String getAdvanceHeader() throws Exception{
		String header = "";
		InputStream inputStream = null;
		try{
			if(checkAuthorization() ){
				inputStream = getClass().getClassLoader().getResourceAsStream("config/advance.cfg");
			}
			if (inputStream == null) {
				throw new Exception("advance.cfg not found");
			}

			Properties pAdvance = new Properties();
			pAdvance.load(inputStream);
			header = pAdvance.getProperty("header","");
			header = encode(header);

			String[] arrHeader = convert.separStr(header,"\n");
			for(int i = 0 ; i < arrHeader.length; i++ ){
				String iheader = arrHeader[i].trim();
				if(iheader.indexOf(":") == -1){
					throw new Exception("the header ["+ iheader +"] is error in advance.cfg");
				}
			}
		}catch(Exception e){
			throw e;
		}finally{
			if(inputStream != null){
				inputStream.close();
			}
		}
		return header;
	}

	//預防資安漏洞
	private byte[] encode(byte[] b) {
		return b;
	}
	private String encode(String str) {
		return str;
	}
}

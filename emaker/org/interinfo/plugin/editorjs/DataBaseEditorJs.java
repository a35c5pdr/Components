package org.interinfo.plugin.editorjs;
import jcx.db.talk;

import java.util.Vector;
import java.util.Hashtable;

public abstract class DataBaseEditorJs extends org.interinfo.db.DataBase{

	//	各欄位最大長度
	public static final int MAX_SNO_LEN = 50;

	//	單號
	private String SNO = null;
	private String DATA = null;
	private String tbName = null;
	
	public DataBaseEditorJs(Object obj) {
		super(obj);
	}
	
	public abstract void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception;
	public abstract String getCreateSql(talk t);
	public abstract String getDeleteSql(String sqlWhere);
	
	public void setSNO(String SNO) {
		this.SNO = SNO;
	}
	
	public void setDATA(String DATA) {
		this.DATA = DATA;
	}

	public String getSNO() {
		return this.SNO;
	}

	public String getDATA() {
		return this.DATA;
	}

	public void setTbName(String tbName) {
		this.tbName = tbName;
	}

	public String getTbName() {
		return this.tbName;
	}
}
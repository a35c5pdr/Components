package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

public class EditorJsHeader extends org.interinfo.plugin.editorjs.DataBaseEditorJsBlock{

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	INO				int, "
				+ " 	TEXT			nvarchar(" + String.valueOf(MAX_TEXT_LEN) + "), "
				+ " 	LEVEL			int, "
				+ " primary key(SNO, INO) "
				+ " ) ";
		return sql;
	}

	public EditorJsHeader(Object obj, EditorJsContent myEditorJsContent) {
		super(obj);
		
		this.myEditorJsContent = myEditorJsContent;
		setTbName( myEditorJsContent.getTbName() + "_HEADER" );

		this.setSNO( myEditorJsContent.getSNO() );
		this.setINO( myEditorJsContent.getINO() );
	}
	

	//	各欄位最大長度
	public static final int MAX_TEXT_LEN = 3000;

	//	各欄位變數
	private EditorJsContent myEditorJsContent = null;
	private String TEXT = "";						//	標題內文
	private int LEVEL = 2;							//	大小

	public void setTEXT(String TEXT) {
		this.TEXT = TEXT;
	}

	public void setLEVEL(int LEVEL) {
		this.LEVEL = LEVEL;
	}

	public void setLEVEL(String LEVEL) {
		setLEVEL( Integer.valueOf(LEVEL) );
	}

	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("INO".equals(COL)) { 
			setINO(value);
		} else if("TEXT".equals(COL)) { 
			setTEXT(value);
		} else if("LEVEL".equals(COL)) { 
			setLEVEL(value);
		}

	}

	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {
		Hashtable htBlock = (Hashtable)objData;

		Hashtable htData = (Hashtable)htBlock.get("data");
		if(htData != null) {
			String text = ((String)pMethod.getOrDefault(htData, "text", "")).trim();
			int level = (Integer)pMethod.getOrDefault(htData, "level", 2);

			this.setTEXT(text);
			this.setLEVEL(level);

			vContainer.add(this);
		}
	}

	public String getTEXT() {
		return this.TEXT;
	}

	public int getLEVEL() {
		return this.LEVEL;
	}

	public String getSqlLEVEL() {
		return String.valueOf( getLEVEL() );
	}

	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("INO".equals(COL)) { 
			return getSqlINO();
		} else if("TEXT".equals(COL)) { 
			return getTEXT();
		} else if("LEVEL".equals(COL)) { 
			return getSqlLEVEL();
		}

		return null;
	}

	public void getContent(Object obj, Object objData) throws Exception{
		Hashtable htJSON = (Hashtable)objData;
		Hashtable htData = new Hashtable();

		htData.put("text", this.getTEXT());
		htData.put("LEVEL", this.getLEVEL());


		htJSON.put("data", htData);
	}

	//	檢查primary key 是否設定
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL") && this.getINO() != -1) {
			return true;
		} else {
			return false;
		}
	}

	//	該物件primary where
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' "
			+ " and " + asName + "INO = " + convert.ToSql(this.getSqlINO()) + " ";
	}

	//	取得該物件資料的 insert sql
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	//	取得該物件資料的 delete sql
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	//	取得該物件資料的 update sql
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}

	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	//	取得該類別查詢db 物件
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	//	取得該類別異動db 物件
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	//	取得該table 各欄位名稱陣列
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "INO",
			asName + "TEXT",
			asName + "LEVEL"
		};
	}

	//	取得各欄位異動db value 語法
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("INO", " " + convert.ToSql(getSqlINO()) + " ");
		ht.put("TEXT", " " + DBN + "'" + convert.ToSql(getTEXT()) + "' ");
		ht.put("LEVEL", " " + convert.ToSql(getSqlLEVEL()) + " ");


		return ht;
	}

	public EditorJsHeader[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJsHeader iHeader = new EditorJsHeader(obj, this.myEditorJsContent);
			setObjectByHash(obj, iHeader, arrData[i], arrCol);
			vData.add(iHeader);
		}
		
		return (EditorJsHeader[])vData.toArray(new EditorJsHeader[0]);
	}

	//	檢查各欄位數值是否正常
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("INO".equals(col)) { 
			ht = checkValue_INO(obj, POSITION);					//	序號
		} else if("TEXT".equals(col)) { 
			ht = checkValue_TEXT(obj, POSITION);				//	標題內文
		} else if("LEVEL".equals(col)) { 
			ht = checkValue_LEVEL(obj, POSITION);				//	大小
		}

		return ht;
	}

	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_INO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_TEXT(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_LEVEL(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		return MVC.createFieldResult(msg, invalid, change);
	}
}

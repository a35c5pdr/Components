package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

public class EditorJsImage extends org.interinfo.plugin.editorjs.DataBaseEditorJsBlock{

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	INO				int, "
				+ " 	CAPTION			nvarchar(" + String.valueOf(MAX_CAPTION_LEN) + "), "
				+ " 	WITH_BORDER		varchar(" + String.valueOf(MAX_WITH_BORDER_LEN) + "), "
				+ " 	STRETCHED		varchar(" + String.valueOf(MAX_STRETCHED_LEN) + "), "
				+ " 	WITH_BACKGROUND	varchar(" + String.valueOf(MAX_WITH_BACKGROUND_LEN) + "), "
				+ " primary key(SNO, INO) "
				+ " ) ";
		return sql;
	}

	public EditorJsImage(Object obj, EditorJsContent myEditorJsContent) {
		super(obj);
			
		setTbName( myEditorJsContent.getTbName() + "_IMAGE" );
		
		this.myEditorJsContent = myEditorJsContent;
		this.setSNO( myEditorJsContent.getSNO() );
		this.setINO( myEditorJsContent.getINO() );
	}

	//	各欄位最大長度
	public static final int MAX_CAPTION_LEN = 300;
	public static final int MAX_WITH_BORDER_LEN = 1;
	public static final int MAX_STRETCHED_LEN = 1;
	public static final int MAX_WITH_BACKGROUND_LEN = 1;
	
	//	各欄位變數
	private EditorJsContent myEditorJsContent = null;
	private String CAPTION = "";					//	內文
	private boolean WITH_BORDER = false;
	private boolean STRETCHED = false;
	private boolean WITH_BACKGROUND = false;

	public void setCAPTION(String CAPTION) {
		this.CAPTION = CAPTION;
	}

	public void setWITH_BORDER(boolean WITH_BORDER) {
		this.WITH_BORDER = WITH_BORDER;
	}

	public void setWITH_BORDER(String WITH_BORDER) {
		this.setWITH_BORDER( "Y".equals(WITH_BORDER) );
	}

	public void setSTRETCHED(boolean STRETCHED) {
		this.STRETCHED = STRETCHED;
	}

	public void setSTRETCHED(String STRETCHED) {
		this.setSTRETCHED( "Y".equals(STRETCHED) );
	}

	public void setWITH_BACKGROUND(boolean WITH_BACKGROUND) {
		this.WITH_BACKGROUND = WITH_BACKGROUND;
	}

	public void setWITH_BACKGROUND(String WITH_BACKGROUND) {
		this.setWITH_BACKGROUND( "Y".equals(WITH_BACKGROUND) );
	}


	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("INO".equals(COL)) { 
			setINO(value);
		} else if("CAPTION".equals(COL)) { 
			setCAPTION(value);
		} else if("WITH_BORDER".equals(COL)) { 
			setWITH_BORDER(value);
		} else if("STRETCHED".equals(COL)) { 
			setSTRETCHED(value);
		} else if("WITH_BACKGROUND".equals(COL)) { 
			setWITH_BACKGROUND(value);
		}

	}

	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {
		Hashtable htBlock = (Hashtable)objData;

		Hashtable htData = (Hashtable)htBlock.get("data");
		if(htData != null) {

			String caption = ((String)htData.get("caption")).trim();
			Boolean stretched = (Boolean)htData.get("stretched");
			Boolean withBackground = (Boolean)htData.get("withBackground");
			Boolean withBorder = (Boolean)htData.get("withBorder");
			Hashtable htFile = (Hashtable)htData.get("file");

			this.setCAPTION(caption);
			this.setSTRETCHED(stretched);
			this.setWITH_BACKGROUND(withBackground);
			this.setWITH_BORDER(withBorder);


			EditorJsImage_File myImage_File = new EditorJsImage_File(obj, this);
			myImage_File.setContentByJSON(obj, htFile, vContainer);

			
			vContainer.add(this);
		}
	}
	
	public String getCAPTION() {
		return this.CAPTION;
	}

	public boolean getWITH_BORDER() {
		return this.WITH_BORDER;
	}

	public String getSqlWITH_BORDER() {
		if(this.getWITH_BORDER()) {
			return "Y";
		} else {
			return "N";
		}
	}

	public boolean getSTRETCHED() {
		return this.STRETCHED;
	}

	public String getSqlSTRETCHED() {
		if(this.getSTRETCHED()) {
			return "Y";
		} else {
			return "N";
		}
	}

	public boolean getWITH_BACKGROUND() {
		return this.WITH_BACKGROUND;
	}

	public String getSqlWITH_BACKGROUND() {
		if(this.getWITH_BACKGROUND()) {
			return "Y";
		} else {
			return "N";
		}
	}

	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("INO".equals(COL)) { 
			return getSqlINO();
		} else if("CAPTION".equals(COL)) { 
			return getCAPTION();
		} else if("WITH_BORDER".equals(COL)) { 
			return getSqlWITH_BORDER();
		} else if("STRETCHED".equals(COL)) { 
			return getSqlSTRETCHED();
		} else if("WITH_BACKGROUND".equals(COL)) { 
			return getSqlWITH_BACKGROUND();
		}

		return null;
	}

	public void getContent(Object obj, Object objData) throws Exception{
		Hashtable htJSON = (Hashtable)objData;
		Hashtable htData = new Hashtable();
		Hashtable htFile = new Hashtable();



		String sqlWhere = ""
					+ " and a.SNO = ? "
					+ " and a.INO = ? ";

		EditorJsImage_File myFile = new EditorJsImage_File(obj, this);
		DBQuerySql querySql = myFile.getQuerySql(sqlWhere);

		EditorJsImage_File[] arrFile = myFile.getObject(obj, querySql, new String[]{this.getSNO(), this.getSqlINO()});
		if(arrFile.length == 1) {
			EditorJsImage_File file = arrFile[0];
			file.getContent(obj, htFile);
		}


		htData.put("caption", this.getCAPTION());
		htData.put("stretched", this.getSTRETCHED());
		htData.put("withBackground", this.getWITH_BACKGROUND());
		htData.put("withBorder", this.getWITH_BORDER());
		htData.put("file", htFile);
		
		htJSON.put("data", htData);
	}

	//	檢查primary key 是否設定
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL") && this.getINO() != -1) {
			return true;
		} else {
			return false;
		}
	}

	//	該物件primary where
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' "
			+ " and " + asName + "INO = " + convert.ToSql(this.getSqlINO()) + " ";
	}

	//	取得該物件資料的 insert sql
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	//	取得該物件資料的 delete sql
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	//	取得該物件資料的 update sql
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}

	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	//	取得該類別查詢db 物件
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	//	取得該類別異動db 物件
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	//	取得該table 各欄位名稱陣列
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "INO",
			asName + "CAPTION",
			asName + "WITH_BORDER",
			asName + "STRETCHED",
			asName + "WITH_BACKGROUND"
		};
	}

	//	取得各欄位異動db value 語法
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("INO", " " + convert.ToSql(getSqlINO()) + " ");
		ht.put("CAPTION", " '" + convert.ToSql(getCAPTION()) + "' ");
		ht.put("WITH_BORDER", " '" + convert.ToSql(getSqlWITH_BORDER()) + "' ");
		ht.put("STRETCHED", " '" + convert.ToSql(getSqlSTRETCHED()) + "' ");
		ht.put("WITH_BACKGROUND", " '" + convert.ToSql(getSqlWITH_BACKGROUND()) + "' ");


		return ht;
	}

	public EditorJsImage[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJsImage iImage = new EditorJsImage(obj, this.myEditorJsContent);
			setObjectByHash(obj, iImage, arrData[i], arrCol);
			vData.add(iImage);
		}
		
		return (EditorJsImage[])vData.toArray(new EditorJsImage[0]);
	}

	//	檢查各欄位數值是否正常
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("INO".equals(col)) { 
			ht = checkValue_INO(obj, POSITION);					//	序號
		} else if("CAPTION".equals(col)) { 
			ht = checkValue_CAPTION(obj, POSITION);				//	
		} else if("WITH_BORDER".equals(col)) { 
			ht = checkValue_WITH_BORDER(obj, POSITION);			//	
		} else if("STRETCHED".equals(col)) { 
			ht = checkValue_STRETCHED(obj, POSITION);			//	
		} else if("WITH_BACKGROUND".equals(col)) { 
			ht = checkValue_WITH_BACKGROUND(obj, POSITION);		//	
		}

		return ht;
	}

	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_INO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_CAPTION(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_WITH_BORDER(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_STRETCHED(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_WITH_BACKGROUND(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}
}

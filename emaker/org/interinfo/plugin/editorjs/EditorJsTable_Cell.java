package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

public class EditorJsTable_Cell extends org.interinfo.plugin.editorjs.DataBaseEditorJsBlock{

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	INO				int, "
				+ " 	RNO				int, "
				+ " 	CNO				int, "
				+ " 	CONTENT			nvarchar(" + String.valueOf(MAX_CONTENT_LEN) + "), "
				+ " primary key(SNO, INO, RNO, CNO) "
				+ " ) ";
		return sql;
	}

	public EditorJsTable_Cell(Object obj, EditorJsTable myEditorJsTable) {
		super(obj);
			
		setTbName( myEditorJsTable.getTbName() + "_CELL" );
		
		this.myEditorJsTable = myEditorJsTable;
		this.setSNO( myEditorJsTable.getSNO() );
		this.setINO( myEditorJsTable.getINO() );
	}

	//	各欄位最大長度
	public static final int MAX_CONTENT_LEN = 300;
	public static final int MAX_CHECKED_LEN = 1;
	
	//	各欄位變數
	private EditorJsTable myEditorJsTable = null;
	private int RNO = -1;					//	ROW
	private int CNO = -1;					//	COLUMN
	private String CONTENT = "";			//	內文

	public void setRNO(int RNO) {
		this.RNO = RNO;
	}

	public void setRNO(String RNO) {
		setRNO( Integer.valueOf(RNO) );
	}

	public void setCNO(int CNO) {
		this.CNO = CNO;
	}

	public void setCNO(String CNO) {
		setCNO( Integer.valueOf(CNO) );
	}

	public void setCONTENT(String CONTENT) {
		this.CONTENT = CONTENT;
	}

	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("INO".equals(COL)) { 
			setINO(value);
		} else if("RNO".equals(COL)) { 
			setRNO(value);
		} else if("CNO".equals(COL)) { 
			setCNO(value);
		} else if("CONTENT".equals(COL)) { 
			setCONTENT(value);
		}

	}

	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {
		String CONTENT = (String)objData;

		this.setCONTENT(CONTENT);

		vContainer.add(this);
	}

	public int getRNO() {
		return this.RNO;
	}

	public String getSqlRNO() {
		return String.valueOf( this.getRNO() );
	}

	public int getCNO() {
		return this.CNO;
	}

	public String getSqlCNO() {
		return String.valueOf( this.getCNO() );
	}

	public String getCONTENT() {
		return this.CONTENT;
	}

	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("INO".equals(COL)) { 
			return getSqlINO();
		} else if("RNO".equals(COL)) { 
			return getSqlRNO();
		} else if("CNO".equals(COL)) { 
			return getSqlCNO();
		} else if("CONTENT".equals(COL)) { 
			return getCONTENT();
		}

		return null;
	}

	public void getContent(Object obj, Object objData) throws Exception{
		Vector vCell = (Vector)objData;

		vCell.add(this.getCONTENT());
	}

	//	檢查primary key 是否設定
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL") && this.getINO() != -1 && this.getRNO() != -1 && this.getCNO() != -1) {
			return true;
		} else {
			return false;
		}
	}

	//	該物件primary where
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' "
			+ " and " + asName + "INO = " + convert.ToSql(this.getSqlINO()) + " "
			+ " and " + asName + "RNO = " + convert.ToSql(this.getSqlRNO()) + " "
			+ " and " + asName + "CNO = " + convert.ToSql(this.getSqlCNO()) + " ";
	}

	//	取得該物件資料的 insert sql
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	//	取得該物件資料的 delete sql
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	//	取得該物件資料的 update sql
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}

	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	//	取得該類別查詢db 物件
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	//	取得該類別異動db 物件
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	//	取得該table 各欄位名稱陣列
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "INO",
			asName + "RNO",
			asName + "CNO",
			asName + "CONTENT",

		};
	}

	//	取得各欄位異動db value 語法
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("INO", " " + convert.ToSql(getSqlINO()) + " ");
		ht.put("RNO", " " + convert.ToSql(getSqlRNO()) + " ");
		ht.put("CNO", " " + convert.ToSql(getSqlCNO()) + " ");
		ht.put("CONTENT", " " + DBN + "'" + convert.ToSql(getCONTENT()) + "' ");

		return ht;
	}

	public EditorJsTable_Cell[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJsTable_Cell iCell = new EditorJsTable_Cell(obj, this.myEditorJsTable);
			setObjectByHash(obj, iCell, arrData[i], arrCol);
			vData.add(iCell);
		}
		
		return (EditorJsTable_Cell[])vData.toArray(new EditorJsTable_Cell[0]);
	}

	//	檢查各欄位數值是否正常
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("INO".equals(col)) { 
			ht = checkValue_INO(obj, POSITION);					//	序號
		} else if("RNO".equals(col)) { 
			ht = checkValue_RNO(obj, POSITION);					//	ROW
		} else if("CNO".equals(col)) { 
			ht = checkValue_CNO(obj, POSITION);					//	COLUMN
		} else if("CONTENT".equals(col)) { 
			ht = checkValue_CONTENT(obj, POSITION);				//	項目內容
		}

		return ht;
	}

	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_INO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_RNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_CNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_CONTENT(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}
}

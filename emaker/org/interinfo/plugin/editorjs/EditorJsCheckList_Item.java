package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

public class EditorJsCheckList_Item extends org.interinfo.plugin.editorjs.DataBaseEditorJsBlock{

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	INO				int, "
				+ " 	CNO				int, "
				+ " 	TEXT			nvarchar(" + String.valueOf(MAX_TEXT_LEN) + "), "
				+ " 	CHECKED			varchar(" + String.valueOf(MAX_CHECKED_LEN) + "), "
				+ " primary key(SNO, INO, CNO) "
				+ " ) ";
		return sql;
	}

	public EditorJsCheckList_Item(Object obj, EditorJsCheckList myEditorJsCheckList) {
		super(obj);
			
		setTbName( myEditorJsCheckList.getTbName() + "_ITEM" );
		
		this.myEditorJsCheckList = myEditorJsCheckList;
		this.setSNO( myEditorJsCheckList.getSNO() );
		this.setINO( myEditorJsCheckList.getINO() );
	}

	//	各欄位最大長度
	private EditorJsCheckList myEditorJsCheckList = null;
	public static final int MAX_TEXT_LEN = 300;
	public static final int MAX_CHECKED_LEN = 1;
	
	//	各欄位變數
	private int CNO = -1;					//	代辦事項序號
	private String TEXT = "";				//	項目內文
	private boolean CHECKED = false;		//	是否完成

	public void setCNO(int CNO) {
		this.CNO = CNO;
	}

	public void setCNO(String CNO) {
		setCNO( Integer.valueOf(CNO) );
	}

	public void setTEXT(String TEXT) {
		this.TEXT = TEXT;
	}

	public void setCHECKED(boolean CHECKED) {
		this.CHECKED = CHECKED;
	}

	public void setCHECKED(String CHECKED) {
		this.setCHECKED( "Y".equals(CHECKED) );
	}

	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("INO".equals(COL)) { 
			setINO(value);
		} else if("CNO".equals(COL)) { 
			setCNO(value);
		} else if("TEXT".equals(COL)) { 
			setTEXT(value);
		} else if("CHECKED".equals(COL)) { 
			setCHECKED(value);
		}

	}

	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {
		Hashtable htBlock = (Hashtable)objData;

		boolean checked = (Boolean)htBlock.get("checked");
		String text = (String)htBlock.get("text");
		
		this.setCHECKED(checked);
		this.setTEXT(text);

		vContainer.add(this);
	}

	public int getCNO() {
		return this.CNO;
	}

	public String getSqlCNO() {
		return String.valueOf( this.getCNO() );
	}

	public String getTEXT() {
		return this.TEXT;
	}

	public boolean getCHECKED() {
		return this.CHECKED;
	}

	public String getSqlCHECKED() {
		if(this.getCHECKED()) {
			return "Y";
		} else {
			return "N";
		}
	}


	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("INO".equals(COL)) { 
			return getSqlINO();
		} else if("CNO".equals(COL)) { 
			return getSqlCNO();
		} else if("TEXT".equals(COL)) { 
			return getTEXT();
		} else if("CHECKED".equals(COL)) { 
			return getSqlCHECKED();
		}

		return null;
	}

	public void getContent(Object obj, Object objData) throws Exception{
		Vector vItem = (Vector)objData;
		Hashtable htData = new Hashtable();
		vItem.add(htData);

		
		htData.put("checked", this.getCHECKED());
		htData.put("text", this.getTEXT());
	}

	//	檢查primary key 是否設定
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL") && this.getINO() != -1 && this.getCNO() != -1) {
			return true;
		} else {
			return false;
		}
	}

	//	該物件primary where
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' "
			+ " and " + asName + "INO = " + convert.ToSql(this.getSqlINO()) + " "
			+ " and " + asName + "CNO = " + convert.ToSql(this.getSqlCNO()) + " ";
	}

	//	取得該物件資料的 insert sql
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	//	取得該物件資料的 delete sql
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	//	取得該物件資料的 update sql
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}

	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	//	取得該類別查詢db 物件
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	//	取得該類別異動db 物件
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	//	取得該table 各欄位名稱陣列
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "INO",
			asName + "CNO",
			asName + "TEXT",
			asName + "CHECKED",
		};
	}

	//	取得各欄位異動db value 語法
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("INO", " " + convert.ToSql(getSqlINO()) + " ");
		ht.put("CNO", " " + convert.ToSql(getSqlCNO()) + " ");
		ht.put("TEXT", " " + DBN + "'" + convert.ToSql(getTEXT()) + "' ");
		ht.put("CHECKED", " '" + convert.ToSql(getSqlCHECKED()) + "' ");

		return ht;
	}

	public EditorJsCheckList_Item[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJsCheckList_Item iCheckList = new EditorJsCheckList_Item(obj, this.myEditorJsCheckList);
			setObjectByHash(obj, iCheckList, arrData[i], arrCol);
			vData.add(iCheckList);
		}
		
		return (EditorJsCheckList_Item[])vData.toArray(new EditorJsCheckList_Item[0]);
	}

	//	檢查各欄位數值是否正常
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("INO".equals(col)) { 
			ht = checkValue_INO(obj, POSITION);					//	序號
		} else if("CNO".equals(col)) { 
			ht = checkValue_CNO(obj, POSITION);					//	代辦事項序號
		} else if("TEXT".equals(col)) { 
			ht = checkValue_TEXT(obj, POSITION);				//	項目內容
		} else if("CHECKED".equals(col)) { 
			ht = checkValue_CHECKED(obj, POSITION);				//	是否完成
		}

		return ht;
	}

	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_INO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_CNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_TEXT(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_CHECKED(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}
}

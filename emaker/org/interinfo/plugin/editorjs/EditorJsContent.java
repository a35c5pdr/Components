package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

public class EditorJsContent extends org.interinfo.plugin.editorjs.DataBaseEditorJsContent{

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	INO				int, "
				+ " 	ITEMTYPE		varchar(" + String.valueOf(MAX_ITEMTYPE_LEN) + "), "
				+ " primary key(SNO, INO) "
				+ " ) ";
		return sql;
	}

	public EditorJsContent(Object obj, EditorJs myEditorJs) {
		super(obj);
		
		this.myEditorJs = myEditorJs;
		setTbName( myEditorJs.getTbName() + "_CONTENT" );
	}

	//	各欄位最大長度
	public static final int MAX_ITEMTYPE_LEN = 30;

	//	各欄位變數
	private EditorJs myEditorJs = null;
	private String ITEMTYPE = null;					//	資料類型

	public void setITEMTYPE(String ITEMTYPE) {
		this.ITEMTYPE = ITEMTYPE;
	}

	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("INO".equals(COL)) { 
			setINO(value);
		} else if("ITEMTYPE".equals(COL)) { 
			setITEMTYPE(value);
		}

	}
	

	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {

		Hashtable htBlock = (Hashtable)objData;
		String type = (String)htBlock.get("type");
		this.setITEMTYPE(type);
		
		if("header".equals(type)) {
			vContainer.add(this);

			EditorJsHeader myHeader = new EditorJsHeader(obj, this);
			myHeader.setContentByJSON(obj, htBlock, vContainer);
		} else if("checklist".equals(type)) {
			vContainer.add(this);

			EditorJsCheckList myCheckList = new EditorJsCheckList(obj, this);
			myCheckList.setContentByJSON(obj, htBlock, vContainer);
		} else if("code".equals(type)) {
			vContainer.add(this);

			EditorJsCode myCode = new EditorJsCode(obj, this);
			myCode.setContentByJSON(obj, htBlock, vContainer);
		} else if("paragraph".equals(type)) {
			vContainer.add(this);

			EditorJsParagraph myParagraph = new EditorJsParagraph(obj, this);
			myParagraph.setContentByJSON(obj, htBlock, vContainer);
		} else if("delimiter".equals(type)) {
			vContainer.add(this);

			EditorJsDelimiter myDelimiter = new EditorJsDelimiter(obj, this);
			myDelimiter.setContentByJSON(obj, htBlock, vContainer);
		} else if("list".equals(type)) {
			vContainer.add(this);

			EditorJsList myList = new EditorJsList(obj, this);
			myList.setContentByJSON(obj, htBlock, vContainer);
		} else if("table".equals(type)) {
			vContainer.add(this);

			EditorJsTable myTable = new EditorJsTable(obj, this);
			myTable.setContentByJSON(obj, htBlock, vContainer);
		} else if("image".equals(type)) {
			vContainer.add(this);

			EditorJsImage myImage = new EditorJsImage(obj, this);
			myImage.setContentByJSON(obj, htBlock, vContainer);
		}

	}

	public String getITEMTYPE() {
		return this.ITEMTYPE;
	}

	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("INO".equals(COL)) { 
			return getSqlINO();
		} else if("ITEMTYPE".equals(COL)) { 
			return getITEMTYPE();
		}

		return null;
	}

	//	檢查primary key 是否設定
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL") && this.getINO() != -1) {
			return true;
		} else {
			return false;
		}
	}

	//	該物件primary where
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' "
			+ " and " + asName + "INO = " + convert.ToSql(this.getSqlINO()) + " ";
	}

	//	取得該物件資料的 insert sql
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	//	取得該物件資料的 delete sql
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	//	取得該物件資料的 update sql
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}

	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	//	取得該類別查詢db 物件
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	//	取得該類別異動db 物件
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	//	取得該table 各欄位名稱陣列
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "INO",
			asName + "ITEMTYPE"
		};
	}

	//	取得各欄位異動db value 語法
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("INO", " " + convert.ToSql(getSqlINO()) + " ");
		ht.put("ITEMTYPE", " '" + convert.ToSql(getITEMTYPE()) + "' ");


		return ht;
	}

	public EditorJsContent[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJsContent iContent = new EditorJsContent(obj, myEditorJs);
			setObjectByHash(obj, iContent, arrData[i], arrCol);
			vData.add(iContent);
		}
		
		return (EditorJsContent[])vData.toArray(new EditorJsContent[0]);
	}

	//	檢查各欄位數值是否正常
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("INO".equals(col)) { 
			ht = checkValue_INO(obj, POSITION);					//	序號
		} else if("ITEMTYPE".equals(col)) { 
			ht = checkValue_ITEMTYPE(obj, POSITION);			//	資料類型
		}

		return ht;
	}

	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_INO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_ITEMTYPE(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJsContent.MAX_ITEMTYPE_LEN);
		if(!pCheck.checkValue(getITEMTYPE(), "NOTNULL")) {
			msg = "資料類型不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getITEMTYPE(), "ISEN")) {
			msg = "資料類型只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getITEMTYPE(), "MAXLENGTH:" + MAXLEN)) {
			msg = "資料類型最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}
}

package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

public class EditorJsList_Item extends org.interinfo.plugin.editorjs.DataBaseEditorJsBlock{

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	INO				int, "
				+ " 	LNO				int, "
				+ " 	ITEM			nvarchar(" + String.valueOf(MAX_ITEM_LEN) + "), "
				+ " primary key(SNO, INO, LNO) "
				+ " ) ";
		return sql;
	}

	public EditorJsList_Item(Object obj, EditorJsList myEditorJsList) {
		super(obj);
			
		setTbName( myEditorJsList.getTbName() + "_ITEM" );
		
		this.myEditorJsList = myEditorJsList;
		this.setSNO( myEditorJsList.getSNO() );
		this.setINO( myEditorJsList.getINO() );
	}

	//	各欄位最大長度
	public static final int MAX_ITEM_LEN = 300;
	public static final int MAX_CHECKED_LEN = 1;
	
	//	各欄位變數
	private EditorJsList myEditorJsList = null;
	private int LNO = -1;					//	代辦事項序號
	private String ITEM = "";				//	項目內文

	public void setLNO(int LNO) {
		this.LNO = LNO;
	}

	public void setLNO(String LNO) {
		setLNO( Integer.valueOf(LNO) );
	}

	public void setITEM(String ITEM) {
		this.ITEM = ITEM;
	}


	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("INO".equals(COL)) { 
			setINO(value);
		} else if("LNO".equals(COL)) { 
			setLNO(value);
		} else if("ITEM".equals(COL)) { 
			setITEM(value);
		}

	}

	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {
		String ITEM = (String)objData;

		this.setITEM(ITEM);

		vContainer.add(this);
	}

	public int getLNO() {
		return this.LNO;
	}

	public String getSqlLNO() {
		return String.valueOf( this.getLNO() );
	}

	public String getITEM() {
		return this.ITEM;
	}

	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("INO".equals(COL)) { 
			return getSqlINO();
		} else if("LNO".equals(COL)) { 
			return getSqlLNO();
		} else if("ITEM".equals(COL)) { 
			return getITEM();
		}

		return null;
	}

	public void getContent(Object obj, Object objData) throws Exception{
		Vector vItem = (Vector)objData;

		vItem.add( this.getITEM() );
	}

	//	檢查primary key 是否設定
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL") && this.getINO() != -1 && this.getLNO() != -1) {
			return true;
		} else {
			return false;
		}
	}

	//	該物件primary where
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' "
			+ " and " + asName + "INO = " + convert.ToSql(this.getSqlINO()) + " "
			+ " and " + asName + "LNO = " + convert.ToSql(this.getSqlLNO()) + " ";
	}

	//	取得該物件資料的 insert sql
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	//	取得該物件資料的 delete sql
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	//	取得該物件資料的 update sql
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}

	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	//	取得該類別查詢db 物件
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	//	取得該類別異動db 物件
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	//	取得該table 各欄位名稱陣列
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "INO",
			asName + "LNO",
			asName + "ITEM"
		};
	}

	//	取得各欄位異動db value 語法
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("INO", " " + convert.ToSql(getSqlINO()) + " ");
		ht.put("LNO", " " + convert.ToSql(getSqlLNO()) + " ");
		ht.put("ITEM", " " + DBN + "'" + convert.ToSql(getITEM()) + "' ");

		return ht;
	}

	public EditorJsList_Item[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJsList_Item iItem = new EditorJsList_Item(obj, this.myEditorJsList);
			setObjectByHash(obj, iItem, arrData[i], arrCol);
			vData.add(iItem);
		}
		
		return (EditorJsList_Item[])vData.toArray(new EditorJsList_Item[0]);
	}

	//	檢查各欄位數值是否正常
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("INO".equals(col)) { 
			ht = checkValue_INO(obj, POSITION);					//	序號
		} else if("LNO".equals(col)) { 
			ht = checkValue_LNO(obj, POSITION);					//	代辦事項序號
		} else if("ITEM".equals(col)) { 
			ht = checkValue_ITEM(obj, POSITION);				//	項目內容
		}

		return ht;
	}

	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_INO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_LNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	public Hashtable checkValue_ITEM(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
	
		return MVC.createFieldResult(msg, invalid, change);
	}
}

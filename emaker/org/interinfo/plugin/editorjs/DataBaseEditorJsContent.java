package org.interinfo.plugin.editorjs;



public abstract class DataBaseEditorJsContent extends org.interinfo.plugin.editorjs.DataBaseEditorJs{
	private int INO = -1;

	public DataBaseEditorJsContent(Object obj) {
		super(obj);
	}

	public void setINO(int INO) {
		this.INO = INO;
	}

	public void setINO(String INO) {
		this.setINO(Integer.valueOf(INO));
	}

	public int getINO() {
		return this.INO;
	}

	public String getSqlINO() {
		return String.valueOf(this.getINO());
	}
}

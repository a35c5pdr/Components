package org.interinfo.plugin.editorjs;

import java.util.Vector;
import java.util.Hashtable;

public abstract class DataBaseEditorJsBlock extends org.interinfo.plugin.editorjs.DataBaseEditorJsContent{
	
	public DataBaseEditorJsBlock(Object obj) {
		super(obj);
	}

	public abstract void getContent(Object obj, Object objData) throws Exception;
}
package org.interinfo.plugin.editorjs;

import java.util.*;
import jcx.lib.*;
import jcx.util.*;
import jcx.jform.hproc;
import jcx.db.talk;

import org.interinfo.core.Conn;
import org.interinfo.core.MVC;
import org.interinfo.db.DataBase;
import org.interinfo.db.DBQuerySql;
import org.interinfo.db.DBExecSql;

/**
 * EditorJs 整合元件
 */
public class EditorJs extends org.interinfo.plugin.editorjs.DataBaseEditorJs{
	
	/**
	 * 取得EditorJs 物件
	 * @param		obj				this(DMaker物件)
	 * @param		tbName			關聯table名稱
	 */
	public EditorJs(Object obj, String tbName) {
		super(obj);

		setTbName( tbName + "_EDITORJS" );
	}
	
	/**
	 * 取得EditorJs 物件<br>
	 * @param		obj				this(DMaker物件)
	 * @param		tbName			關聯table名稱
	 * @param		SNO_TITLE		單號開頭
	 */
	public EditorJs(Object obj, String tbName, String SNO_TITLE) {
		super(obj);
		
		this.SNO_TITLE = SNO_TITLE;
		setTbName( tbName + "_EDITORJS" );
	}
	

	public String getCreateSql(talk t) {
		String sql = ""
				+ " create table " + getTbName() + " ( "
				+ " 	SNO				varchar(" + String.valueOf(MAX_SNO_LEN) + "), "
				+ " 	DATA			nvarchar(" + String.valueOf(MAX_DATA_LEN) + "), "
				+ " primary key(SNO) "
				+ " ) ";
		return sql;
	}

	//	各欄位最大長度
	public static final int MAX_TITLE_LEN = 300;
	public static final String MAX_DATA_LEN = "max";
	
	//	各欄位變數
	private String SNO_TITLE = "";
	//private String TITLE = null;			//	文章標題
	
	/**
	 * 產生新的SNO單號 (SNO_TITLE + YYYYmmdd + NNNNN)
	 * @param		obj				this(DMaker物件)
	 * @exception	Exception		throws Exception
	 */
	public void setNewSNO(Object obj) throws Exception{
		String SNO = Conn.getNewDayKey(obj, this.getTbName(), "SNO", this.SNO_TITLE);

		this.setSNO(SNO);
	}
	
	/**
	 * @deprecated
	 */
	public void setValue(String COL, String value) throws Exception {

		if("SNO".equals(COL)) { 
			setSNO(value);
		} else if("DATA".equals(COL)) { 
			setDATA(value);
		}

	}
	
	/**
	 * 依照EditorJs 所產出的JSON 資料內容產生相關DataBase 物件並放置容器中
	 * @param		obj				this(DMaker物件)
	 * @param		objData			JSON 資料
	 * @param		vContainer		物件容器
	 * @exception	Exception		throws Exception
	 */
	public void setContentByJSON(Object obj, Object objData, Vector vContainer) throws Exception {
		Hashtable htJSON = (Hashtable)objData;
		Vector vBlocks = (Vector)htJSON.get("blocks");
		vContainer.add(this);

		for(int i = 0; i < vBlocks.size(); i++) {
			Hashtable iHtBlock = (Hashtable)vBlocks.get(i);

			EditorJsContent iContent = new EditorJsContent(obj, this);
			iContent.setSNO( this.getSNO() );
			iContent.setINO( i );
			iContent.setContentByJSON(obj, iHtBlock, vContainer);
		}

		this.setDATA(convert.toJSON(htJSON));
	}

	public String getSNO_TITLE() {
		return this.SNO_TITLE;
	}
	
	/**
	 * @deprecated
	 */
	public String getValue(String COL) throws Exception {

		if("SNO".equals(COL)) { 
			return getSNO();
		} else if("DATA".equals(COL)) { 
			return getDATA();
		}

		return null;
	}

	/**
	 * 檢查primary key 是否設定
	 * @return			primary key 是否設定
	 */
	public boolean isKeySet()throws Exception{
		if( pCheck.checkValue(this.getSNO(), "NOTNULL")) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 取得該物件primary where 條件
	 * @return			primary where 條件
	 */
	public String getPrimaryWhere(talk t, String asName) {
		return ""
			+ " and " + asName + "SNO = '" + convert.ToSql(this.getSNO()) + "' ";
	}
	
	/**
	 * 依照設定primary key 查詢DB並設定各欄位數值至物件中
	 * @param		obj				this(DMaker物件)
	 * @exception	Exception		throws Exception
	 */
	public void query(Object obj)throws Exception{
		if(isKeySet()) {
			String[] arrCol = getTbColumn();
			talk t = Conn.getTalk(obj);
			DBQuerySql querySql = getQuerySql( getPrimaryWhere(t, "a.") );
			
			String[][] arrData = querySql.queryDB(obj);
			if(arrData.length == 1) {
				setObjectByHash(obj, this, arrData[0], arrCol);
			}
		}
	}
	
	/**
	 * @deprecated
	 */
	public void getInsertSql(talk t, Vector vSql) throws Exception{
		DBExecSql execSql = getExecSql();

		execSql.setCol( getTbColumn() );
		execSql.setValue( getInsertHash(t) );

		vSql.add( execSql.getInsertSql() );
	}
	
	/**
	 * @deprecated
	 */
	public void getDeleteSql(talk t, Vector vSql) throws Exception{
		if(isKeySet()) {
			String DBN = pDB.getDBN(t);

			String sqlWhere = getPrimaryWhere(t);

			vSql.add( getDeleteSql(sqlWhere) );
		}
	}

	/**
	 * @deprecated
	 */
	public void getUpdateSql(talk t, String[] arrCol, Vector vSql) throws Exception{
		if(isKeySet()) {
			DBExecSql execSql = getExecSql();

			execSql.setCol( getTbColumn() );
			execSql.setValue( getInsertHash(t) );
			
			vSql.add( execSql.getUpdateSql(arrCol, getPrimaryWhere(t)) );
		}
	}
	
	/**
	 * @deprecated
	 */
	public String getDeleteSql(String sqlWhere) {
		return getExecSql().getDeleteSql(sqlWhere);
	}

	/**
	 * 取得該類別查詢db 物件
	 * @param		sqlWhere			自訂where條件, like: and a.SNO = ''
	 * @return		DBQuerySql 物件
	 */
	public DBQuerySql getQuerySql(String sqlWhere) {
		return new DBQuerySql(getTbName(), getTbColumn("a."), "a")
			.setSelectWhere(sqlWhere);
	}
	
	/**
	 * @deprecated
	 */
	public DBExecSql getExecSql() {
		return new DBExecSql(getTbName());
	}
	
	/**
	 * @deprecated
	 */
	public static String[] getTbColumn() {
		return getTbColumn("");
	}

	public static String[] getTbColumn(String asName) {
		return new String[]{
			asName + "SNO",
			asName + "DATA"
		};
	}

	/**
	 * @deprecated
	 */
	public Hashtable getInsertHash(talk t)throws Exception{
		String DBN = pDB.getDBN(t);

		Hashtable ht = new Hashtable();
		ht.put("SNO", " '" + convert.ToSql(getSNO()) + "' ");
		ht.put("DATA", " " + DBN + "'" + convert.ToSql(getDATA()) + "' ");


		return ht;
	}
	
	/**
	 * 依照DBQuerySql 查詢相關EditorJs 物件資料
	 * @param		obj				this(DMaker物件)
	 * @param		querySql		查詢sql語法
	 * @param		arr				talk.queryFromPool parameter
	 * @return		EditorJs陣列
	 * @exception	Exception	throws Exception
	 */
	public EditorJs[] getObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		String[] arrCol = getTbColumn();
		String[][] arrData = querySql.queryDB(obj, arr);
		
		Vector vData = new Vector();
		for(int i = 0; i < arrData.length; i++) {
			EditorJs iEditorJs = new EditorJs(obj, this.getTbName());
			setObjectByHash(obj, iEditorJs, arrData[i], arrCol);
			vData.add(iEditorJs);
		}
		
		return (EditorJs[])vData.toArray(new EditorJs[0]);
	}

	/**
	 * 依照指定欄位取得物件hastable(依照primary key做分類)
	 * @param		obj				this(DMaker物件)
	 * @param		COL				指定欄位
	 * @param		arrCOL			talk.queryFromPool parameter
	 * @return		Hashtable
	 * @exception	Exception	throws Exception
	 */
	public Hashtable getHashObjectByCol(Object obj, String COL, String[] arrCOL) throws Exception{
		if(arrCOL == null || arrCOL.length == 0) {
			return new Hashtable();
		} else {
			DBQuerySql querySql = getQuerySql("");
			
			querySql.setSelectWhere( 
				" and " + querySql.getSqlInByCol(COL, arrCOL.length) 
			);
			return getHashObject(obj, querySql, arrCOL);
		}
	}
	
	/**
	 * 依照指定欄位取得物件hastable(依照primary key做分類)
	 * @param		obj				this(DMaker物件)
	 * @param		querySql		查詢sql語法
	 * @param		arr				talk.queryFromPool parameter
	 * @return		Hashtable
	 * @exception	Exception	throws Exception
	 */
	public Hashtable getHashObject(Object obj, DBQuerySql querySql, String[] arr) throws Exception{
		Hashtable htData = new Hashtable();
		EditorJs[] arrData = getObject(obj, querySql, arr);

		for(int i = 0; i < arrData.length; i++) {
			EditorJs iEditorJs = arrData[i];
			String iSNO = iEditorJs.getSNO();

			htData.put(iSNO, iEditorJs);
		}

		return htData;
	}

	/**
	 * @deprecated
	 */
	public Hashtable checkValue(Object obj, String col, int POSITION) throws Exception{
		Hashtable ht = new Hashtable();

		if("SNO".equals(col)) { 
			ht = checkValue_SNO(obj, POSITION);					//	單號
		} else if("DATA".equals(col)) { 
			ht = checkValue_DATA(obj, POSITION);				//	資料
		}

		return ht;
	}
	
	/**
	 * @deprecated
	 */
	public Hashtable checkValue_SNO(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};
		
		String MAXLEN = String.valueOf(EditorJs.MAX_SNO_LEN);
		if(!pCheck.checkValue(getSNO(), "NOTNULL")) {
			msg = "單號不可空白";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "ISEN")) {
			msg = "單號只可輸入英數字";
			invalid = true;
		} else if(!pCheck.checkValue(getSNO(), "MAXLENGTH:" + MAXLEN)) {
			msg = "單號最多%1碼";
			change = new String[]{MAXLEN};
			invalid = true;
		}
	
		return MVC.createFieldResult(msg, invalid, change);
	}

	/**
	 * @deprecated
	 */
	public Hashtable checkValue_DATA(Object obj, int POSITION) throws Exception{
		String msg = "";
		boolean invalid = false;
		String[] change = new String[]{};

		if(!pCheck.checkValue(getDATA(), "NOTNULL")) {
			msg = "內容不可空白";
			invalid = true;
		}

		return MVC.createFieldResult(msg, invalid, change);
	}


	/**
	 * 檢查DB 中是否有相關聯table，若沒有則自動create 至DB 中
	 * @param		obj				this(DMaker物件)
	 * @exception	Exception	throws Exception
	 */
	public void setDefaultEditorJsDB(Object obj) throws Exception{
		talk t = Conn.getTalk(obj);

		String[][] arrCol = t.getColumnsFromPool(this.getTbName());
		if(arrCol.length == 0) {
			DataBaseEditorJs[] arrDataBaseEditorJs = getAllEditorJsModel(obj, this);
	
			Vector vCreateSql = new Vector();
			for(int i = 0; i < arrDataBaseEditorJs.length; i++) {
				DataBaseEditorJs iDataBaseEditorJs = arrDataBaseEditorJs[i];

				vCreateSql.add( iDataBaseEditorJs.getCreateSql(t) );
			}
			
			t.execFromPool((String[])vCreateSql.toArray(new String[0]));
		}
	}
	
	/**
	 * 取得該筆單據相關 delete sql 語法並放置sql 容器中
	 * @param		obj				this(DMaker物件)
	 * @param		vSql			sql 資料
	 * @exception	Exception		throws Exception
	 */
	public void getDeleteArticleSql(Object obj, Vector vSql) throws Exception{
		talk t  = Conn.getTalk(obj);
		DataBaseEditorJs[] arrEditorJs = getAllEditorJsModel(obj, this);
		
		String sqlWhere = getPrimaryWhere(t);
		for(int i = 0; i < arrEditorJs.length; i++) {
			DataBaseEditorJs iEditorJs = arrEditorJs[i];

			vSql.add( iEditorJs.getDeleteSql(sqlWhere) );
		}
	}
	
	/**
	 * 取得該筆單據詳細資訊 (JSON)並放置容器中
	 * @param		obj				this(DMaker物件)
	 * @param		htData			容器
	 * @exception	Exception		throws Exception
	 */
	public Hashtable getContent(Object obj, Hashtable htData) throws Exception{
		
		Vector vBlocks = new Vector();
		htData.put("blocks", vBlocks);

		EditorJsContent myContent = new EditorJsContent(obj, this);

		EditorJsContent[] arrContent = null;
		Hashtable htType = new Hashtable();
		{
			DBQuerySql querySql = myContent.getQuerySql("");
			querySql.setSelectWhere( " and a.SNO = ? " );
			querySql.setSelectOrderBy(" order by a.SNO, a.INO ");
			
			arrContent = myContent.getObject(obj, querySql, new String[]{this.getSNO()});

			for(int i = 0; i < arrContent.length; i++) {
				EditorJsContent iContent = arrContent[i];
				String iINO = iContent.getSqlINO();
				String iITEMTYPE = iContent.getITEMTYPE();

				Vector iV = (Vector)htType.get(iITEMTYPE);
				if(iV == null) {
					iV = new Vector();
					htType.put(iITEMTYPE, iV);
				}	
				iV.add(iINO);
			}
		}
		

		//	header
		Hashtable htHeader = new Hashtable();
		if(htType.containsKey("header")) {	
			Vector vINO = (Vector)htType.get("header");
			EditorJsHeader myheader = new EditorJsHeader(obj, myContent);

			DBQuerySql querySql = myheader.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsHeader[] arrHeader = myheader.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrHeader.length; i++) {
				EditorJsHeader iHeader = arrHeader[i];
				int iINO = iHeader.getINO();
				
				htHeader.put(iINO, iHeader);
			}
		}
		
		//	checklist
		Hashtable htCheckList = new Hashtable();
		if(htType.containsKey("checklist")) {	
			Vector vINO = (Vector)htType.get("checklist");
			EditorJsCheckList myCheckList = new EditorJsCheckList(obj, myContent);

			DBQuerySql querySql = myCheckList.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsCheckList[] arrCheckList = myCheckList.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrCheckList.length; i++) {
				EditorJsCheckList iCheckLis = arrCheckList[i];
				int iINO = iCheckLis.getINO();
				
				htCheckList.put(iINO, iCheckLis);
			}
		}
		
		//	code
		Hashtable htCode = new Hashtable();
		if(htType.containsKey("code")) {	
			Vector vINO = (Vector)htType.get("code");
			EditorJsCode myCode = new EditorJsCode(obj, myContent);

			DBQuerySql querySql = myCode.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsCode[] arrCode = myCode.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrCode.length; i++) {
				EditorJsCode iCode = arrCode[i];
				int iINO = iCode.getINO();
				
				htCode.put(iINO, iCode);
			}
		}

		//	paragraph
		Hashtable htParagraph = new Hashtable();
		if(htType.containsKey("paragraph")) {	
			Vector vINO = (Vector)htType.get("paragraph");
			EditorJsParagraph myParagraph = new EditorJsParagraph(obj, myContent);

			DBQuerySql querySql = myParagraph.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsParagraph[] arrParagraph = myParagraph.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrParagraph.length; i++) {
				EditorJsParagraph iParagraph = arrParagraph[i];
				int iINO = iParagraph.getINO();
				
				htParagraph.put(iINO, iParagraph);
			}
		}

		//	delimiter
		Hashtable htDelimiter = new Hashtable();
		if(htType.containsKey("delimiter")) {	
			Vector vINO = (Vector)htType.get("delimiter");
			EditorJsDelimiter myDelimiter = new EditorJsDelimiter(obj, myContent);

			DBQuerySql querySql = myDelimiter.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsDelimiter[] arrDelimiter = myDelimiter.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrDelimiter.length; i++) {
				EditorJsDelimiter iDelimiter = arrDelimiter[i];
				int iINO = iDelimiter.getINO();
				
				htDelimiter.put(iINO, iDelimiter);
			}
		}
		
		//	list
		Hashtable htList = new Hashtable();
		if(htType.containsKey("list")) {	
			Vector vINO = (Vector)htType.get("list");
			EditorJsList myList = new EditorJsList(obj, myContent);

			DBQuerySql querySql = myList.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsList[] arrList = myList.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrList.length; i++) {
				EditorJsList iList = arrList[i];
				int iINO = iList.getINO();
				
				htList.put(iINO, iList);
			}
		}


		//	table
		Hashtable htTable = new Hashtable();
		if(htType.containsKey("table")) {	
			Vector vINO = (Vector)htType.get("table");
			EditorJsTable myTable = new EditorJsTable(obj, myContent);

			DBQuerySql querySql = myTable.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsTable[] arrTable = myTable.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrTable.length; i++) {
				EditorJsTable iTable = arrTable[i];
				int iINO = iTable.getINO();
				
				htTable.put(iINO, iTable);
			}
		}


		//	image
		Hashtable htImage = new Hashtable();
		if(htType.containsKey("image")) {	
			Vector vINO = (Vector)htType.get("image");
			EditorJsImage myImage = new EditorJsImage(obj, myContent);

			DBQuerySql querySql = myImage.getQuerySql("");

			String sqlWhere = ""
					+ " and a.SNO = '" + convert.ToSql(this.getSNO()) + "' "
					+ " and " + querySql.getSqlInByCol("INO", vINO.size());
			querySql.setSelectWhere(sqlWhere);

			EditorJsImage[] arrImage = myImage.getObject(obj, querySql, (String[])vINO.toArray(new String[0]));

			for(int i = 0; i < arrImage.length; i++) {
				EditorJsImage iImage = arrImage[i];
				int iINO = iImage.getINO();
				
				htImage.put(iINO, iImage);
			}
		}

		
		for(int i = 0; i < arrContent.length; i++) {
			EditorJsContent iContent = arrContent[i];
			int iINO = iContent.getINO();
			String iITEMTYPE = iContent.getITEMTYPE();
			Hashtable iHtData = new Hashtable();

			DataBaseEditorJsBlock iBlock = null;
			if("header".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htHeader.get(iINO);
			} else if("checklist".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htCheckList.get(iINO);
			} else if("code".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htCode.get(iINO);
			} else if("paragraph".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htParagraph.get(iINO);
			} else if("delimiter".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htDelimiter.get(iINO);
			} else if("list".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htList.get(iINO);
			} else if("table".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htTable.get(iINO);
			} else if("image".equals(iITEMTYPE)) {
				iBlock = (DataBaseEditorJsBlock)htImage.get(iINO);
			}


			if(iBlock != null) {
				iHtData.put("type", iITEMTYPE);
				iBlock.getContent(obj, iHtData);
				vBlocks.add(iHtData);
			}
		}


		return htData;
	}
	
	/**
	 * @deprecated
	 */
	public static DataBaseEditorJs[] getAllEditorJsModel(Object obj, EditorJs myEditorJs) {
		
		//	content
		EditorJsContent myContent = new EditorJsContent(obj, myEditorJs);

		//	checklist
		EditorJsCheckList myCheckList = new EditorJsCheckList(obj, myContent);
		EditorJsCheckList_Item myCheckList_Item = new EditorJsCheckList_Item(obj, myCheckList);

		//	code
		EditorJsCode myCode = new EditorJsCode(obj, myContent);

		
		//	header
		EditorJsHeader myHeader = new EditorJsHeader(obj, myContent);

		//	paragraph
		EditorJsParagraph myParagraph = new EditorJsParagraph(obj, myContent);

		//	delimiter
		EditorJsDelimiter myDelimiter = new EditorJsDelimiter(obj, myContent);

		//	list
		EditorJsList myList = new EditorJsList(obj, myContent);
		EditorJsList_Item myList_Item = new EditorJsList_Item(obj, myList);

		//	table
		EditorJsTable myTable = new EditorJsTable(obj, myContent);
		EditorJsTable_Cell myTable_Cell = new EditorJsTable_Cell(obj, myTable);

		//	image
		EditorJsImage myImage = new EditorJsImage(obj, myContent);
		EditorJsImage_File myImage_File = new EditorJsImage_File(obj, myImage);

		
		return new DataBaseEditorJs[]{
			myCheckList,
			myCheckList_Item,
			myCode,
			myContent,
			myEditorJs,
			myHeader,
			myParagraph,
			myDelimiter,
			myList,
			myList_Item,
			myTable,
			myTable_Cell,
			myImage,
			myImage_File
		};
	}

}

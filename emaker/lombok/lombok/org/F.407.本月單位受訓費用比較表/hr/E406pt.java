/*
 version	modify_date		modify_user		description
 1.00		20220302		KEVIN			修改CPNYID.SUPER
*/
package hr;
import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;
/*
		程式設計師：PEGGY
		程式功能：E.6.6.年度單位受訓費用總表(E5006)_查詢按鈕程式
		區域變數：
		函式：
		用到的外部程式：
		修改記錄：
			2005031101PEGGY：初次上線
			20141201 JENNY:修改程式內容，舊的請參考 action_20141201old
*/
public class E406pt extends bTransaction{
	public boolean action(String value)throws Throwable{
		// 回傳值為 true 表示執行接下來的資料庫異動或查詢
		// 回傳值為 false 表示接下來不執行任何指令
		// 傳入值 value 為 "新增","查詢","修改","刪除","列印","PRINT" (列印預覽的列印按鈕),"PRINTALL" (列印預覽的全部列印按鈕) 其中之一
		message("");
		
		//查詢
		if ("查詢".equals(value)){
			StringBuffer sbSQL = new StringBuffer();
			String sql = "";
			String YYMM = getQueryValue("YYMM").trim();    //年度
			String YYMM1 = YYMM;
			String CPNYID = getQueryValue("CPNYID");
			//20190103 DANIEL PATRICK begin:單位複選
			String DEPT_NO = getQueryValue("DEPT_NO");
			if(DEPT_NO.length() > 0){
				DEPT_NO = DEPT_NO.replace("'","");
				DEPT_NO = DEPT_NO.replace(",","','");
			}
			//20190103 DANIEL PATRICK end:單位複選
			//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
			//String SUPERA = (String)get("CPNYID.SUPERA");
			String SUPERA = (String)get("EDUCATION.SUPERA");
			//20220302 KEVIN ELIZA end:修改CPNYID.SUPER
			//String CPNYID = (String)get("iCPNYID");  //公司別
			String DATELIST = (String)get("SYSTEM.DATELIST");
			talk t = getTalk();
			setTableData("table1", new String[][]{});
			String DBTYPE = (String)get("SYSTEM.DBTYPE");

			if(DBTYPE==null) DBTYPE = "M";

			String FLAGD = "+";
			if(DBTYPE.trim().equals("O")) FLAGD = "||";
			//檢查年度
			if (YYMM.length() == 0){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("查詢年月不可空白!"));
				message(hr.Msg.hrm8b_F407.Msg001);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			if (DATELIST.equals("A") && !check.isRocDay(YYMM+"01"))
			{
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("年月格式輸入錯誤(正確格式yymm)"));
				message(hr.Msg.hrm8b_F407.Msg002);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			else if (DATELIST.equals("B") && !check.isACDay(YYMM+"01"))
			{
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("年月格式輸入錯誤(正確格式YYYYmm)"));
				message(hr.Msg.hrm8b_F407.Msg003);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			
			setValue("YYMM" ,YYMM);
			//20141201 JENNY LULU:查詢資料是月，所以要加191100
			//if(DATELIST.equals("A")) YYMM1 = operation.floatAdd(YYMM1,"1911",0);
			if(DATELIST.equals("A")) YYMM1 = operation.floatAdd(YYMM1,"191100",0);
			
			//先讀取部門資料
			Hashtable htDEPT_NAME = new Hashtable();
			//20190103 DANIEL PATRICK begin:單位複選
			//sql = " select DEP_NO , DEP_NAME, DEP_CODE from HRUSER_DEPT_BAS where DEP_NO<100000 and CPNYID = '" + convert.ToSql(CPNYID) + "' order by DEP_NO";
			//20190719 DANIEL PATRICK begin:顯示停用
			//sql = " select DEP_NO , DEP_NAME, DEP_CODE "
			sql = " select DEP_NO , DEP_NAME, DEP_CODE, DEP_DISABLE "
			//20190719 DANIEL PATRICK end:顯示停用
				+ " from HRUSER_DEPT_BAS "
				+ " where DEP_NO<100000 "
				+ " and CPNYID = '" + convert.ToSql(CPNYID) + "' ";
			if(DEPT_NO.length() > 0){
				sql+= " and DEP_NO in ('" + DEPT_NO + "') ";
			}
			sql+= " order by DEP_NO";
			//20190103 DANIEL PATRICK end:單位複選
			String rent[][] = t.queryFromPool(sql);
			if (rent.length == 0){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("查無部門基本資料!"));
				message(hr.Msg.hrm8b_F407.Msg004);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			//20141201 JENNY LULU :每個單位的時數與費用，KEY值用陣列接出來
			//[0]上課費用(公司) , [1]上課費用(個人) , [2]總時數
			String[] arrShowKey = new String[]{"@COMP_PAY" , "@USER_PAY" , "@TOT_HR"};

			//20141201 JENNY LULU begin:修改作法，合併成一個迴圈
			Hashtable deptRow = new Hashtable();
			Vector vDEPT = new Vector();//要放回表格用
			for (int i=0; i<rent.length; i++){
				String iDEP_NO = rent[i][0].trim();
				String iDEP_NAME = rent[i][1].trim();
				String iDEP_CODE = rent[i][2].trim();
				//20190719 DANIEL PATRICK begin:顯示停用
				String iDEP_DISABLE = rent[i][3].trim();
				if("Y".equals(iDEP_DISABLE)){
					iDEP_NAME+= "("+translate("停用")+")";
				}
				//20190719 DANIEL PATRICK end:顯示停用
				htDEPT_NAME.put(iDEP_NO , iDEP_CODE + " " + iDEP_NAME);//最後要秀出來的部門名稱
				deptRow.put(iDEP_NO , "" + i);//部門的順序
				//length = 6 [0]放部門代碼 , 後面的數字都先放0
				vDEPT.addElement(new String[]{iDEP_NO , "0" , "0" , "0" , "0" , "0"});
			}
			//最後一筆就放總計 , 後面的數字都先放0
			vDEPT.addElement(new String[]{translate("總計") , "0" , "0" , "0" , "0" , "0"});
			
			String newTab[][] = (String[][]) vDEPT.toArray(new String[0][0]);

			//讀取資料
			//外訓申請檔
			sbSQL.setLength(0);
			sbSQL.append(" select c.DEPT_NO , isnull(sum(a.TOT_HR),0) , isnull(sum(a.COMP_PAY),0)");
			sbSQL.append(" , isnull(sum(a.USER_PAY),0) , isnull(sum(a.COMP_PAY),0)+isnull(sum(a.USER_PAY),0)");
			sbSQL.append(" from PQ32 a,HRUSER c");
			sbSQL.append(" where a.EMPID = c.EMPID");
			sbSQL.append(" and a.CPNYID = '" + convert.ToSql(CPNYID) + "'").append(SUPERA);
			//20190103 DANIEL PATRICK begin:單位複選
			if(DEPT_NO.length() > 0){
				sbSQL.append(" and c.DEPT_NO in ('" + DEPT_NO + "') ");
			}
			//20190103 DANIEL PATRICK end:單位複選
			sbSQL.append(" and a.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31'");
			sbSQL.append(" group by c.DEPT_NO");
			String rTab[][] = t.queryFromPool( sbSQL.toString() );

			//Hashtable htPQ32CompPay = new Hashtable();
			int iRow = 0;
			//20141201 JENNY LULU:總計的位置
			int iLength = newTab.length-1;
			for (int i = 0 ; i < rTab.length ; i++){
				//20141201 JENNY LULU begin:把值宣告出來用
				String iDEPT_NO = rTab[i][0].trim();//部門
				//20141201 JENNY LULU:增加try - catch
				//iRow = Integer.parseInt(convert.replace(""+deptRow.get(rTab[i][0].trim()),"null","-1"));
				try{
					iRow = Integer.parseInt(convert.replace(""+deptRow.get(iDEPT_NO),"null","-1"));
				}catch(Exception e){
					iRow = -1;
				}
				if (iRow < 0) continue;
				//要計算訓練的資料
				String iTOT_HR = rTab[i][1].trim();//上課時數
				String iCOMP_PAY = rTab[i][2].trim();//上課費用(公司)
				String iUSER_PAY = rTab[i][3].trim();//上課費用(個人)
				String iPAY = rTab[i][4].trim();//上課總費用
				//防呆一下，預防不是數字
				//TOT_HR	decimal(10,1)	null,	/*時數*/
				if(!pCheck.checkValue(iTOT_HR , "NUM:20,3")){
					iTOT_HR = "0";
				}
				//COMP_PAY	decimal(10,0)	null,	/*公司負擔*/
				if(!pCheck.checkValue(iCOMP_PAY , "NUM:20,3")){
					iCOMP_PAY = "0";
				}
				//USER_PAY	decimal(10,0)	null,	/*員工負擔*/
				if(!pCheck.checkValue(iUSER_PAY , "NUM:20,3")){
					iUSER_PAY = "0";
				}
				//上課總費用 = 公司負擔+員工負擔
				if(!pCheck.checkValue(iPAY , "NUM:20,3")){
					iPAY = "0";
				}

				//每個部門
				newTab[iRow][1] = operation.floatAdd(newTab[iRow][1], iTOT_HR, 10);//上課時數
				newTab[iRow][2] = operation.floatAdd(newTab[iRow][2], iCOMP_PAY, 10);//上課費用(公司)
				newTab[iRow][3] = operation.floatAdd(newTab[iRow][3], iUSER_PAY, 10);//上課費用(個人)
				newTab[iRow][4] = operation.floatAdd(newTab[iRow][4], iPAY, 10);//上課總費用
				//總計
				//20190507 DANIEL PATRICK begin:後面有做加總 這邊不用重複做
				//newTab[iLength][1] = operation.floatAdd(newTab[iLength][1], iTOT_HR, 10);//上課時數
				//newTab[iLength][2] = operation.floatAdd(newTab[iLength][2], iCOMP_PAY, 10);//上課費用(公司)
				//newTab[iLength][3] = operation.floatAdd(newTab[iLength][3], iUSER_PAY, 10);//上課費用(個人)
				//newTab[iLength][4] = operation.floatAdd(newTab[iLength][4], iPAY, 10);//上課總費用
				//20190507 DANIEL PATRICK end:後面有做加總 這邊不用重複做
				//20141201 JENNY LULU end:把值宣告出來用
			}
			
			String[][] data = newTab;
			//2011/10/21:edward:begin:Eline:公司負擔金額：捉取PQ22.COM_PAY但要再除以報名人數(不含請假和報名取消人員)四捨五入得到整數位再看這個單位有哪些人報名這個課程PQ23 把費用分攤到個人,再用單位做費用加總
			
			Hashtable hData = new Hashtable();
			//取得每個內訓的人數(扣除 報名取消與請假的)
			sbSQL.setLength(0);
			sbSQL.append(" select distinct count(*) , LESS_KEY from PQ23");
			sbSQL.append(" where CPNYID = '"+convert.ToSql(CPNYID)+"'");
			sbSQL.append(" and STUDY_CLOSE not in ('1' , '2')");//結業註記0結業1報名取消2請假3未結業
			sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31')");
			sbSQL.append(" group by LESS_KEY");
			String[][] ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String iCount = ret[i][0].trim();//人數
				String iLESS_KEY = ret[i][1].trim();//上課編號
				String key = iLESS_KEY + "@LESS_KEY";
				hData.put(key , iCount);
			}
		
			//PQ22 內訓開課主檔
			sbSQL.setLength(0);
			sbSQL.append(" select isnull(sum(a.COMP_PAY),0) , a.LESS_KEY ");
			sbSQL.append(" , isnull(sum(a.USER_PAY),0) , isnull(sum(a.TOT_HR),0)");
			sbSQL.append(" from PQ22 a");
			sbSQL.append(" where a.CPNYID = '"+ convert.ToSql(CPNYID) +"'").append(SUPERA);
			sbSQL.append(" and a.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31'");
			sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22_CLOSE x)");
			sbSQL.append(" group by a.LESS_KEY");
			//ret = t.queryFromPool("select COMP_PAY , LESS_KEY , USER_PAY  from PQ22 where CPNYID = '"+convert.ToSql(CPNYID)+"' and SDATE like '" + convert.ToSql(YYMM1) + "%'");
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String iLESS_KEY = ret[i][1].trim();//課程編號

				String key1 = iLESS_KEY + "@LESS_KEY";
				if ( hData.containsKey(key1) ){
					String iCOMP_PAY = ret[i][0].trim();//上課費用(公司)
					String iUSER_PAY = ret[i][2].trim();//上課費用(個人)
					String iTOT_HR = ret[i][3].trim();//總時數

					String pCount = (String)hData.get(key1);//取得上課人數
					try{
						iCOMP_PAY = operation.floatDivide(iCOMP_PAY , pCount , 10);
					}catch (Exception e){}
					if ( iCOMP_PAY == null ){
						iCOMP_PAY = "0";
					}

					//arrShowKey[0]上課費用(公司) , [1]上課費用(個人) , [2]總時數
					//上課費用(公司)
					String key2 = iLESS_KEY + arrShowKey[0];
					hData.put(key2 , iCOMP_PAY);
					//上課費用(個人)
					String key3 = iLESS_KEY + arrShowKey[1];
					hData.put(key3 , iUSER_PAY);
					//總時數
					String key4 = iLESS_KEY + arrShowKey[2];
					hData.put(key4 , iTOT_HR);
				}
			}
			//取出 公司別裡的所有員工與該員工的部門
			Hashtable htHRUSER = new Hashtable();
			sbSQL.setLength(0);
			sbSQL.append(" select EMPID , DEPT_NO from HRUSER");
			sbSQL.append(" where CPNYID = '"+ convert.ToSql(CPNYID) +"'");
			//20190103 DANIEL PATRICK begin:單位複選
			if(DEPT_NO.length() > 0){
				sbSQL.append(" and DEPT_NO in ('" + DEPT_NO + "') ");
			}
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String iEMPID = ret[i][0].trim();
				String iDEPT_NO = ret[i][1].trim();
				htHRUSER.put(iEMPID , iDEPT_NO);
			}
			//20190103 DANIEL PATRICK end:單位複選
			//取出員工有上課的課程
			Hashtable htLESS_KEY = new Hashtable();//每個課程有幾個人 , 裡面再放一個Hashtable 放各部門
			Hashtable DEP_COUNT = new Hashtable();
			sbSQL.setLength(0);
			sbSQL.append(" select distinct a.EMPID , a.LESS_KEY");
			sbSQL.append(" from PQ23 a , HRUSER b");
			//sbSQL.append(" where a.CPNYID = '"+convert.ToSql(CPNYID)+"'");
			sbSQL.append(" where b.CPNYID = '"+convert.ToSql(CPNYID)+"'");
			sbSQL.append(" and a.EMPID = b.EMPID");
			//20190103 DANIEL PATRICK begin:單位複選
			if(DEPT_NO.length() > 0){
				sbSQL.append(" and b.DEPT_NO in ('" + DEPT_NO + "') ");
			}
			//20190103 DANIEL PATRICK end:單位複選
			sbSQL.append(" and a.STUDY_CLOSE not in ('1' , '2')");//結業註記0結業1報名取消2請假3未結業
			sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31')");
			ret = t.queryFromPool( sbSQL.toString() );
			int counterr = 0;
			for (int i=0;i<ret.length;i++){
				//先記錄每個部門參與此課程的人數
				String iEMPID = ret[i][0].trim();
				String iLESS_KEY = ret[i][1].trim();
	
				String iDEPT_NO = (String)htHRUSER.get( iEMPID );//取出員工的部門
				if ( iDEPT_NO != null ){
					//計算每個課程 各個部門佔了幾個人
					if ( htLESS_KEY.containsKey(iLESS_KEY) ){
						DEP_COUNT = (Hashtable)htLESS_KEY.get( iLESS_KEY );//先取出課程
						if ( DEP_COUNT == null ){
							continue;
						}
						//計算該課程的部門人數
						if ( DEP_COUNT.containsKey(iDEPT_NO) ){
							String tmp = (String)DEP_COUNT.get(iDEPT_NO);//取出該課程 的部門有幾個人
							DEP_COUNT.put(iDEPT_NO , operation.add(tmp , "1"));
						}else{
							DEP_COUNT.put(iDEPT_NO , "1");
						}
						htLESS_KEY.put(iLESS_KEY , DEP_COUNT);

					}else{
						DEP_COUNT = new Hashtable();
						DEP_COUNT.put(iDEPT_NO , "1");//部門+1
						htLESS_KEY.put(iLESS_KEY , DEP_COUNT);//課程 裡 放部門數量
					}
				}
			}
			//計算每個課程的上課費用和時數
			Hashtable htChk = new Hashtable();
			for (int i=0;i<ret.length;i++){
				String LESS_KEY = ret[i][1].trim();//課程編號

				if ( htChk.containsKey(LESS_KEY) ){
					continue;
				}else{
					htChk.put(LESS_KEY , "");
				}
				
				//取出有上到該課程編號的部門
				DEP_COUNT = (Hashtable)htLESS_KEY.get(LESS_KEY);
				if ( DEP_COUNT == null ){
					continue;
				}
				Vector vKey = new Vector(DEP_COUNT.keySet());
				for (int j=0; j<vKey.size(); j++){
					String jDEPT_NO = (String)vKey.elementAt(j);//部門代號
					String DEP_COUNTR = (String)DEP_COUNT.get(jDEPT_NO);//每個課程的部門的人數
					if ( DEP_COUNTR == null ){//應該是不會
						continue;
					}

					//[0]上課費用(公司) , [1]上課費用(個人) , [2]總時數
					for(int k = 0 ; k < arrShowKey.length ; k++){
						//取出課程的費用時數等資料的KEY值   EX:I00201409160001@COMP_PAY or I00201409160001@USER_PAY...
						String key_LESSKEY = LESS_KEY + arrShowKey[k];
						if ( hData.containsKey(key_LESSKEY) ){
							//取出部門的上課 費用 or 時數等資料的KEY值   EX:6@DEPT@COMP_PAY or 6@DEPT@USER_PAY...
							String key_DEPT = jDEPT_NO + "@DEPT" + arrShowKey[k];

							//取出部門的上課 費用 or 時數等資料
							String kPayHr = (String)hData.get(key_LESSKEY);

							//該課程的部門 的值(費用或時數) = 取出的值(費用或時數) * 上課人數
							String kDEP_PAY = operation.floatMultiply(kPayHr , DEP_COUNTR , 10);

							//計算部門的 上課費用(公司)
							if ( hData.containsKey(key_DEPT) ){
								String DEP_ALL = (String)hData.get(key_DEPT);
								DEP_ALL = operation.floatAdd(DEP_ALL , kPayHr , 10);
								hData.put(key_DEPT , DEP_ALL);
							}else{
								hData.put(key_DEPT , kPayHr);
							}
						}
					}
				}
			}

			//20141201 JENNY LULU :每個單位的時數與費用的位置
			//[0]上課費用(公司) , [1]上課費用(個人) , [2]總時數
			Integer[] arrShowInt = new Integer[]{2 , 3 , 1};
			//要顯示的小數位,依欄位放
			//[0]單位(沒有用) , [1]總時數 , [2]上課費用(公司) , [3]上課費用(個人) , [4]上課總金額
			Integer[] arrShowPoint = new Integer[]{0 , 1 , 2 , 2 , 2};

			//上課費用等總數量
			//上課時數 , 上課費用(公司) , 上課費用(個人) , 上課總金額
			String[] arrTOT = new String[]{"0","0","0","0"};

			//data[i][0]=部門代號  後面都是0
			for (int i=0;i<data.length;i++){
				String iDEPT_NO = data[i][0].trim();//部門代號
				//[0]上課費用(公司) , [1]上課費用(個人) , [2]總時數
				//String[] arrShowKey = new String[]{"@COMP_PAY" , "@USER_PAY" , "@TOT_HR"};
				for(int j = 0 ; j < arrShowKey.length ; j++){
					String key_DEPT = iDEPT_NO + "@DEPT" + arrShowKey[j];//取出部門的上課 費用時數等資料的KEY值
					int jCount = arrShowInt[j];
					//取出資料
					if ( hData.containsKey(key_DEPT) ){
						String tmp = (String)hData.get(key_DEPT);
						if ( operation.compareTo(tmp , "0") != 0 ){
							data[i][jCount] = operation.floatAdd(data[i][jCount] , tmp , 10);
							
						}
					}
				}
				//部門的上課總金額 = 部門費用(公司)+部門費用(個人)
				data[i][4] = operation.add(data[i][2] , data[i][3] , 10);
				for(int j = 1 ; j <= 4 ; j++){
					String temp = "0";
					//如果不是最後一筆，要把數字加到總計去
					if (i != iLength){
						temp = data[i][j];
					}
					data[iLength][j] = operation.add(data[iLength][j] , temp , 10);
					//整理小數位
					int point = arrShowPoint[j];
					data[i][j] = operation.floatAdd(data[i][j] , "0" , point);
				}
				//顯示秀出來的部門名稱
				String DEPT_NAME = (String)htDEPT_NAME.get(iDEPT_NO);
				if ( DEPT_NAME != null ){
					data[i][0] = DEPT_NAME;
				}
			}
			//2011/10/21:edward:end
			setTableData("table1", data);
			setValue("CPNYID", CPNYID);
			return false;
		}
		return true;
	}

	//20141201 JENNY LULU:改寫，所以這裡只留著預防
	public boolean action_20141201old(String value)throws Throwable{
		message("");
		
		//查詢
		if (value.equals("查詢")){
			StringBuffer sbSQL = new StringBuffer();
			String sql = "";
			String YYMM = getQueryValue("YYMM").trim();    //年度
			String YYMM1 = YYMM;
			String CPNYID = getQueryValue("CPNYID");
			String SUPERA = (String) get("CPNYID.SUPERA");
			//String CPNYID = (String)get("iCPNYID");  //公司別
			String DATELIST = (String)get("SYSTEM.DATELIST");
			talk t = getTalk();
			setTableData("table1", new String[][]{});
			String DBTYPE = (String)get("SYSTEM.DBTYPE");

			if(DBTYPE==null) DBTYPE = "M";

			String FLAGD = "+";
			if(DBTYPE.trim().equals("O")) FLAGD = "||";
			//檢查年度
			if (YYMM.length() == 0){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("查詢年月不可空白!"));
				message(hr.Msg.hrm8b_F407.Msg001);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			if (DATELIST.equals("A") && !check.isRocDay(YYMM+"01"))
			{
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("年月格式輸入錯誤(正確格式yymm)"));
				message(hr.Msg.hrm8b_F407.Msg002);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			else if (DATELIST.equals("B") && !check.isACDay(YYMM+"01"))
			{
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("年月格式輸入錯誤(正確格式YYYYmm)"));
				message(hr.Msg.hrm8b_F407.Msg003);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			
			setValue("YYMM" ,YYMM);
			if(DATELIST.equals("A")) YYMM1 = operation.floatAdd(YYMM1,"1911",0);
			
			//先讀取部門資料
			Hashtable htDEPT_NAME = new Hashtable();
			sql = " select DEP_NO , DEP_NAME, DEP_CODE from HRUSER_DEPT_BAS where DEP_NO<100000 and CPNYID = '" + convert.ToSql(CPNYID) + "' order by DEP_NO";
			String rent[][] = t.queryFromPool(sql);
			if (rent.length == 0){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("查無部門基本資料!"));
				message(hr.Msg.hrm8b_F407.Msg004);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			
			for (int i=0; i<rent.length; i++){
				String DEP_NO = rent[i][0].trim();
				String DEP_NAME = rent[i][1].trim();
				String DEP_CODE = rent[i][2].trim();
				htDEPT_NAME.put(DEP_NO , DEP_CODE + " " + DEP_NAME);
			}
			
			Hashtable deptRow = new Hashtable();
			String newTab[][] = new String[rent.length+1][6];
			for (int i = 0 ; i < newTab.length ; i++){
				if ( i != newTab.length-1){
					newTab[i][0] = rent[i][0].trim(); //部門
					deptRow.put(newTab[i][0].trim(), "" + i);
				}else{
					newTab[i][0] = "總計：";
				}
				for (int j = 1 ; j < newTab[i].length ; j++){
					newTab[i][j] = "0";
				}
			}
			//讀取資料
/*
			sql = " select c.DEPT_NO"//0
			      + ",isnull(sum(a.TOT_HR),0)"//1
				  + ",isnull(sum(a.COMP_PAY),0)"//2
				  + ",isnull(sum(a.USER_PAY),0)"//3
				  + ",isnull(sum(a.COMP_PAY),0)+isnull(sum(a.USER_PAY),0)"//4
				  + ",1"//5
				  + " from PQ22 a, PQ23 b, HRUSER c"
				  + " where a.LESS_KEY = b.LESS_KEY"
				  + " and b.EMPID = c.EMPID and b.STUDY_CLOSE not in ('1' , '2')"
				  + " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
				  + " and a.SDATE like '" + convert.ToSql(YYMM1) + "%'"
				  + " group by c.DEPT_NO"
				 // + " \r\n union all \r\n"
				    + " \r\n union \r\n"
				  + " select c.DEPT_NO"//0
				  + ",isnull(sum(a.TOT_HR),0)"//1
				  + ",isnull(sum(a.COMP_PAY),0)"//2
				  + ",isnull(sum(a.USER_PAY),0)"//3
				  + ",isnull(sum(a.COMP_PAY),0)+isnull(sum(a.USER_PAY),0)"//4
				  + ",2"//5
				  + " from PQ32 a,HRUSER c"
				  + " where a.EMPID = c.EMPID"
				  + " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
				  + " and a.SDATE like '" + convert.ToSql(YYMM1) + "%'"
				  + " group by c.DEPT_NO"
				 // + " \r\n union all \r\n"
				  + " \r\n union \r\n"
				  + " select c.DEPT_NO"
				  + ",isnull(sum(a.TOT_HR),0)"
				  + ",isnull(sum(a.COMP_PAY),0)"
				  + ",isnull(sum(a.USER_PAY),0)"
				  + ",isnull(sum(a.COMP_PAY),0)+isnull(sum(a.USER_PAY),0)"
				  + ",3"
				  + " from PQ33 a,PQ33_D0 b,HRUSER c"
				  + " where a.LESS_KEY = b.LESS_KEY"
				  + " and b.EMPID = c.EMPID"
				  + " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
				  + " and a.SDATE like '" + convert.ToSql(YYMM1) + "%'"
				  + " group by c.DEPT_NO"
				  + " order by 1";
				  String rTab[][] = t.queryFromPool(sql);
*/
		   sbSQL.setLength(0);
		   sbSQL.append("select c.DEPT_NO , isnull(sum(a.TOT_HR),0) , isnull(sum(a.COMP_PAY),0)");
		   sbSQL.append(" , isnull(sum(a.USER_PAY),0) , isnull(sum(a.COMP_PAY),0)+isnull(sum(a.USER_PAY),0)");
		   sbSQL.append(" from PQ32 a,HRUSER c");
		   sbSQL.append(" where a.EMPID = c.EMPID");
		   sbSQL.append(" and a.CPNYID = '" + convert.ToSql(CPNYID) + "'").append(SUPERA);
		   sbSQL.append(" and a.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31'");
		   sbSQL.append(" group by c.DEPT_NO");
		   String rTab[][] = t.queryFromPool( sbSQL.toString() );

		    //Hashtable htPQ32CompPay = new Hashtable();
			int iRow = 0;
			for (int i = 0 ; i < rTab.length ; i++){
				iRow = Integer.parseInt(convert.replace(""+deptRow.get(rTab[i][0].trim()),"null","-1"));
				if (iRow < 0) continue;
				newTab[iRow][1] = operation.floatAdd(newTab[iRow][1].trim(), rTab[i][1].trim(), 10);
				newTab[iRow][2] = operation.floatAdd(newTab[iRow][2].trim(), rTab[i][2].trim(), 10);
/*
				//20120221 draco============================================================
				newTab[iRow][2] = operation.floatAdd(newTab[iRow][2].trim(), rTab[i][2].trim(), 0);
				if(rTab[i][5].trim().equals("2"))
				{
					htPQ32CompPay.put(rTab[i][0].trim(),rTab[i][2].trim());
				}
				else
				{
					newTab[iRow][2] = operation.floatAdd(newTab[iRow][2].trim(), rTab[i][2].trim(), 0);
				}
				//============================================================20120221 draco
*/
				newTab[iRow][3] = operation.floatAdd(newTab[iRow][3].trim(), rTab[i][3].trim(), 10);
				newTab[iRow][4] = operation.floatAdd(newTab[iRow][4].trim(), rTab[i][4].trim(), 10);
				newTab[newTab.length-1][1] = operation.floatAdd(newTab[newTab.length-1][1].trim(), rTab[i][1].trim(), 10);
				newTab[newTab.length-1][2] = operation.floatAdd(newTab[newTab.length-1][2].trim(), rTab[i][2].trim(), 10);
				newTab[newTab.length-1][3] = operation.floatAdd(newTab[newTab.length-1][3].trim(), rTab[i][3].trim(), 10);
				newTab[newTab.length-1][4] = operation.floatAdd(newTab[newTab.length-1][4].trim(), rTab[i][4].trim(), 10);
			}
			
			String[][] data = newTab;
//2011/10/21:edward:begin:Eline:公司負擔金額：捉取PQ22.COM_PAY但要再除以報名人數(不含請假和報名取消人員)四捨五入得到整數位再看這個單位有哪些人報名這個課程PQ23 把費用分攤到個人,再用單位做費用加總
			
			Hashtable hData = new Hashtable();

			sbSQL.setLength(0);
			sbSQL.append("select distinct count(*) , LESS_KEY from PQ23");
			sbSQL.append(" where CPNYID = '"+convert.ToSql(CPNYID)+"'");
			sbSQL.append(" and STUDY_CLOSE not in ('1' , '2')");
			sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31')");
			sbSQL.append(" group by LESS_KEY");
			String[][] ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String key = ret[i][1].trim() + "@LESS_KEY";
				hData.put(key , ret[i][0].trim());
			}
		
			sbSQL.setLength(0);
			sbSQL.append("select isnull(sum(a.COMP_PAY),0) , a.LESS_KEY , isnull(sum(a.USER_PAY),0) , isnull(sum(a.TOT_HR),0)");
			sbSQL.append(" from PQ22 a");
			sbSQL.append(" where a.CPNYID = '"+ convert.ToSql(CPNYID) +"'").append(SUPERA);
			sbSQL.append(" and a.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31'");
			sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22_CLOSE x)");
			sbSQL.append(" group by a.LESS_KEY");
//			ret = t.queryFromPool("select COMP_PAY , LESS_KEY , USER_PAY  from PQ22 where CPNYID = '"+convert.ToSql(CPNYID)+"' and SDATE like '" + convert.ToSql(YYMM1) + "%'");
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String key1 = ret[i][1].trim() + "@LESS_KEY";
				String COMP_PAY = ret[i][0].trim();
				if ( hData.containsKey(key1) ){
					String pCount = (String)hData.get(key1);
					try{
						COMP_PAY = operation.floatDivide(COMP_PAY , pCount , 10);
					}catch (Exception e){}
					if ( COMP_PAY == null ){
						COMP_PAY = "0";
					}
					String key2 = ret[i][1].trim() + "@COMP_PAY";
					hData.put(key2 , COMP_PAY);
					
					String key3 = ret[i][1].trim() + "@USER_PAY";
					hData.put(key3 , ret[i][2].trim());
					
					String key4 = ret[i][1].trim() + "@TOT_HR";
					hData.put(key4 , ret[i][3].trim());
				}
			}

			Hashtable htHRUSER = new Hashtable();
			sbSQL.setLength(0);
			sbSQL.append("select EMPID , DEPT_NO from HRUSER");
			sbSQL.append(" where CPNYID = '"+ convert.ToSql(CPNYID) +"'");
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String EMPID = ret[i][0].trim();
				String DEPT_NO = ret[i][1].trim();
				htHRUSER.put(EMPID , DEPT_NO);
			}
/*
			sbSQL.setLength(0);
			sbSQL.append("select count(a.EMPID) , b.DEPT_NO , a.LESS_KEY");
			sbSQL.append(" from PQ23 a , HRUSER b");
			sbSQL.append(" where a.EMPID = b.EMPID");
			sbSQL.append(" and a.CPNYID = '"+convert.ToSql(CPNYID)+"'");
			sbSQL.append(" and a.STUDY_CLOSE not in ('1' , '2')");
			sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31')");
			sbSQL.append(" group by b.DEPT_NO , a.LESS_KEY");
*/
			Hashtable htLESS_KEY = new Hashtable();
			Hashtable DEP_COUNT = new Hashtable();
			sbSQL.setLength(0);
			sbSQL.append("select distinct a.EMPID , a.LESS_KEY");
			sbSQL.append(" from PQ23 a , HRUSER b");
			sbSQL.append(" where a.CPNYID = '"+convert.ToSql(CPNYID)+"'");
			sbSQL.append(" and a.EMPID = b.EMPID");
			sbSQL.append(" and a.STUDY_CLOSE not in ('1' , '2')");
			sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMM1) + "01' and '" + convert.ToSql(YYMM1) + "31')");
			ret = t.queryFromPool( sbSQL.toString() );
			int counterr = 0;
			for (int i=0;i<ret.length;i++){
				
				//先記錄每個部門參與此課程的人數
				String EMPID = ret[i][0].trim();
				String LESS_KEY = ret[i][1].trim();
	
				String DEPT_NO = (String)htHRUSER.get( EMPID );
				if ( DEPT_NO != null ){
					if ( htLESS_KEY.containsKey(LESS_KEY) ){
						DEP_COUNT = (Hashtable)htLESS_KEY.get( LESS_KEY );
						if ( DEP_COUNT.containsKey(DEPT_NO) ){
							String tmp = (String)DEP_COUNT.get(DEPT_NO);
							DEP_COUNT.put(DEPT_NO , operation.add(tmp , "1"));
						}else{
							DEP_COUNT.put(DEPT_NO , "1");
						}
						htLESS_KEY.put(LESS_KEY , DEP_COUNT);

					}else{
						DEP_COUNT = new Hashtable();
						DEP_COUNT.put(DEPT_NO , "1");
						htLESS_KEY.put(LESS_KEY , DEP_COUNT);
					}
				}
			}
			
			Hashtable htChk = new Hashtable();
			for (int i=0;i<ret.length;i++){
				String LESS_KEY = ret[i][1].trim();

				if ( htChk.containsKey(LESS_KEY) ){
					continue;
				}else{
					htChk.put(LESS_KEY , "");
				}
				
				String key1 = LESS_KEY + "@COMP_PAY";
				String key3 = LESS_KEY + "@USER_PAY";
				String key4 = LESS_KEY + "@TOT_HR";
				
				DEP_COUNT = (Hashtable)htLESS_KEY.get(LESS_KEY);
				if ( DEP_COUNT == null ){
					continue;
				}

				Vector vKey = new Vector(DEP_COUNT.keySet());
				for (int j=0; j<vKey.size(); j++){
					String jDEPT_NO = (String)vKey.elementAt(j);
					String DEP_COUNTR = (String)DEP_COUNT.get(jDEPT_NO);
					if ( DEP_COUNTR == null ){//應該是不會
						continue;
					}

					if ( hData.containsKey(key1) ){
						String jKey1 = jDEPT_NO + "@DEP_PAY";
						String COMP_PAY = (String)hData.get(key1);
						String DEP_PAY = operation.floatMultiply(COMP_PAY , DEP_COUNTR , 10);
						if ( hData.containsKey(jKey1) ){
							String DEP_PAY_ALL = (String)hData.get(jKey1);
							DEP_PAY_ALL = operation.floatAdd(DEP_PAY_ALL , DEP_PAY , 10);
							hData.put(jKey1 , DEP_PAY_ALL);
						}else{
							hData.put(jKey1 , DEP_PAY);
						}
					}
					
					if ( hData.containsKey(key3) ){
						String jKey2 = jDEPT_NO + "@USER_PAY";
						String USER_PAY = (String)hData.get(key3);
						String DEP_USER_PAY = operation.floatMultiply(USER_PAY , DEP_COUNTR , 10);
						if ( hData.containsKey(jKey2) ){
							String USER_PAY_ALL = (String)hData.get(jKey2);
							USER_PAY_ALL = operation.floatAdd(USER_PAY_ALL , DEP_USER_PAY , 10);
							hData.put(jKey2 , USER_PAY_ALL);
						}else{
							hData.put(jKey2 , DEP_USER_PAY);
						}
					}
					
					if ( hData.containsKey(key4) ){
						String jKey3 = jDEPT_NO + "@TOT_HR";
						String TOT_HR = (String)hData.get(key4);
						String DEP_TOT_HR = operation.floatMultiply(TOT_HR , DEP_COUNTR , 10);
						if ( hData.containsKey(jKey3) ){
							String TOT_HR_ALL = (String)hData.get(jKey3);
							TOT_HR_ALL = operation.floatAdd(TOT_HR_ALL , DEP_TOT_HR , 10);
							hData.put(jKey3 , TOT_HR_ALL);
						}else{
							hData.put(jKey3 , DEP_TOT_HR);
						}
					}
				}
			}

			String TOT1 = "0";//TOT_HR
			String TOT2 = "0";//COMP_PAY
			String TOT3 = "0";//USER_PAY
			String TOT4 = "0";//TOT_PAY
			for (int i=0;i<data.length;i++)
			{
					String key1 = data[i][0].trim() + "@DEP_PAY";
					String key2 = data[i][0].trim() + "@USER_PAY";
					String key3 = data[i][0].trim() + "@TOT_HR";

					if ( hData.containsKey(key1) ){
						String tmp = (String)hData.get(key1);
						if ( operation.compareTo(tmp , "0") != 0 ){
							data[i][2] = operation.floatAdd(data[i][2] , tmp , 10);
							
						}
					}

					if ( hData.containsKey(key2) ){
						String tmp = (String)hData.get(key2);
						if ( operation.compareTo(tmp , "0") != 0 ){
							data[i][3] = operation.floatAdd(data[i][3] , tmp , 10);
						}
					}
					
					if ( hData.containsKey(key3) ){
						String tmp = (String)hData.get(key3);
						if ( operation.compareTo(tmp , "0") != 0 ){
							data[i][1] = operation.floatAdd(data[i][1] , tmp , 10);
						}
					}

				data[i][4] = operation.add(data[i][2] , data[i][3] , 10);
				if (i == (newTab.length-1)){
					data[i][1] = operation.add(TOT1 , "0" , 10);
					data[i][2] = operation.add(TOT2 , "0" , 10);
					data[i][3] = operation.add(TOT3 , "0" , 10);
					data[i][4] = operation.add(TOT4 , "0" , 10);
				}else{
					TOT1 = operation.add(TOT1 , data[i][1] , 10);
					TOT2 = operation.add(TOT2 , data[i][2] , 10);
					TOT3 = operation.add(TOT3 , data[i][3] , 10);
					TOT4 = operation.add(TOT4 , data[i][4] , 10);

				}
			}

			for (int i=0; i<data.length; i++){
				String DEPT_NO = data[i][0].trim();
				String DEPT_NAME = (String)htDEPT_NAME.get(DEPT_NO);
				if ( DEPT_NAME != null ){
					data[i][0] = DEPT_NAME;
				}
				
				data[i][1] = operation.floatAdd(data[i][1] , "0" , 1);
				data[i][2] = operation.floatAdd(data[i][2] , "0" , 2);
				data[i][3] = operation.floatAdd(data[i][3] , "0" , 2);
				data[i][4] = operation.floatAdd(data[i][4] , "0" , 2);
			}
//2011/10/21:edward:end
			setTableData("table1", data);
			setValue("CPNYID", CPNYID);
			return false;
		}
		return true;
	}
	public String getInformation(){
		return "---------------\u67e5\u8a62\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
	}
}

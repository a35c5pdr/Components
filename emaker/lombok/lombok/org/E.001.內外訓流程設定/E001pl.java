/*
	version		modify_date		modify_user		description
	1.00		20220301		ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
package hr;
import javax.swing.*;
import jcx.jform.bproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

public class E001pl extends bproc{
	String tablename="RULT7D";
	void add_new_record(String CPNYID,String FLOWNO,String TYPES) throws Exception{
		String ISNULL = (String)get("SYSTEM.ISNULL");
		if(ISNULL==null) ISNULL = "";
		ISNULL = (getTalk().f_db_type.equals("oracle")?"ISNULL":"");

		String ret[][]=null;

		String today=getToday("YYYYmmdd");
		String time=getTime("hms");
		Vector SQL=new Vector();
//		for(int i=0;i<ret.length;i++){
			StringBuffer sb=new StringBuffer();
			sb.append("insert into "+tablename+" (CPNYID,FLOWNO,TYPES,APPROVE,SIGN1,SIGN2,SIGN3,SIGN4,SIGN5,SIGN6,SIGN7,SIGN8,SIGN9,SIGN10,SIGN11,SIGN12,SIGN13,SIGN14,SIGN15,SIGN16)");
			sb.append(" select '"+CPNYID+"',"+(getTalk().f_db_type.equals("mssql")?"N":"")+"'"+FLOWNO+"','"+TYPES+"',APPROVE,SIGN1,SIGN2,SIGN3,SIGN4,SIGN5,SIGN6,SIGN7,SIGN8,SIGN9,SIGN10,SIGN11,SIGN12,SIGN13,SIGN14,SIGN15,SIGN16 ");
			sb.append(" from "+tablename+" where isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"' and isnull(FLOWNO,'"+ISNULL+"')='"+ISNULL+"' and isnull(TYPES,'"+ISNULL+"')='"+ISNULL+"'");
			SQL.addElement(sb.toString());
//		}

		getTalk().execFromPool((String[])SQL.toArray(new String[0]));

	}
	
	public void refresh_tree()throws Throwable{
		talk t = getTalk();
		String sql = "";
		sql = " select distinct CPNYID , FLOWNO , TYPES " 
			+ " from " + tablename+" where 1 = 1 " 
			//20220301 ANDYHOU begin:權限改使用EDUCATION.SUPER與CONDITION_B06
			//+ get("CPNYID.SUPER")
			+ get("EDUCATION.SUPER")
			//20220301 ANDYHOU end:權限改使用EDUCATION.SUPER與CONDITION_B06
			+ " order by CPNYID " 
			+ (getTalk().f_db_type.equals("oracle")?"NULLS FIRST":"")
			+ " , FLOWNO " + (getTalk().f_db_type.equals("oracle")?"NULLS FIRST":"") 
			+ ",TYPES "+(getTalk().f_db_type.equals("oracle")?"NULLS FIRST":"")+""
			;
		String[][] d=t.queryFromPool(sql);
		if(d.length==0){
			//20220301 ANDYHOU begin:權限改使用EDUCATION.SUPER與CONDITION_B06
			//if("".equals(get("CPNYID.SUPER"))){
			if("".equals(get("EDUCATION.SUPER"))){
			//20220301 ANDYHOU end:權限改使用EDUCATION.SUPER與CONDITION_B06
				t.execFromPool("insert into "+tablename+" (CPNYID,FLOWNO,TYPES) values('','','')");
				d=t.queryFromPool(sql);
			} else {
				
				//20190308 LICK COULSON begin 訊息翻譯
				//message("沒有您可以管理的公司設定");
				message(hr.Msg.hrm8b_E001.Msg001);
				//20190308 LICK COULSON end 訊息翻譯
			}
		}
		setTableData("TREE",d);
	}

	public String getDefaultValue(String value)throws Throwable{
		String ISNULL = (String)get("SYSTEM.ISNULL");
		if(ISNULL==null) ISNULL = "";

		ISNULL = (getTalk().f_db_type.equals("oracle")?"ISNULL":"");
		
		if(getName().equals("add")){
			int row=getTable("TREE").getSelectedRow();
			int col=getTable("TREE").getSelectedColumn();
			String data[][]=getTableData("TREE");
//System.err.println("row="+row+" col="+getTable("TREE").getSelectedColumn());
//			if(true) return value;
			if(col==0){
				String sql = "";
				talk t=getTalk();
				sql = " select distinct COCNAME , CPNYID " 
					+ " from COMPANY " 
					+ " where 1 = 1 " 
					//20220301 ANDYHOU begin:權限改使用EDUCATION.SUPER與CONDITION_B06
					//+ get("CPNYID.SUPER")
					+ get("EDUCATION.SUPER")
					//20220301 ANDYHOU end:權限改使用EDUCATION.SUPER與CONDITION_B06
					+ " and CPNYID not in (select CPNYID from " + tablename + "  where CPNYID is not null) " 
					+ " order by CPNYID ";
				String cc[][]=t.queryFromPool(sql);
				if(cc.length==0){
					
					//20190308 LICK COULSON begin 訊息翻譯
					//messagebox("目前所有可管理的公司都有設定過參數了，您必需先去 H.2.4 建立新的公司資料.");
					message(hr.Msg.hrm8b_E001.Msg002);
					//20190308 LICK COULSON end 訊息翻譯
					return value;
				}

				Vector vn=new Vector();
				for(int i=0;i<cc.length;i++) vn.addElement(cc[i][0]);


				String type1[]=(String[])vn.toArray(new String[0]);
				String default1=type1[0];
//				Object ret=JOptionPane.showInputDialog(getPanel(), "新增新的公司別特休參數\r\n請選擇公司別?", "建立參數",JOptionPane.OK_CANCEL_OPTION, null, type1,default1);
//				Object ret=showInputDialog( "新增新的公司別職稱內外訓流程參數\r\n請選擇公司別?", "建立參數", type1,default1);
				
				//20190308 LICK COULSON begin 訊息翻譯
				//Object ret=showInputDialog( "請選擇項目?", "建立參數", type1,default1);
				Object ret=showInputDialog( hr.Msg.hrm8b_E001.Msg003, "建立參數", type1,default1);
				//20190308 LICK COULSON end 訊息翻譯
				if(ret!=null){
						String NO1=null;
						for(int i=0;i<cc.length;i++){
							if(cc[i][0].equals(ret)){
								NO1=cc[i][1];
								break;
							}
						}
						if(NO1==null){
							
							//20190308 LICK COULSON begin 訊息翻譯
							//messagebox("找不到此公司別(不合理的錯誤)!");
							message(hr.Msg.hrm8b_E001.Msg004);
							//20190308 LICK COULSON end 訊息翻譯
							return value;
						}
						add_new_record(NO1,"","");
						

						refresh_tree();

				}

			} else if(col==1){
//				if(data[row][1].equals("")){
					String CPNYID=data[row][0];
					talk t=getTalk();
//					String cc[][]=t.queryFromPool("select distinct UTNAME,FLOWNO from USERTYPE where FLOWNO not in (select UTYPE from KSYSTEM where CPNYID='"+CPNYID+"' and UTYPE is not null) Order by UTYPE ");
//					if(cc.length==0){
//						messagebox("目前所有身份別都有設定過參數了，您可以修改現有設定或先去 H.2.12 建立新的身份別.");
//						return value;
//					}

//					Vector vn=new Vector();
//					for(int i=0;i<cc.length;i++) vn.addElement(cc[i][0]);

					if(CPNYID.equals("")){
						
						//20190308 LICK COULSON begin 訊息翻譯
						//messagebox("公司別空白的通用設定不可以建立別的組別. 請先建立新的公司!");
						message(hr.Msg.hrm8b_E001.Msg005);
						//20190308 LICK COULSON end 訊息翻譯
						return value;
					}

//					String type1[]=(String[])vn.toArray(new String[0]);
//					String default1=type1[0];
					String ITEMS[]=new String[0];
					ITEMS=hr.common5.getFLOWGROUP();
					for(int kk=0;kk<ITEMS.length;kk++) ITEMS[kk]=translate(ITEMS[kk]);
//					Object ret=JOptionPane.showInputDialog(getPanel(), "新增新的流程組別\r\n請選擇組別名稱?(不可以重覆或空白)", "建立組別",JOptionPane.OK_CANCEL_OPTION,null,ITEMS,"");
//					Object ret=showInputDialog( "新增新的流程組別\r\n請選擇組別名稱?(不可以重覆或空白)", "建立組別",ITEMS,"");
					
					//20190308 LICK COULSON begin 訊息翻譯
					//Object ret=showInputDialog( "請選擇組別名稱?(不可以重覆或空白)", "建立組別", ITEMS,"");
					Object ret=showInputDialog( hr.Msg.hrm8b_E001.Msg006, "建立組別", ITEMS,"");
					//20190308 LICK COULSON end 訊息翻譯
					if(ret!=null){
						if(ret.equals("")){
							
							//20190308 LICK COULSON begin 訊息翻譯
							//messagebox("這個組別名稱不可為空白，請再試一次.");
							message(hr.Msg.hrm8b_E001.Msg007);
							//20190308 LICK COULSON end 訊息翻譯
							return value;
						}
						for(int kk=0;kk<ITEMS.length;kk++){
							if(ret.equals(ITEMS[kk])){
								ret=hr.common5.getFLOWGROUP()[kk];
								break;
							}
						}

						String cc[][]=t.queryFromPool("select FLOWNO from "+tablename+" where CPNYID='"+CPNYID+"' and FLOWNO='"+ret+"'");
						if(cc.length!=0){
							
							//20190308 LICK COULSON begin 訊息翻譯
							//messagebox("這個組別名稱已使用過，請再試一次.");
							message(hr.Msg.hrm8b_E001.Msg008);
							//20190308 LICK COULSON end 訊息翻譯
							return value;
						}
						add_new_record(CPNYID,(String)ret,"");
						

						refresh_tree();

					}
			} else if(col==2){
					String CPNYID=data[row][0];
					String FLOWNO=data[row][1];
					talk t=getTalk();
//					String cc[][]=t.queryFromPool("select distinct UTNAME,FLOWNO from USERTYPE where FLOWNO not in (select UTYPE from KSYSTEM where CPNYID='"+CPNYID+"' and UTYPE is not null) Order by UTYPE ");
//					if(cc.length==0){
//						messagebox("目前所有身份別都有設定過參數了，您可以修改現有設定或先去 H.2.12 建立新的身份別.");
//						return value;
//					}

//					Vector vn=new Vector();
//					for(int i=0;i<cc.length;i++) vn.addElement(cc[i][0]);

//					if(CPNYID.equals("")){
//						messagebox("公司別空白的通用設定不可以建立別的組別. 請先建立新的公司!");
//						return value;
//					}

		
//					String type1[]=(String[])vn.toArray(new String[0]);
//					String default1=type1[0];
//					String ITEMS[]=new String[];
//					String ITEMS[]=new String[]{"","A.內訓","B.外訓"};
					String ITEMS[]=new String[]{"","A內訓(申請總金額)計劃內","B內訓(申請總金額)計劃外","C內訓(預算和實際差額)計劃內","D內訓(預算和實際差額)計劃外"
																,"E外訓(申請總金額)計劃內","F外訓(申請總金額)計劃外"};
					
//					Object ret=JOptionPane.showInputDialog(getPanel(), "新增新的流程異動項目\r\n請選擇異動項目名稱?(不可以重覆或空白)", "建立異動項目",JOptionPane.OK_CANCEL_OPTION,null,ITEMS,"");
//					Object ret=showInputDialog("新增新的流程異動項目\r\n請選擇異動項目名稱?(不可以重覆或空白)", "建立異動項目",ITEMS,"");
					
					//20190308 LICK COULSON begin 訊息翻譯
					//Object ret=showInputDialog("請選擇異動項目名稱?(不可以重覆或空白)", "建立異動項目",ITEMS,"");
					Object ret=showInputDialog(hr.Msg.hrm8b_E001.Msg009, "建立異動項目",ITEMS,"");
					//20190308 LICK COULSON end 訊息翻譯
					if(ret!=null){
						
						if(ret.equals("")){
							
							//20190308 LICK COULSON begin 訊息翻譯
							//messagebox("這個異動項目名稱不可為空白，請再試一次.");
							message(hr.Msg.hrm8b_E001.Msg010);
							//20190308 LICK COULSON end 訊息翻譯
							return value;
						}
						String ret1=(ret.toString()).substring(0,1);
						if(ret1.equals("")){
							//20190308 LICK COULSON begin 訊息翻譯
							//messagebox("這個異動項目名稱不可為空白，請再試一次.");
							message(hr.Msg.hrm8b_E001.Msg010);
							//20190308 LICK COULSON end 訊息翻譯
							return value;
						}

						String cc[][]=t.queryFromPool("select FLOWNO from "+tablename+" where CPNYID='"+CPNYID+"' and isnull(FLOWNO,'"+ISNULL+"')='"+(FLOWNO.equals("")?ISNULL:FLOWNO)+"' and TYPES='"+ret1+"'");
						if(cc.length!=0){
							
							//20190308 LICK COULSON begin 訊息翻譯
							//messagebox("這個異動項目名稱已使用過，請再試一次.");
							message(hr.Msg.hrm8b_E001.Msg011);
							//20190308 LICK COULSON end 訊息翻譯
							return value;
						}
						add_new_record(CPNYID,FLOWNO,(String)ret1);
						

						refresh_tree();

					}
			}
		} else if(getName().equals("del")){


			int row=getTable("TREE").getSelectedRow();
			String data[][]=getTableData("TREE");
			String CPNYID=data[row][0];
			String FLOWNO=data[row][1];
			String TYPES=data[row][2];
			if(FLOWNO.equals("") && TYPES.equals("")){
				
				//20190308 LICK COULSON begin 訊息翻譯
				//messagebox("每個公司的通用組別不能被刪除.");
				message(hr.Msg.hrm8b_E001.Msg012);
				//20190308 LICK COULSON end 訊息翻譯
//				return value;
			}

			String msg1="";
			String sql="";
			if(TYPES.equals("")){
				if(FLOWNO.equals("")){
					sql="select count(*) from HRUSER where CPNYID='"+CPNYID+"' and isnull(FLOWNO,'"+ISNULL+"')='"+ISNULL+"'";
				} else {
					sql="select count(*) from HRUSER where CPNYID='"+CPNYID+"' and FLOWNO='"+FLOWNO+"'";
				}


				String cc[][]=getTalk().queryFromPool(sql);
				if(!cc[0][0].equals("0")){
					
					//20190308 LICK COULSON begin 訊息翻譯
					//msg1=("這個組別名稱曾經被 "+cc[0][0]+" 個使用者使用，建議您先行保留這個組別.");
					msg1=translate(hr.Msg.hrm8b_E001.Msg013, new String[]{cc[0][0]});
					//20190308 LICK COULSON end 訊息翻譯
				}
			}


//			int ret=JOptionPane.showConfirmDialog(getPanel(),msg1+"請問您確定刪除這項設定嗎?","刪除確認",JOptionPane.YES_NO_OPTION);
			
			//20190308 LICK COULSON begin 訊息翻譯
			//int ret=showConfirmDialog(msg1+"請問您確定刪除這項設定嗎?","刪除確認",JOptionPane.YES_NO_OPTION);
			int ret=showConfirmDialog(msg1+translate(hr.Msg.hrm8b_E001.Msg014),"刪除確認",JOptionPane.YES_NO_OPTION);
			//20190308 LICK COULSON end 訊息翻譯
			if(ret==JOptionPane.YES_OPTION){
				sql="delete from "+tablename+" where CPNYID='"+CPNYID+"' and isnull(FLOWNO,'"+ISNULL+"')='"+(FLOWNO.equals("")?ISNULL:FLOWNO)+"' and isnull(TYPES,'"+ISNULL+"')='"+(TYPES.equals("")?ISNULL:TYPES)+"'";

				getTalk().execFromPool(sql);

				refresh_tree();
			}
		}
		String name=getName();
		if(POSITION==1){
			talk t=getTalk();
			if(name.equals("FORM_STATUS")){

				String sql="select count(*) from "+tablename+" where isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"' and isnull(FLOWNO,'"+ISNULL+"')='"+ISNULL+"' and isnull(TYPES,'"+ISNULL+"')='"+ISNULL+"'";
				String a[][]=t.queryFromPool(sql);
				if(a[0][0].equals("0")){
					t.execFromPool("insert into "+tablename+" (CPNYID,FLOWNO,TYPES,APPROVE) values('','','','')");
				}
				refresh_tree();

			}
		}
		return value;
	}
	public String getInformation(){
		return "---------------FORM_STATUS(FORM_STATUS).defaultValue()----------------";
	}
}

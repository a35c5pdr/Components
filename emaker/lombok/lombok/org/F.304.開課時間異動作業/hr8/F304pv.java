/*
	version	modify_date	modify_user	description
	1.00	20201130	LUCAS	增加學分欄位
	1.01	20220302	KEVIN	修改CPNYID.SUPER
*/
package hr8;
import javax.swing.*;
import jcx.jform.hproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

public class F304pv extends hproc{
	talk t = null;
	String sql = "";
	String[][] ret = null;
	String DATELIST = "";
	String value = "";
	public String action(String value)throws Throwable{
		// 可自定欄位檢核條件 
		// 傳入值 value 原輸入值 
		t = getTalk();
		DATELIST = (String)get("SYSTEM.DATELIST");
		this.value = value;
		String name = getName();
		if("PQ16NO".equals(name)){
			 PQ16NO();
		}
		return value;
	}

	private boolean PQ16NO() throws Exception{
		String PQ16NO = value;//開課編號
		//	20190108 MAX COULSON BEGIN:新增報名起迄日
		//sql = "select LESS_NO,SDATE,EDATE,TOT_HR,(select LESSNAME from PQ03 where PQ03.LESS_NO = PQ22.LESS_NO) "//1-4
		sql = "select LESS_NO,SDATE,EDATE,TOT_HR,(select LESSNAME from PQ03 where PQ03.LESS_NO = PQ22.LESS_NO) , ENTERSDATE , ENTEREDATE "//1-6
		//	20190108 MAX COULSON END:新增報名起迄日 
			+ " from PQ22 where LESS_KEY = '"+convert.ToSql(PQ16NO)+"' "
			//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
			//+ "and (1=1 "+(String) get("CPNYID.SUPER")+" or CPNYID is null or CPNYID='') ";
			+ "and (1=1 "+(String) get("EDUCATION.SUPER")+" or CPNYID is null or CPNYID='') ";
			//20220302 KEVIN ELIZA end:修改CPNYID.SUPER
		ret = t.queryFromPool(sql);
		if (ret.length != 0){
			String LESS_NO3 = ret[0][0].trim();	//課程編號
			String SDATE = ret[0][1].trim();	//起始日期
			String EDATE = ret[0][2].trim();	//結束日期
			String TOT_HR = ret[0][3].trim();	//總時數
			String LESS_NAME = ret[0][4].trim();//課程名稱
			//	20190108 MAX COULSON BEGIN:新增報名起迄日
			String ENTERSDATE = ret[0][5].trim();	//課程起日
			String ENTEREDATE = ret[0][6].trim();	//課程迄日
			//	20190108 MAX COULSON END:新增報名起迄日
			setValue("LESS_NO3",LESS_NO3);
			setValue("LESS_NAME",LESS_NAME);
			if (DATELIST.equals("A")){
				if(check.isACDay(SDATE)){
					SDATE = convert.ac2roc(SDATE);
				}
				if(check.isACDay(EDATE)){
					EDATE = convert.ac2roc(EDATE);
				}
				//	20190108 MAX COULSON BEGIN:新增報名起迄日
				if(check.isACDay(ENTERSDATE)){
					ENTERSDATE = convert.ac2roc(ENTERSDATE);
				}
				if(check.isACDay(ENTEREDATE)){
					ENTEREDATE = convert.ac2roc(ENTEREDATE);
				}
				//	20190108 MAX COULSON END:新增報名起迄日
			}
			
			setValue("SDATE",SDATE);
			setValue("EDATE",EDATE);
			//	20190108 MAX COULSON BEGIN:新增報名起迄日
			setValue("ENTERSDATE",ENTERSDATE);
			setValue("ENTEREDATE",ENTEREDATE);
			//	20190108 MAX COULSON END:新增報名起迄日
			setValue("TOT_HR",TOT_HR);
		}
		//開課明細副表
		sql = " select SKEY , TRAIN_DATE , STIME , ETIME , TRAIN_HR , TEACHER_FROM , TEACHER, "//0-6
			//20201130 LUCAS CANDY begin:增加學分欄位
			//+ " TRAIN_PLACE , TEACHFEE , CLSYN "//7-9
			+ " TRAIN_PLACE , TEACHFEE , CLSYN , CREDIT"//7-10
			//20201130 LUCAS CANDY end:增加學分欄位
			+ " from PQ22_D "
			+ " where LESS_KEY = '"+convert.ToSql(PQ16NO)+"'";
		ret = t.queryFromPool(sql);
		boolean isClose = false;
		for (int i=0;i<ret.length;i++){
			if(DATELIST.equals("A")){
				ret[i][1] = convert.ac2roc(ret[i][1].trim());
			}
			if(ret[i][9].trim().equals("Y")){
		
				isClose = true;
			}
		}
		setEditable("LESS_NO3",false);
		setTableData("table1",ret);
		if (isClose){
			for(int i = 0 ; i < ret.length ; i++){
				for(int j = 0 ; j < ret[i].length ; j++){
					addScript("EMC['table1'].setCellEditable('" + i + "','" + j + "'," + false + ");");
				}
			}
			setEditable("SDATE",false);
			setEditable("LESS_NO3",false);
			setEditable("EDATE",false);
			setEditable("TOT_HR",false);
			setEditable("NOTE",false);
			getButton(1).setVisible(false);
			//	20190108 MAX COULSON BEGIN:新增報名起迄日
			setEditable("ENTERSDATE",false);
			setEditable("ENTEREDATE",false);
			//	20190108 MAX COULSON END:新增報名起迄日
			getTableButton("table1",0).setVisible(false);
			getTableButton("table1",2).setVisible(false);
		}else{
			for(int i = 0 ; i < ret.length ; i++){
				for(int j = 0 ; j < ret[i].length ; j++){
					addScript("EMC['table1'].setCellEditable('" + i + "','" + j + "'," + true + ");");
				}
			}
			setEditable("table1",true);
			setEditable("SDATE",true);
			setEditable("EDATE",true);
			setEditable("TOT_HR",true);
			setEditable("NOTE",true);
			//	20190108 MAX COULSON BEGIN:新增報名起迄日
			setEditable("ENTERSDATE",true);
			setEditable("ENTEREDATE",true);
			//	20190108 MAX COULSON END:新增報名起迄日
			getButton(1).setVisible(true);
			getTableButton("table1",0).setVisible(true);
			getTableButton("table1",2).setVisible(true);
		}
		return true;
	}
}

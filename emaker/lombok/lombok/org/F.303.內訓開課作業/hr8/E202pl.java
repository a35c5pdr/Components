/*
	version	modify_date	modify_user	description
	1.00	20211111	DANIEL	197082-清空檔案，避免再次上傳
	1.01	20220302	KEVIN	修改CPNYID.SUPER
*/
package hr8;
import javax.swing.*;
import jcx.jform.hproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import hr.common5;


public class E202pl extends hproc{
	talk t = null;
	String sql = "";
	String[][] ret = null;
	String value = "";
	String SUPER = "";
	public String action(String value)throws Throwable{
		t = getTalk();
		//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
		//SUPER = (String)get("CPNYID.SUPER");
		SUPER = (String)get("EDUCATION.SUPER");
		//20220302 KEVIN ELIZA end:修改CPNYID.SUPER
		String name = getName();
		this.value = value;
		//20211111 DANIEL ELINE begin:避免檔案重複上傳
		common5.deleteUploadFile(this);
		//20211111 DANIEL ELINE end:避免檔案重複上傳
		if("FORM_STATUS".equals(name)){
			FORM_STATUS();
		}else if("cal1".equals(name)){
			cal1();
		}else if("cal2".equals(name)){
			cal2();
		}else if("button3".equals(name)){
			//重置附表順序
			//20130117 TIM 用setTableData貌似會把隱藏的CPNYID, LESS_KEY, LESS_NO的值洗掉, 所已修改的時候抓不到對應的值而無法insert
			String[][] ret = getTableData("table1");
			//20181226 William Patrick begin : 序號帶不出來
			//setValueAt("table1", String.valueOf(ret.length), ret.length-1, "SKEY");
			for(int i=0;i<ret.length;i++){
				setValueAt("table1", String.valueOf(i+1), i, "SKEY");
			}
			//20181226 William Patrick end : 序號帶不出來
			String TOT_HR = "0";
			//20171002 TIMO ELINE 刪除要改一下上課總時數
			for(int i = 0 ; i < ret.length; i++ ){
				if(ret[i][7].length() == 0){
					ret[i][7] = "0";
				}
				TOT_HR = operation.add(ret[i][7],TOT_HR,1);
			}
			setValue("TOT_HR",TOT_HR);
			/*
			for(int i = 0; i < ret.length; i++){
				ret[i][3] = (i+1)+"";
			}
			setTableData("table1", ret);
			*/
			//20190527 William Cindy begin : 按下表格刪除按鈕後，也要重新計算講師費
			TEACHFEE();
			//20190527 William Cindy end : 按下表格刪除按鈕後，也要重新計算講師費
			return value;
		}else if("DEPT_NO1".equals(name)){
			if(POSITION!=1){
			    return value;
			}
			return (String)hr.common.alluserget(getUser().trim()+"@D",getTalk());
		}else if(	"PLANTFEE".equals(name) ||	"TEACHFEE".equals(name) || "MISCFEE".equals(name) || "COST_4".equals(name)
				||	"COST_5".equals(name)	||	"COST_6".equals(name)	|| "COST_7".equals(name)){
			return FEE();
		}
		return value;
	}


	private void FORM_STATUS() throws Exception{
		setControls();   //設定控制項編輯狀態
		//	20190111 MAX COULSON BEGIN:所有必填欄位都鎖住 如果有存在在PQ22_CLOSE就全鎖住
		if(POSITION == 4){
			String [][]ret = null;
			String LESS_KEY = getValue("LESS_KEY").trim();
			sql = "select count(*) from PQ22_CLOSE where LESS_KEY = '"+convert.ToSql(LESS_KEY)+"'";
			ret = t.queryFromPool(sql);
			boolean bCONFIRM = false;
			String PQ22FLOWC = ret[0][0].trim(); 
			if(!"0".equals(PQ22FLOWC)){
				bCONFIRM = true;
			}
			setEditable("SDATE",false);
			setEditable("EDATE",false);
			
			setEditable("table1",false);
			setEditable("PEOPLE_AMT",false);
			setEditable("ENTERSDATE",false);
			setEditable("ENTEREDATE",false);
			setEditable("WAITPEO",false);
			
			setEditable("PLAN_1",false);
			setEditable("PLAN_1",false);
			setEditable("PLAN_2",false);
			setEditable("PLAN_3",false);
			setEditable("COST_1",false);
			setEditable("COST_2",false);
			setEditable("COST_3",false);
			setEditable("PLANITEM",true);
			setEditable("TRAIN_1",true);
			setEditable("TRAIN_2",true);
			setEditable("ISOTHDEPT",true);
			//20190507 DANIEL PATRICK begin:ISOTHDEPT 是 Y 才打開DEPT_NO2
			setEditable("DEPT_NO2",true);
			//20190507 DANIEL PATRICK end:ISOTHDEPT 是 Y 才打開DEPT_NO2
			setEditable("PLANTFEE",true);
			setEditable("TEACHFEE",true);
			setEditable("MISCFEE",true);
			setEditable("COST_4",true);
			setEditable("COST_5",true);
			setEditable("COST_6",true);
			setEditable("COST_7",true);
			setEditable("USER_PAY",true);
			setEditable("FILES",true);
			setEditable("FILES1",true);
			setEditable("FILES2",true);
			setEditable("FILES3",true);
			setEditable("NOTE1",true);
			setEditable("NOTE2",true);
			setVisible("em_edit_button",true);
			if(bCONFIRM){
				setEditable("PLAN1",false);
				setEditable("PLANITEM",false);
				setEditable("TRAIN_1",false);
				setEditable("TRAIN_2",false);
				setEditable("ISOTHDEPT",false);
				//20190507 DANIEL PATRICK begin:ISOTHDEPT 是 Y 才打開DEPT_NO2
				setEditable("DEPT_NO2",false);
				//20190507 DANIEL PATRICK end:ISOTHDEPT 是 Y 才打開DEPT_NO2
				setEditable("PLANTFEE",false);
				setEditable("TEACHFEE",false);
				setEditable("MISCFEE",false);
				setEditable("COST_4",false);
				setEditable("COST_5",false);
				setEditable("COST_6",false);
				setEditable("COST_7",false);
				setEditable("USER_PAY",false);
				setEditable("FILES",false);
				setEditable("FILES1",false);
				setEditable("FILES2",false);
				setEditable("FILES3",false);
				setEditable("NOTE1",false);
				setEditable("NOTE2",false);
				setVisible("em_edit_button",false);
			}
		}
		//	20190111 MAX COULSON END:所有必填欄位都鎖住 如果有存在在PQ22_CLOSE 就全鎖住
		//String source = getState().trim();
			//新增狀態
		//if(POSITION == 1 && "E.202.開課作業(無流程)".equals(source)){
		//20131126 BLUE 不知道為什麼要用getState去判斷 感覺有點怪  不過也不敢拿掉 改成getFunctionID來做
		String FUNCID = getFunctionID();
		//20181018 DANIEL PATRICK begin:8版功能名稱不同
		//if(POSITION == 1 && "E.202".equals(FUNCID)){
		if(POSITION == 1 && "F.303".equals(FUNCID)){
		//20181018 DANIEL PATRICK end:8版功能名稱不同
//			setValue("CPNYID",convert.replace(""+get("iCPNYID"),"null",""));
			setValue("LESS_KEY","");  
			setValue("COMP_PAY","0");       //預設公司付費為零   
			setValue("USER_PAY","0");        //預設員工自付為零
			setValue("TOT_AMT","0");           //預設總金額為零
			setValue("PEOPLE_AMT","0");    //正取人員名額
			setValue("WAITPEO", "0");   		//備取人員名額 //20121205 Tim Eline 新增
			
			String DEPT_NO1 = getValue("DEPT_NO1").trim();
			if ( DEPT_NO1 == null || DEPT_NO1.length() == 0 ){
				DEPT_NO1 = (String)hr.common.alluserget(getUser().trim()+"@D",t);
			}
			
			sql = "select CPNYID from HRUSER_DEPT_BAS where DEP_NO = "+ DEPT_NO1;
			ret = t.queryFromPool( sql , 60 );
			if ( ret.length > 0 ){
				setValue("CPNYID" , ret[0][0].trim());
			}
			return;
		}	
		if ( POSITION == 1 ){
			setValue("LESS_KEY" , "");
		}
		
		//列表,查詢,流程
		if(POSITION  == 3 || POSITION == 4 || POSITION == 5){
			String LESS_NO = getValue("LESS_NO").trim(); //課程編號
			String COMP_PAY = getValue("COMP_PAY").trim();
			String USER_PAY = getValue("USER_PAY").trim();
			String LESS_NO1 = getValue("LESS_NO1").trim();
			sql = "select LESS_NO1,LESS_NO2 from PQ03 where LESS_NO = '"+convert.ToSql(LESS_NO)+"' "+SUPER;
			ret = t.queryFromPool(sql);
			if (ret.length>0){
				setValue("LESS_NO1",ret[0][0].trim()); //課程大分類
				put("iLESS_NO1", ret[0][0].trim());
				setValue("LESS_NO2",ret[0][1].trim()); //課程小分類
			}
			if(COMP_PAY.length() == 0){
				COMP_PAY = "0";
			}
			if(USER_PAY.length() == 0){
				USER_PAY = "0";
			}
			//20190128 TIMO COULSON begin 不用+USER_PAY了
			//int totAmt = Integer.parseInt(COMP_PAY) + Integer.parseInt(USER_PAY);
			int totAmt = Integer.parseInt(COMP_PAY);
			//20190128 TIMO COULSON end 不用+USER_PAY了
			setValue("TOT_AMT",""+totAmt);    //顯示總金額
			return;
		}
		return;
	}
	
	//設定控制項編輯狀態	
	public void setControls(){
		setEditable("LESS_KEY",false);     //開課編號
		setEditable("ISPLAN",false);     //開課編號
		
		setEditable("TOT_HR",false);         //上課總時數 
//		setEditable("LESS_DESC",false);   //課程描述
//		setEditable("TRAIN_1",false);         //必修人員
//		setEditable("TRAIN_2",false);         //選修人員
		setEditable("TOT_AMT", false);      //總金額
		if(POSITION == 3){
			getTableButton("table1",0).setVisible(false);
			getTableButton("table1",2).setVisible(false);
		}else{
			getTableButton("table1",0).setVisible(true);
			getTableButton("table1",2).setVisible(true);
		}
	}


	private void cal1() throws Exception{
		String SDATE = getValue("SDATE").trim();
		String CPNYID = getValue("CPNYID").trim();
		String COMP_PAY = getValue("COMP_PAY").trim();
		String DEPT_NO1 = getValue("DEPT_NO1").trim();
		String LESS_KEY = getValue("LESS_KEY").trim();
		String LESS_NO = getValue("LESS_NO").trim();
		String LESS_NO1 = getValue("LESS_NO1").trim();
		String LESS_NO2 = getValue("LESS_NO2").trim();
		String YY = "";

		if(check.isDate(SDATE,"yymmdd")) {
			SDATE = operation.add(SDATE,"19110000",0);
		}
		if(check.isDate(SDATE,"YYYYmmdd")){
			YY = datetime.getYear(SDATE);
		}
		if(COMP_PAY.length() == 0){
			COMP_PAY="0";
		}

		String[][] sa2 = null;
		if(DEPT_NO1.length()>0 && YY.length()>0){
			sql = " select TRANTOTAMT from PQ17N where DEPT_NO='"+DEPT_NO1+"' and YY='"+YY+"' and CPNYID='"+convert.ToSql(CPNYID)+"'";
			sa2 = getTalk().queryFromPool(sql);
			String TOT = "0";
			if(sa2.length>0) TOT = sa2[0][0].trim();
			
			sql = " select isnull(sum(isnull(COMP_PAY,0)),0) from PQ22";
			sql+= " where SDATE like '"+YY+"%' and DEPT_NO1='"+DEPT_NO1+"'";
			if(POSITION != 1) sql+= " and LESS_KEY<>'"+LESS_KEY+"'";
			sql+= " union ";					
			sql+= " select isnull(sum(isnull(COMP_PAY,0)),0) from PQ32";
			sql+= " where SDATE like '"+YY+"%' and DEPT_NO='"+DEPT_NO1+"'";
			if(POSITION != 1) sql+= " and LESS_KEY<>'"+LESS_KEY+"'";
			sa2 = getTalk().queryFromPool(sql);
			String used = "0";
			if(sa2.length>0) used = operation.add(used,sa2[0][0],0);
			if(sa2.length>1) used = operation.add(used,sa2[1][0],0);
			used = operation.add(used,COMP_PAY,0);
			
			String amt1 = operation.sub(TOT,used,0);
			
			setValue("PLAN_1",TOT);
			setValue("PLAN_2",used);
			setValue("PLAN_3",amt1);
			
			sql = " select PLMONEY from PQ16 where LESS_NO1='"+convert.ToSql(LESS_NO1)+"'";
			sql+= " and LESS_NO2='"+convert.ToSql(LESS_NO2)+"'";
			sql+= " and LESS_NO='"+convert.ToSql(LESS_NO)+"'";
			sql+= " and YY='"+YY+"' and DEPT_NO='"+DEPT_NO1+"'";
			String PLMONEY = "0";
			sa2 = getTalk().queryFromPool(sql);
			if(sa2.length>0) PLMONEY = sa2[0][0].trim();
			
			String amt2 = operation.sub(PLMONEY,COMP_PAY,0);
			
			setValue("COST_1",PLMONEY);
			setValue("COST_2",COMP_PAY);
			setValue("COST_3",amt2);
			
			sa2 = getTalk().queryFromPool("select TRAIN_1,TRAIN_2,STUDENTCOUNT,BUSITEM from PQ15 where YY = '"+YY+"' and DEPT_NO1 = '"+convert.ToSql(DEPT_NO1)+"' and LESS_NO = '"+convert.ToSql(LESS_NO)+"' and LESS_NO1 = '"+convert.ToSql(LESS_NO1)+"' and LESS_NO2 = '"+convert.ToSql(LESS_NO2)+"'");
			if (sa2.length > 0){
				setValue("TRAIN_1",sa2[0][0].trim());
				setValue("TRAIN_2",sa2[0][1].trim());
				setValue("PEOPLE_AMT",sa2[0][2].trim());
				setValue("PLANITEM",sa2[0][3].trim());
			}
		}
	}

	private void cal2(){
		String COST_4 = getValue("COST_4").trim();
		String COST_5 = getValue("COST_5").trim();
		String COST_6 = getValue("COST_6").trim();
		String COST_7 = getValue("COST_7").trim();
		String PLANTFEE = getValue("PLANTFEE").trim();
		String TEACHFEE = getValue("TEACHFEE").trim();
		String MISCFEE = getValue("MISCFEE").trim();
		String amt = "0";
		if(check.isNum(COST_4)) amt = operation.add(amt,COST_4,0);
		if(check.isNum(COST_5)) amt = operation.add(amt,COST_5,0);
		if(check.isNum(COST_6)) amt = operation.add(amt,COST_6,0);
		if(check.isNum(COST_7)) amt = operation.add(amt,COST_7,0);
		if(check.isNum(PLANTFEE)) amt = operation.add(amt,PLANTFEE,0);
		if(check.isNum(TEACHFEE)) amt = operation.add(amt,TEACHFEE,0);
		if(check.isNum(MISCFEE)) amt = operation.add(amt,MISCFEE,0);
		setValue("TOT_AMT",amt);
		setValue("COMP_PAY",amt);
		getButton("cal1").doClick();
	}

	private String FEE(){
		if(POSITION!=1){
			return value;
		}
		return "0";
	}
	
	//20190527 William Cindy begin : 按下表格刪除按鈕後，也要重新計算講師費
	private void TEACHFEE() throws Exception{
		String[][] TEACHFEE = getTableData("table1");
		String totalFEE = "0";
		for(int i = 0; i < TEACHFEE.length; i ++){
			String iTEACHFEE = "";
			
			iTEACHFEE = TEACHFEE[i][11].trim();
			if(iTEACHFEE.length()>0){
				try{
					long FF = Long.parseLong(iTEACHFEE.trim());
					if(FF<0){
						message("講師費格式輸入錯誤，請輸入數字");
						return ;
					}
				}catch(Exception e){
					message("講師費格式輸入錯誤，請輸入數字");
					return ;
				}
			}
			
			if(iTEACHFEE.length() == 0){
				iTEACHFEE = "0";
			}
			totalFEE = operation.add(iTEACHFEE, totalFEE);
		}

		setValue("TEACHFEE", totalFEE);
		cal2();
	}
	//20190527 William Cindy end : 按下表格刪除按鈕後，也要重新計算講師費

	public String getInformation(){
		return "---------------button3(button3).defaultValue()----------------";
	}
}

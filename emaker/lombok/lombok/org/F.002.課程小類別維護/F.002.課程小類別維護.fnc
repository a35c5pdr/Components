�� sr java.util.Vectorٗ}[�;� I capacityIncrementI elementCount[ elementDatat [Ljava/lang/Object;xp       ur [Ljava.lang.Object;��X�s)l  xp   
sr java.util.Hashtable�%!J� F 
loadFactorI 	thresholdxp?@     "w   .   "t control_detailsr java.lang.Boolean� r�՜�� Z valuexpt flowsq ~ ?@     w      t 	待處理sq ~ ?@     w      t rootsq ~ t x2sr java.lang.Integer⠤���8 I valuexr java.lang.Number������  xp   Pt captionq ~ t x1sq ~    
t y2sq ~    Pt y1sq ~    
t buttonssq ~         uq ~    
t 核准t 剔退ppppppppxt processsq ~ ?@     w       xxxt levelt 3t spec2tw<html>
<head>
</head>
<body>
<p>E.2.2.課程小類別維護
</p><p>【功能說明】
</p><p>1. 此作業提供使用者建立及維護課程小類別資料，必須先完成建立課程小類別維護資料後，方可建立小類別維護，例如：A人資類B電子類C電機類D資料類
</p><p>2. 此作業預設會帶出已經存在的課程小類別資料，提供使用者參考。
</p><p>3. 點選資料表格的下方"新增按鈕"後，表格會增加一列空白的資料，請先下拉課程大類別代碼後，再輸入課程小分類代碼與小分類名稱，點選畫面上方"新增/修改" 按鈕存檔即可 。
</p><p>4. 若有需要修改之前的名稱，可直接選擇該筆資料內容，修改小類別名稱後，點選畫面上方"新增/修改" 按鈕存檔即可 ，但是大分類代碼、小分類代碼一經設定後，不可修改。
</p><p>5. 若有需要刪除之前的資料，可直接選擇該筆資料內容，點選資料表格的下方"刪除按鈕"後，再點選畫面上方"新增/修改" 按鈕存檔即可 。
</p><p>6. 若有需要插入已存在的資料，可直接選擇該筆資料內容，點選資料表格的下方"插入資料按鈕"後，系統會新增一筆空白的資料列於該選擇資料列的上方。
</p><p>7. 若有需要查詢，您可以先點選查詢按鈕，輸入您想要選擇的查詢條件，按下"確定"按鈕後，系統會帶出符合查詢條件的資料。
</p><p>8. 可列印畫面上所顯示的課程小類別資料
</p><p>9. 欄位說明 :
</p><p>    課程大分類代碼(LESS_NO1):下拉式清單關聯課程大分類檔(PQ01)，顯示代號加上名稱(LESS_NO1 + ' ' + LESS_NAME1)，不可空白
</p><p>    課程小分類代碼(LESS_NO2): 可輸入小分類代碼，不可空白，不可重覆
</p><p>    課程小分類名稱(LESS_NAME2):可輸入小分類名稱，不可空白
</p><p>    
</p><p>【新增/ 修改】
</p><p>1. 此功能按鈕等同存檔，存檔前會檢核"課程大類別代碼不可空白!"、"課程小類別代碼不可空白!"、"課程小類別代碼不可重覆"、"課程小類別名稱不可空白!"
</p><p>2. 同時寫入課程小類別資料(PQ02) 的申請人(MUSER)=登錄帳號、申請日(MDATE)=系統日今天、申請時間(MTIME)=系統時間。
</p><p>
</p><p>
</p><p>【查詢】
</p><p>查詢條件 :
</p><p>1. 課程大類別代號：下拉式選單課程大類別(PQ01)，選擇代碼(PQ02.LESS_NO1)，顯示大類別名稱(PQ01.LESS_NAME1)，空白查詢全部
</p><p>2. 課程小類別代號：下拉式清單可選擇課程小類別代號，空白查詢全部
</p><p>3. 課程小類別名稱(LIKE)：下拉式清單帶出小類別名稱，可關鍵字輸入，空白查詢全部
</p><p>
</p><p>
</p><p>【列印】
</p><p>1. 可列印畫面上所顯示的課程小類別資料
</p><p>
</p><p>
</p><p>【主表欄位說明】
</p><p>課程小類別維護表(PQ02)
</p><p>1. 課程大分類代碼(LESS_NO1):下拉式清單關聯課程大分類檔(PQ01)，顯示代號加上名稱(LESS_NO1 + ' ' + LESS_NAME1)
</p><p>2. 課程小分類代碼(LESS_NO2): 可輸入小分類代碼，不可空白，不可重覆
</p><p>3. 課程小分類名稱(LESS_NAME2):可輸入小分類名稱，不可空白。
</p><p>4. 申請人(MUSER):隱藏欄位
</p><p>5. 申請日(MDATE): 隱藏欄位
</p><p>6. 申請時間(MTIME):隱藏欄位
</p><p>
</p><p>
</p><p>
</p></body>
</html>t 	allowEditsq ~  t 
spec_caretsq ~     t html_numbersq ~    t allowExportsq ~  t batcht  t flowControlsq ~  t allowAddsq ~  t form_uniqueIDsr java.lang.Long;��̏#� J valuexq ~    �њ�t typet formt 
allowPrintsq ~  t html_uniqueIDsq ~ 5   �x�Ȇt spect$E.2.2.課程小類別維護
【功能說明】
1. 此作業提供使用者建立及維護課程小類別資料，必須先完成建立課程小類別維護資料後，方可建立小類別維護，例如：A人資類B電子類C電機類D資料類
2. 此作業預設會帶出已經存在的課程小類別資料，提供使用者參考。
3. 點選資料表格的下方"新增按鈕"後，表格會增加一列空白的資料，請先下拉課程大類別代碼後，再輸入課程小分類代碼與小分類名稱，點選畫面上方"新增/修改" 按鈕存檔即可 。
4. 若有需要修改之前的名稱，可直接選擇該筆資料內容，修改小類別名稱後，點選畫面上方"新增/修改" 按鈕存檔即可 ，但是大分類代碼、小分類代碼一經設定後，不可修改。
5. 若有需要刪除之前的資料，可直接選擇該筆資料內容，點選資料表格的下方"刪除按鈕"後，再點選畫面上方"新增/修改" 按鈕存檔即可 。
6. 若有需要插入已存在的資料，可直接選擇該筆資料內容，點選資料表格的下方"插入資料按鈕"後，系統會新增一筆空白的資料列於該選擇資料列的上方。
7. 若有需要查詢，您可以先點選查詢按鈕，輸入您想要選擇的查詢條件，按下"確定"按鈕後，系統會帶出符合查詢條件的資料。
8. 可列印畫面上所顯示的課程小類別資料
9. 欄位說明 :
    課程大分類代碼(LESS_NO1):下拉式清單關聯課程大分類檔(PQ01)，顯示代號加上名稱(LESS_NO1 + ' ' + LESS_NAME1)，不可空白
    課程小分類代碼(LESS_NO2): 可輸入小分類代碼，不可空白，不可重覆
    課程小分類名稱(LESS_NAME2):可輸入小分類名稱，不可空白
    
【新增/ 修改】
1. 此功能按鈕等同存檔，存檔前會檢核"課程大類別代碼不可空白!"、"課程小類別代碼不可空白!"、"課程小類別代碼不可重覆"、"課程小類別名稱不可空白!"
2. 同時寫入課程小類別資料(PQ02) 的申請人(MUSER)=登錄帳號、申請日(MDATE)=系統日今天、申請時間(MTIME)=系統時間。


【查詢】
查詢條件 :
1. 課程大類別代號：下拉式選單課程大類別(PQ01)，選擇代碼(PQ02.LESS_NO1)，顯示大類別名稱(PQ01.LESS_NAME1)，空白查詢全部
2. 課程小類別代號：下拉式清單可選擇課程小類別代號，空白查詢全部
3. 課程小類別名稱(LIKE)：下拉式清單帶出小類別名稱，可關鍵字輸入，空白查詢全部


【列印】
1. 可列印畫面上所顯示的課程小類別資料


【主表欄位說明】
課程小類別維護表(PQ02)
1. 課程大分類代碼(LESS_NO1):下拉式清單關聯課程大分類檔(PQ01)，顯示代號加上名稱(LESS_NO1 + ' ' + LESS_NAME1)
2. 課程小分類代碼(LESS_NO2): 可輸入小分類代碼，不可空白，不可重覆
3. 課程小分類名稱(LESS_NAME2):可輸入小分類名稱，不可空白。
4. 申請人(MUSER):隱藏欄位
5. 申請日(MDATE): 隱藏欄位
6. 申請時間(MTIME):隱藏欄位


t 
advancecfgsq ~ ?@     8w   K   5t 
print_viewt  t progress_queryt  t 刪除按鈕標題t 	[default]t 新增按鈕標題q ~ Ft allow_batch_approvet Yest !刪除發生錯誤才顯示訊息sq ~  t 
qwin_widtht  t query_triggersq ~ ?@     w      t importt import jcx.jform.bNotify;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t timesq ~ 5  GIAk�t prgnamet PN1405683329740t 	classHeadt  extends bNotify{
t idt admint 
methodHeadt <public void actionPerformed(String value)throws Throwable{
t infot _public String getInformation(){
	return "---------------query_trigger()----------------";
}
t contentt �// 當執行完 Transaction 時,會執行本段程式
//可用以寄發Email通知或是自動再處理自定Transaction
String[][] data =getTableData("table1");
if(data.length ==0){
	message("查無資料");
}

return;t classEndt }
t caretsq ~    �t 	methodEndq ~ axt allowCreatorModifyt Not 刪除按鈕程式sq ~ ?@     w       xt 新增按鈕程式sq ~ ?@     w       xt allow_flow_list_queryt Not 
key_updatet by global configt flow_mirrort  t print_currentt Not field_checksq ~ ?@     w       xt 列印按鈕標題q ~ Ft progress_addq ~ Dt insert_triggersq ~ ?@     w       xt 查詢條件設定sq ~ ?@     w       xt query_scopet 
Owned rowst flow_table2q ~ Ft flow_table1q ~ Ft 
firstStageq ~ t update_triggersq ~ ?@     w       xt print_triggersq ~ ?@     w       xt progress_deleteq ~ Dt delete_triggersq ~ ?@     w       xt 列印按鈕程式sq ~ ?@     w       xt !修改發生錯誤才顯示訊息sq ~  t 
flow_ordert  t 
whiteBoardt Not helpt  t qmaxq ~ Bt 刪除前先顯示確認視窗sq ~ t checkKeyt  t progress_editq ~ Dt 	queryModet Yest allowFlowModifyt Yest 查詢按鈕標題q ~ Ft 自定查詢條件sq ~ ?@     w       xt 	flow_viewq ~ Mt 修改按鈕標題t 新增／修改t q_typet isolatet qwin_heightq ~ Mt 	flow_typet Stateful Flowt 查詢按鈕程式sq ~ ?@     w       xt SAq ~ �t 修改按鈕程式sq ~ ?@     w      t contentt  t oldsq ~         uq ~    
sq ~ ?@     w      t timesq ~ 5  :�Ѭ�t versq ~     t contentt  t idt admint prgnamet 	hr/E003ptxsq ~ ?@     w      t timesq ~ 5  :��V�t versq ~    t contentq ~ �t idq ~ �t prgnamet hr6/E1002ptxsq ~ ?@     w      q ~ �sq ~ 5  :��E�q ~ �sq ~    q ~ �q ~ �q ~ �q ~ �q ~ �t hr6/E1002ptxpppppppxt idt admint 	methodEndt }
t classEndq ~ �t infot �public String getInformation(){
	return "---------------\u4fee\u6539\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
}
t 	classHeadt  extends bTransaction{
t timesq ~ 5  :����t embeddedsq ~  t verq ~ �t importt �import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t caretsq ~   �t prgnamet 	hr/E003ptt old_prgnamet hr6/E1002ptt 
methodHeadt 6public boolean action(String value)throws Throwable{
xt !新增發生錯誤才顯示訊息sq ~  t 	only_flowt Not 新增後清除所有欄位sq ~  t customize_listt Noxt uniqueIDsq ~ 5  8&�O�t urlt http://t namet F.002.課程小類別維護t formsq ~          uq ~    
ppppppppppxt 	authorizesq ~ ?@     w   %   t (100069)sq ~ ?@     w      t allowDeletesq ~  t allowAddsq ~  t 	allowEditsq ~  t 
allowPrintsq ~  t 
allowQuerysq ~  t allowExportsq ~  xt (110003)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100063)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100004)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt Everyonesq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt A3SETsq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ xt (110004)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100064)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100065)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100006)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~ q ~ �sq ~  q ~ �sq ~ q ~ �sq ~ xt (100016)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100030)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100066)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100007)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100001)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ xt (100027)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100008)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100018)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (110001)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100002)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100012)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100022)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (110002)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100003)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ xt (100049)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xxt 
allowQuerysq ~  t report_uniqueIDsq ~ 5   ��6�t form_numbersq ~    t allowDeletesq ~  q ~ ysq ~ ?@     w      t 
LESS_NAME2sq ~ ?@     w      t captiont 課程小類別名稱t enabledsq ~ t op1t andt defaultvaluesq ~ ?@     w       xt 	inputtypet 文字輸入t assistsq ~ ?@     w       xt opt liket 欄位檢核sq ~ ?@     w       xxt MTIMEsq ~ ?@     w      q ~�t MTIMEq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�t =q ~�sq ~ ?@     w       xxt LESS_NO2sq ~ ?@     	w      	q ~�q ~�t 
html_checksq ~ ?@     w       xq ~�q ~�q ~�q ~�q ~�t 課程小類別代號q ~�sq ~ ?@     w       xq ~�sq ~ q ~�sq ~ ?@     w       xq ~�sq ~ ?@     w       xxt (showTextCOSCNAME)sq ~ ?@     w      t captiont  t enabledsq ~ t op1t andt defaultvaluesq ~ ?@     w       xt 	inputtypet 文字輸入t assistsq ~ ?@     w       xt opt =t 欄位檢核sq ~ ?@     w       xxt LESS_NO1sq ~ ?@     w      q ~�q ~�q ~ �sq ~ ?@     w      t prgnamet PQ1075428120953t 
methodHeadt +public String getFilter()throws Throwable{
t importt wimport jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t 	methodEndt }
t contentt?// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return "and PQ01.LESS_NO1 = PQ02.LESS_NO1 group by PQ02.LESS_NO1,PQ01.LESS_NAME1";t 	classHeadt  extends bQuery{
t caretsq ~    �t classEndq ~	xt allowSpaceWhenSelectsq ~  q ~�sq ~ t inputtype_text_4t <PQ01,PQ02[{!@#}]PQ02.LESS_NO1[{!@#}]PQ01.LESS_NAME1[{!@#}]hrq ~�sq ~ ?@     w       xq ~�t 文字輸入t 
allowRadiosq ~  q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xt 
allowInputsq ~  q ~�t 課程大類別代號xt MUSERsq ~ ?@     w      q ~�t MUSERq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt MDATEsq ~ ?@     w      q ~�t MDATEq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt CPNYIDsq ~ ?@     w      q ~�t CPNYIDq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxxt detailsq ~ ?@     w      t Everyonesq ~ ?@     w      t LESS_NO2sq ~ ?@     w      t cfg1sq ~ t columnsq ~ t querysq ~ t hidesq ~  xt LESS_NO1sq ~ ?@     w      q ~:sq ~ q ~<sq ~ q ~>sq ~ q ~@sq ~  xt text9sq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt box6sq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt 
LESS_NAME2sq ~ ?@     w      q ~:sq ~  q ~<sq ~ q ~>sq ~ q ~@sq ~  xt text3sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~  xt MUSERsq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt text2sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt box1sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~  xt text1sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~  xt field3sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt table2sq ~ ?@     w      q ~Xsq ~  q ~Zsq ~  q ~Vsq ~  q ~Tsq ~  t 查詢條件設定sq ~ ?@     w   	   t 
LESS_NAME2sq ~ ?@     w      t allowSpaceWhenSelectsq ~ t 
allowInputsq ~ t inputtype_text_4t &PQ02[{!@#}]LESS_NAME2[{!@#}]LESS_NAME2t opt liket 自定查詢條件sq ~ ?@     
w      
q ~ �sq ~ 5  70�q ~ �t// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by LESS_NAME2";q ~ �t adminq ~ �t  extends bQuery{
q ~ �t +public String getFilter()throws Throwable{
q ~ �t }
q ~ �t wimport jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

q ~ �sq ~    �q ~ �q ~�q ~ �t PQ1108457499453xt enabledsq ~ t 
allowRadiosq ~  q ~Xsq ~ t 	inputtypet &下拉選單(自其它資料表帶出)q ~ t 課程小類別名稱t defaultvaluesq ~ ?@     w       xxt LESS_NO2sq ~ ?@     w      q ~�sq ~ q ~�sq ~  q ~�t "PQ02[{!@#}]LESS_NO2[{!@#}]LESS_NO2q ~�t =q ~�sq ~ ?@     
w      
q ~ �sq ~ 5  6�`q ~ �t	// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by LESS_NO2";q ~ �q ~�q ~ �q ~�q ~ �q ~�q ~ �q ~�q ~ �q ~�q ~ �sq ~    �q ~ �q ~�q ~ �t PQ1108457479875xq ~�sq ~ q ~�sq ~  q ~Xq ~�q ~�q ~�q ~ t 課程小類別代碼q ~�sq ~ ?@     w       xxt LESS_NO1sq ~ ?@     w      q ~�sq ~ q ~�sq ~  q ~�t 6PQ01[{!@#}]LESS_NO1[{!@#}]LESS_NO1 + ' ' +  LESS_NAME1q ~�q ~�q ~�sq ~ ?@     
w      
q ~ �sq ~ 5  5�/q ~ �t	// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by LESS_NO1";q ~ �q ~�q ~ �q ~�q ~ �q ~�q ~ �q ~�q ~ �q ~�q ~ �sq ~    �q ~ �q ~�q ~ �t PQ1108457403734xq ~�sq ~ q ~�sq ~  q ~Xq ~�q ~�q ~�q ~ t 課程大類別代碼q ~�sq ~ ?@     w       xxt MDATEsq ~ ?@     w   	   q ~Xsq ~  q ~�t 文字輸入q ~�sq ~ ?@     w       xq ~�t =q ~�sq ~ q ~ q ~�xt MTIMEsq ~ ?@     w   	   q ~Xsq ~  q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ q ~ q ~�xt MUSERsq ~ ?@     w   	   q ~Xsq ~  q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ q ~ q ~�xxxt field2sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt field1sq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt table1sq ~ ?@     w      q ~>sq ~  q ~@sq ~  q ~<sq ~  q ~:sq ~  t 查詢條件設定sq ~ ?@     w      t 
LESS_NAME2sq ~ ?@     w      q ~�sq ~ ?@     w      q ~ �t �import jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;

q ~ �sq ~ 5  Gb���q ~ �t PQ1406111368174q ~ �q ~�q ~ �t adminq ~ �q ~�q ~ �t�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :endq ~ �q ~�t oldsq ~         uq ~    
sq ~ ?@     w      t timesq ~ 5  Gb�Kpt prgnamet PQ1406111206023t contentt�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) ";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :endt idq ~�xsq ~ ?@     w      q ~�sq ~ 5  Gb�Jq ~�t PQ1406111205327q ~�t�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) ";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :endq ~q ~�xsq ~ ?@     w      q ~�sq ~ 5  Gb�Z8q ~�t PQ1406111078755q ~�t�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order LESS_NO2";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :endq ~q ~�xpppppppxq ~ �sq ~   �q ~ �q ~�xt allowSpaceWhenSelectsq ~ t enabledsq ~ t inputtype_text_4t &PQ02[{!@#}]LESS_NAME2[{!@#}]LESS_NAME2t defaultvaluesq ~ ?@     w       xt 	inputtypet 文字輸入t querysq ~ q ~�sq ~  q ~�sq ~ ?@     w       xt opt liket 欄位檢核sq ~ ?@     w       xt 
allowInputsq ~ t captiont 課程小類別名稱xt MTIMEsq ~ ?@     w      q ~q ~!q ~sq ~ q ~sq ~ ?@     w       xq ~sq ~  q ~t 文字輸入q ~�sq ~ ?@     w       xq ~q ~�q ~sq ~ ?@     w       xxt LESS_NO2sq ~ ?@     w      q ~�sq ~  q ~sq ~  q ~sq ~ q ~sq ~ ?@     w       xq ~t 課程小類別代號q ~�sq ~ ?@     w      q ~ �t �import jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;


q ~ �sq ~ 5  E��q ~ �t PQ1646131318555q ~ �q ~�q ~ �t adminq ~ �q ~�q ~ �t�/*
	version		modify_date		modify_user		description
	1.00				20220301			ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by LESS_NO1";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);

//20220301 ANDYHOU ELIZA begin:權限改使用EDUCATION.SUPER與CONDITION_B06
//return (String)get("CPNYID.SUPER") + " order by 1";
String EDUCATION_SUPER = (String)get("EDUCATION.SUPER");
return EDUCATION_SUPER + " order by 1";
//20220301 ANDYHOU ELIZA end:權限改使用EDUCATION.SUPER與CONDITION_B06

q ~ �q ~�q ~�sq ~         uq ~    
sq ~ ?@     w      t timesq ~ 5  Gb�f�t contentt"// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return (String)get("CPNYID.SUPER") + " order by 1";
t idq ~�t prgnamet PQ1406112392730xsq ~ ?@     w      q ~�sq ~ 5  Gb�P�q ~�t PQ1406112387001q ~�t&// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100


return (String)get("CPNYID.SUPER") + " order by 1";
q ~q ~�xsq ~ ?@     w      q ~�sq ~ 5  Gb�y�q ~�t PQ1406111807723q ~�t�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

/*
//return (String)get("CPNYID.SUPER") + " order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :end
*/
return "";q ~q ~�xpppppppxq ~ �sq ~   5q ~ �q ~�xq ~t $PQ02[{!@#}]LESS_NO2[{!@#}]LESS_CODE2q ~sq ~ q ~t &下拉選單(自其它資料表帶出)q ~sq ~  q ~sq ~ ?@     w       xt keyword_searchsq ~  q ~q ~�q ~�sq ~ ?@     w       xxt 
LESS_CODE2sq ~ ?@     w      t allowSpaceWhenSelectsq ~ t 	inputtypeq ~t 
allowRadiosq ~  t assistsq ~ ?@     w       xt 自定查詢條件sq ~ ?@     w      t importt �import jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;


q ~�sq ~ 5  G��9�q ~�t PQ1407136512205t 	classHeadt  extends bQuery{
q ~t admint 
methodHeadt +public String getFilter()throws Throwable{
q ~�tr// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :end

t classEndt }
t oldsq ~         uq ~    
sq ~ ?@     w      t timesq ~ 5  G��t prgnamet PQ1407135587823t contentt�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
System.err.println("XXXXXXXx="+getValue("LESS_NO1"));
System.err.println("XXXXXXXx="+getQueryValue("LESS_NO1"));
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :end

t idq ~bxsq ~ ?@     w      q ~lsq ~ 5  G��T�q ~nt PQ1407135011781q ~ptr// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by 1";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :end

q ~rq ~bxsq ~ ?@     w      q ~lsq ~ 5  G�ƫ�q ~nt PQ1407134903008q ~pt@// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
String LESS_NO1 = getQueryValue("LESS_NO1");
System.err.println("*****LESS_NO1******"+LESS_NO1);
//return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by 1";


//return " select LESS_CODE2 from PQ02 where LESS_NO1='"+convert.ToSql(LESS_NO1)+"' " ;
return "";
//20140723 Judy  查詢畫面也要顯示公司別為空的課程小類別(即通用小類別) :end

q ~rq ~bxpppppppxt caretsq ~   t 	methodEndq ~gxt 
allowInputsq ~  q ~�sq ~ ?@     w       xt 欄位檢核sq ~ ?@     w       xt inputtype_text_4t $PQ02[{!@#}]LESS_NO1[{!@#}]LESS_CODE2t querysq ~ t captiont 課程小類別代碼t defaultvaluesq ~ ?@     w       xt opt liket enabledsq ~ xt LESS_NO1sq ~ ?@     w      q ~�sq ~ ?@     w      q ~nt PH1407137007030t oldsq ~         uq ~    
sq ~ ?@     w      q ~lsq ~ 5  G��A4q ~nt PH1407136907290q ~pt�// 可自定HTML版本各欄位的onChange 的動作 
// 傳入值 value 
String sql = "select LESS_CODE2 from PQ02 where LESS_NO1 = '"+convert.ToSql(value)+"' ";
String[][] ret =getTalk().queryFromPool(sql);
Vector v1 = new Vector();
Vector v2 = new Vector();
for(int i=0;i<ret.length;i++){
	v1.add(ret[i][0]);
	v2.add(ret[i][0]);	
}
setValue("table1,LESS_CODE2","");
setReference("table1.LESS_CODE2",v1,v2);
 return value;q ~rq ~bxsq ~ ?@     w      q ~lsq ~ 5  G�ޟ�q ~nt PH1407136472724q ~pt�// 可自定HTML版本各欄位的onChange 的動作 
// 傳入值 value 
String sql = "select LESS_CODE2 from PQ02 where LESS_NO1 = '"+convert.ToSql(value)+"' ";
String[][] ret =getTalk().queryFromPool(sql);
Vector v1 = new Vector();
Vector v2 = new Vector();
for(int i=0;i<ret.length;i++){
	v1.add(ret[0][0]);
	v2.add(ret[0][0]);	
}
setValue("table1,LESS_CODE2","");
setReference("table1.LESS_CODE2",v1,v2);
 return value;q ~rq ~bxsq ~ ?@     w      q ~lsq ~ 5  G��{q ~nt PH1407135980141q ~pt�// 可自定HTML版本各欄位的onChange 的動作 
// 傳入值 value 
String sql = "select LESS_CODE2 from PQ02 where LESS_NO1 = '"+convert.ToSql(value)+"' ";
String[][] ret =getTalk().queryFromPool(sql);
Vector v1 = new Vector();
Vector v2 = new Vector();
for(int i=0;i<ret.length;i++){
	v1.add(ret[0][0]);
	v2.add(ret[0][0]);	
}
setReference("table1.LESS_CODE2",v1,v2);
 return value;q ~rq ~bxpppppppxt caretsq ~   Bt importt }import jcx.jform.hproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t enabledsq ~  q ~lsq ~ 5  G����t 	methodEndt }
t 	classHeadt  extends hproc{
t classEndq ~�t infot �public String getInformation(){
	return "---------------null(\u8ab2\u7a0b\u5927\u985e\u5225\u4ee3\u865f).html_action()----------------";
}
q ~rq ~bt 
methodHeadt 5public String action(String value)throws Throwable{
q ~pt�// 可自定HTML版本各欄位的onChange 的動作 
// 傳入值 value 
String sql = "select LESS_CODE2 from PQ02 where LESS_NO1 = '"+convert.ToSql(value)+"' ";
String[][] ret =getTalk().queryFromPool(sql);
Vector v1 = new Vector();
Vector v2 = new Vector();
for(int i=0;i<ret.length;i++){
	v1.add(ret[i][0]);
	v2.add(ret[i][0]);	
}
setValue("table1.LESS_CODE2","");
setReference("table1.LESS_CODE2",v1,v2);
 return value;xq ~Nsq ~  q ~t >PQ01[{!@#}]LESS_NO1[{!@#}]rtrim(LESS_CODE1) + ' ' + LESS_NAME1q ~sq ~ ?@     w       xq ~sq ~  q ~q ~�q ~�sq ~ ?@     w       xq ~sq ~ q ~sq ~ q ~sq ~ q ~q ~Kq ~sq ~ ?@     w       xq ~�sq ~  q ~t 課程大類別代號q ~�sq ~ ?@     w      q ~ �t �import jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;

q ~ �sq ~ 5  E�q ~ �t PQ1646131265336q ~ �q ~�q ~ �q ~4q ~ �q ~�q ~ �t�/*
	version		modify_date		modify_user		description
	1.00				20220301			ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by LESS_NO1";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);

//20220301 ANDYHOU ELIZA begin:權限改使用EDUCATION.SUPER與CONDITION_B06
//return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by LESS_NO1";
String EDUCATION_SUPER = (String)get("EDUCATION.SUPER");
return "and ((1=1 "+ EDUCATION_SUPER + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by LESS_NO1";
//20220301 ANDYHOU ELIZA end:權限改使用EDUCATION.SUPER與CONDITION_B06
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :endq ~ �q ~�t oldsq ~         uq ~    
sq ~ ?@     w      q ~9sq ~ 5  G����q ~;t�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by LESS_NO1";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by LESS_NO1";

//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :endq ~=q ~bq ~>t PQ1407133725323xsq ~ ?@     w      q ~lsq ~ 5  G����q ~nt PQ1407133071485q ~pt�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by LESS_NO1";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
//return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by LESS_NO1";
return "";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :endq ~rq ~bxsq ~ ?@     w      q ~lsq ~ 5  Gb���q ~nt PQ1406109329135q ~pt�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

//return (String)get("CPNYID.SUPER") + " order by LESS_NO1";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :begin
talk t = getTalk();
String ISNULL = pDB.getISNULL(t);
return "and ((1=1 "+(String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) order by LESS_NO1";
//20140723 Judy Eline 查詢畫面也要顯示公司別為空的課程大類別(即通用大類別) :endq ~rq ~�xpppppppxq ~ �sq ~   �q ~ �q ~�xxt MUSERsq ~ ?@     	w      	q ~q ~&t 
allowInputsq ~  q ~q ~�q ~q ~�q ~�sq ~ ?@     w       xq ~sq ~ q ~sq ~  q ~sq ~ ?@     w       xq ~sq ~ ?@     w       xxt MDATEsq ~ ?@     w      q ~q ~�q ~sq ~ q ~sq ~ ?@     w       xq ~sq ~  q ~q ~&q ~�sq ~ ?@     w       xq ~q ~�q ~sq ~ ?@     w       xxt CPNYIDsq ~ ?@     w      q ~�t 	公司別q ~�sq ~ q ~�sq ~ ?@     w       xt querysq ~  q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxxxt image1sq ~ ?@     w      q ~Tsq ~  q ~Vsq ~  q ~Xsq ~  q ~Zsq ~  xt text11sq ~ ?@     w      q ~Tsq ~  q ~Vsq ~  q ~Xsq ~  q ~Zsq ~  xt text10sq ~ ?@     w      q ~Jsq ~  q ~Lsq ~  q ~Nsq ~  q ~Psq ~  xt default_companysq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt MDATEsq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt MTIMEsq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt FORM_STATUSsq ~ ?@     w      q ~:sq ~  q ~<sq ~  q ~>sq ~  q ~@sq ~ xt showTextCOSCNAMEsq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xxxt clazzt  t portal_datasq ~ ?@     w      t config2sq ~         2uq ~    Psq ~         uq ~    
t  q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxsq ~         uq ~    
q ~-q ~-ppppppppxppppppppppppppppppppppppppppppxt config1sq ~ ?@     w      t widthsur [IM�`&v겥  xp         xt config3sq ~         uq ~    sq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
q ~-q ~-q ~-q ~-ppppppxsq ~         uq ~    
t  q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxsq ~         uq ~    
q ~�q ~�q ~�q ~�ppppppxxxt stypet htmlt linksq ~         uq ~    (sq ~         uq ~    
t     E.2.1.課程大類別維護sq ~ sq ~ pppppppxsq ~         uq ~    
t     E.2.3.課程資料維護sq ~ sq ~ pppppppxsq ~         uq ~    
t  sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxsq ~         uq ~    
q ~�sq ~ sq ~ pppppppxppppppppppxt keyt F.002t 列表設定sq ~ ?@     w      t Everyonesq ~ ?@     w      t listsq ~         uq ~    
sq ~         uq ~    
t LESS_NO1(LESS_NO1)t 課程大類別代號t 置左sq ~    �ppppppxsq ~         uq ~    
t LESS_NO2(LESS_NO2)t 課程小類別代號t 置中sq ~    �ppppppxsq ~         uq ~    
t LESS_NAME2(LESS_NAME2)t 課程小類別名稱q ~Nsq ~    �ppppppxpppppppxxxxpppppppppx
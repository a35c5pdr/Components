/*
	version modify_date modify_user description
	1.00	20210114	Bristol		(公版新增) F.023.先修課程維護
	1.00	20220301	ANDYHOU		權限改使用EDUCATION.SUPER與CONDITION_B06
*/
package hr8;
import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;
import javax.swing.*;
import hr.Msg.hrm8b_F023;

public class F023pt extends bTransaction{
	String command = "";
	String sql = "";
	String[][] ret = null;
	talk t = null;
	String USER, today, time;
	String ISNULL, DBN;
	String DATELIST = "B";
	String DATETYPE = "YYYY";
	String DATESTR = "西元";
	String BACK = "history.back();";
	public boolean action(String value)throws Throwable{
		// 回傳值為 true 表示執行接下來的資料庫異動或查詢
		// 回傳值為 false 表示接下來不執行任何指令
		// 傳入值 value 為 "新增","查詢","修改","刪除","列印","PRINT" (列印預覽的列印按鈕),"PRINTALL" (列印預覽的全部列印按鈕) 其中之一
		command = value;
		t = getTalk();

		USER = getUser().trim();
		today = datetime.getToday("YYYYmmdd");
		time = datetime.getTime("hms");

		ISNULL = pDB.getISNULL(t);
		DBN = pDB.getDBN(t);

		DATELIST = (String)get("SYSTEM.DATELIST");
		if("A".equals(DATELIST)){
			DATETYPE = "yyy";
			DATESTR = "民國";
		}
		
		if("查詢".equals(command)){
			 return QUERY();											// 查詢
		} else if("修改".equals(command)){
			 return UPDATE();											// 修改
		}
		
		return true;
	}

	/**
	 * 查詢
	 * return boolean
	 * @throws Throwable
	 */
	public boolean QUERY() throws Throwable {
		// 刷新表格
		setTableData("table1", new String[0][0]);
		setTableData("table2", new String[0][0]);

		// 修改按鈕
		getButton(3).setVisible(true);
		getButton(3).setEnabled(true);

		//getTableButton("table1", 0).setVisible(true);   //新增按鈕
		//getTableButton("table1", 2).setVisible(true);   //刪除按鈕
		addScript("EMC['table1'].getButton(0).setVisible(true);");
		addScript("EMC['table1'].getButton(1).setVisible(true);");
		addScript("EMC['table1'].getButton(2).setVisible(true);");
		
		String LESS_NO = getQueryValue("table1.LESS_NO").trim();			// 課程名稱

		boolean b100006 = false;

		sql = " select 1 from HRUSER_DEPT where ID = '" + USER + "' and DEP_NO in ('100006') ";
		ret = t.queryFromPool(sql);
		if(ret.length > 0){
			b100006 = true;
		}

		/* "and EMPID in (select HANDLE_USER from HR_CONDITION06 where EMPID = '" + EMPID + "')" */

		// 課程所屬公司別(PQ03.CPNYID) SUPER 及 CONDITION_B06
		StringBuffer sbLESS_NO = new StringBuffer();
		sql = " select a.LESS_NO " 
			+ " from PQ03 a "
			+ " where a.LESS_NO in (select LESS_NO from PRE_LESS where LESS_NO = a.LESS_NO) ";
		
		//20220302 ANDYHOU ELIZA begin:權限改使用EDUCATION.SUPER與CONDITION_B06
		/*
		if(!b100006){
			sql += " and ( CPNYID in (select CPNYID from HRUSER where 1 = 1 " + (String)get("CONDITION_B06")
				+  (String)get("CPNYID.SUPER") + ") "
				+  " or isnull(CPNYID, '') = '' "
				+  " ) ";
		}
		*/
		sql += " and ( CPNYID in (select CPNYID from HRUSER where 1 = 1 " + (String)get("CONDITION_B06")
			+  (String)get("EDUCATION.SUPER") + ") "
			+  " or isnull(CPNYID, '') = '' "
			+  " ) "
			;
		//20220302 ANDYHOU ELIZA end:權限改使用EDUCATION.SUPER與CONDITION_B06
		

		ret = t.queryFromPool(sql);
		for(int i = 0 ; i < ret.length; i++ ){
			String iLESS_NO = ret[i][0].trim();								// 課程編號
			sbLESS_NO.append("'" + iLESS_NO + "',");
		}
		if(sbLESS_NO.length() > 0){
			sbLESS_NO.deleteCharAt(sbLESS_NO.length() -1);
		} else {
			sbLESS_NO.append("''");
		}

		// 整理DB課程資料
		Hashtable htLESS_NO = new Hashtable();								// 課程名稱 , Vector所需先修課程
		Vector vLESS_NO = new Vector();										// 存放KEY (課程名稱 + 序號)
		Vector vKEY = new Vector();											// 判斷是否重複
		Vector vPRE_LESS_NO = new Vector();									// 存放先修課程，後放入Hashtable

		sql = " select LESS_NO , SKEY , PRE_LESS_NO "
			+ " from PRE_LESS "
			+ " where 1 = 1 "
			+ " and LESS_NO in (" + sbLESS_NO.toString() + ") ";

		if(LESS_NO.length() > 0){
			sql += " and LESS_NO = '" + convert.ToSql(LESS_NO) + "' ";
		}
		
		sql += " order by PRE_LESS.LESS_NO , PRE_LESS.SKEY , PRE_LESS.PRE_LESS_NO ";
		
		ret = t.queryFromPool(sql);
		if(ret.length == 0){
			message("查無資料");
			return false;
		}
		// 整理序號相同的先修課程放在一起
		for(int i = 0 ; i < ret.length ; i++ ){
			String iLESS_NO = ret[i][0].trim();								// 課程名稱
			String iSKEY = ret[i][1].trim();								// 序號
			String iPRE_LESS_NO = ret[i][2].trim();							// 先修課程
			String key = iLESS_NO + "@@" + iSKEY;
			String LAST_KEY = "";
			
			if(i > 0){
				LAST_KEY = (String)vKEY.get(i - 1);

				// 若和前一筆相同放入Vector，不同則放入Hashtable 並new一個重新放
				if(key.equals(LAST_KEY)){
					vPRE_LESS_NO.add(iPRE_LESS_NO);
				} else {
					
					vLESS_NO.add(LAST_KEY);
					htLESS_NO.put(LAST_KEY, vPRE_LESS_NO);

					vPRE_LESS_NO = new Vector();
					vPRE_LESS_NO.add(iPRE_LESS_NO);
				}
			} else {
				// 第一筆
				vPRE_LESS_NO.add(iPRE_LESS_NO);
			}
			
			// 最後一筆
			if(i == ret.length - 1){
				vLESS_NO.add(key);
				htLESS_NO.put(key, vPRE_LESS_NO);
			}

			vKEY.add(key);

		} // forloop end

		// 彙整表格資料 
		Vector vData = new Vector();
		Vector vRet = new Vector();

		// 每個課程名稱 + 序號
		for(int i = 0 ; i < vLESS_NO.size() ; i++ ){
			String key = (String)vLESS_NO.get(i);
			
			String[] TEMP_LESS = key.split("@@");
			String iLESS_NO = TEMP_LESS[0].trim();							// 課程名稱
			String iSKEY = TEMP_LESS[1].trim();								// 序號

			vData.add(iLESS_NO);
			vData.add(iSKEY);

			// 每個課程名稱的全部先修課程
			Vector ivPRE_LESS_NO = (Vector)htLESS_NO.get(key);
			for(int j = 0 ; j < ivPRE_LESS_NO.size() ; j++ ){
				String jPRE_LESS_NO = (String)ivPRE_LESS_NO.get(j);

				vData.add(jPRE_LESS_NO);
			}

			vRet.add( (String[])vData.toArray(new String[0]) );
			vData = new Vector();

		} // forloop end

		ret = (String[][])vRet.toArray(new String[0][0]);
		setTableData("table1", ret);
		setTableData("table2", ret);

		return false;
	}

	/**
	 * 修改
	 * return boolean
	 * @throws Throwable
	 */
	public boolean UPDATE() throws Throwable {
		String LESS_NO = getQueryValue("table1.LESS_NO").trim();					// 課程名稱
		Vector vSQL = new Vector();
		
		/**
		 * 這是因為課程有卡CONDITION_B06和SUPER，假設資料庫有10筆課程，
		 * 登入者卡控權限只能查出6筆，將此6筆在查詢時多放在table2
		 * 供DELETE-INSERT以及整理排序使用
		 */

		// 權限控管的畫面資料(隱藏的table2)
		Hashtable htTable2 = new Hashtable();
		Vector vTable2_KEY = new Vector();
		Set hsLESS_NO = new HashSet();

		ret = getTableData("table2");
		for(int i = 0 ; i < ret.length ; i++ ){
			String iLESS_NO = ret[i][0].trim();								// 課程編號
			String iSKEY = ret[i][1].trim();								// 先修序號
			String key = iLESS_NO + "@@" + iSKEY;

			// 刪除: 原查詢畫面資料
			sql = " delete from PRE_LESS "
				+ " where 1 = 1 ";
			if(LESS_NO.length() > 0){
				sql += " and LESS_NO = '" + convert.ToSql(LESS_NO) + "' ";
			} else {
				sql += " and LESS_NO = '" + convert.ToSql(iLESS_NO) + "' "
					 + " and SKEY = '" + convert.ToSql(iSKEY) + "' ";
			}
			vSQL.add(sql);

			hsLESS_NO.add(iLESS_NO);
		}

		// 不重複的 LESS_NO
		StringBuffer sbLESS_NO = new StringBuffer();
		String[] arrLESS_NO = new String[hsLESS_NO.size()];
		hsLESS_NO.toArray(arrLESS_NO);
		
		for(int i = 0 ; i < arrLESS_NO.length ; i++ ) {
			String iLESS_NO = arrLESS_NO[i].trim();
            sbLESS_NO.append("'" + iLESS_NO + "',");
        }
		if(sbLESS_NO.length() > 0){
			sbLESS_NO.deleteCharAt(sbLESS_NO.length() - 1);
		} else {
			sbLESS_NO.append("''");
		}

		// 查課程名稱(PQ03)
		Hashtable htLESSNAME = new Hashtable();

		sql = " select LESS_NO , LESSNAME "
			+ " from PQ03 ";
		
		ret = t.queryFromPool(sql);
		for(int i = 0 ; i < ret.length ; i++ ){
			String iLESS_NO = ret[i][0].trim();
			String iLESSNAME = ret[i][1].trim();

			htLESSNAME.put(iLESS_NO, iLESSNAME);
		}

		// 查不包含畫面的 DB資料(PRE_LESS)
		Hashtable htDBLESS_NO = new Hashtable();
		Hashtable htDBPRE_LESS_NO = new Hashtable();

		sql = " select LESS_NO , SKEY , PRE_LESS_NO "
			+ " from PRE_LESS "
			+ " where LESS_NO not in (" + sbLESS_NO.toString() + ") ";

		ret = t.queryFromPool(sql);
		for(int i = 0 ; i < ret.length ; i++ ){
			String iLESS_NO = ret[i][0].trim();								// 課程名稱
			String iPRE_LESS_NO = ret[i][2].trim();							// 序號
			String key1 = iLESS_NO + "@@" + iPRE_LESS_NO;
			String key2 = iPRE_LESS_NO + "@@" + iLESS_NO;
			String row = String.valueOf(i + 1);

			htDBLESS_NO.put(key1, row);
			htDBPRE_LESS_NO.put(key2, row);
		}
		
		// 合併畫面 和 DB資料 (不重複)
		ret = getTableData("table1");
		for(int i = 0 ; i < ret.length ; i++ ){
			String iLESS_NO = ret[i][0].trim();								// 課程名稱
			String iSKEY = ret[i][1].trim();								// 序號
			String key = iLESS_NO + "@@" + iSKEY;
			
			String[] arrTable2 = (String[])htTable2.get(key);
			if(arrTable2 == null){
				htTable2.put(key, ret[i]);
				vTable2_KEY.add(key);
			} else {
				htTable2.remove(key);
				vTable2_KEY.remove(key);
			}
		}

		// 重新排列
		Collections.sort(vTable2_KEY);

		// 重新排序整理: Hashtable(key: 課程編號 + 序號 , value: 新序號)
		Hashtable htLESS_NO_SKEY = new Hashtable();
		Vector vTEMP = new Vector();
		String count = "1";

		for(int i = 0 ; i < vTable2_KEY.size() ; i++ ){
			String key = (String)vTable2_KEY.get(i);
			
			String[] arrTable2 = (String[])htTable2.get(key);
			String iLESS_NO = arrTable2[0].trim();							// 課程名稱
			String iSKEY = arrTable2[1].trim();								// 序號

			if(i > 0){
				String LAST_KEY = (String)vTEMP.get(i - 1);

				// 若和前一筆相同跳過
				if(key.equals(LAST_KEY) ){
					continue;
				} else {
					String[] arrTEMP = LAST_KEY.split("@@");
					String LAST_LESS_NO = arrTEMP[0].trim();

					// 判斷課程名稱若不同，count 從0開始
					if(!iLESS_NO.equals(LAST_LESS_NO) ){
						count = "0";
					}
					
					count = operation.add(count, "1");
					htLESS_NO_SKEY.put(key, count);
				}
			} else {
				// 第一筆
				htLESS_NO_SKEY.put(key, count);
			}

			// 最後一筆
			if(i == ret.length - 1){
				htLESS_NO_SKEY.put(key, count);
			}
			
			vTEMP.add(key);
		}

		// 彙整資料
		Hashtable htALL_LESS_NO = new Hashtable(); 
		Vector vALL_LESS_NO = new Vector();

		Hashtable htLESS_NO = new Hashtable();
		Hashtable htPRE_LESS_NO = new Hashtable();
		
		ret = getTableData("table1");
		for(int i = 0 ; i < ret.length ; i++ ){
			Vector vPRE_LESS_NO = new Vector();

			String iLESS_NO = ret[i][0].trim();								// 課程編號
			String iSKEY = ret[i][1].trim();								// 先修序號
			String iPRE_LESS_NO1 = ret[i][2].trim();						// 先修課程編號1
			String iPRE_LESS_NO2 = ret[i][3].trim();						// 先修課程編號2
			String iPRE_LESS_NO3 = ret[i][4].trim();						// 先修課程編號3
			String iPRE_LESS_NO4 = ret[i][5].trim();						// 先修課程編號4
			String iPRE_LESS_NO5 = ret[i][6].trim();						// 先修課程編號5
			String row = String.valueOf(i + 1);
			String key = iLESS_NO + "@@" + iSKEY;
			
			if(iPRE_LESS_NO1.length() > 0){
				vPRE_LESS_NO.add(iPRE_LESS_NO1);
			}

			if(iPRE_LESS_NO2.length() > 0){
				vPRE_LESS_NO.add(iPRE_LESS_NO2);
			}

			if(iPRE_LESS_NO3.length() > 0){
				vPRE_LESS_NO.add(iPRE_LESS_NO3);
			}

			if(iPRE_LESS_NO4.length() > 0){
				vPRE_LESS_NO.add(iPRE_LESS_NO4);
			}

			if(iPRE_LESS_NO5.length() > 0){
				vPRE_LESS_NO.add(iPRE_LESS_NO5);
			}

			// 檢核
			{
				// 課程編號 (不可空白)
				if(!pCheck.checkValue(iLESS_NO, "NOTNULL") ){
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg001, "table1", i, 0, new String[]{row } );
					return false;
				}

				// 序號 (不可空白)
				if(!pCheck.checkValue(iSKEY, "NOTNULL") ){
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg002, "table1", i, 1, new String[]{row } );
					return false;
				}

				// 先修課程編號 (不可空白)
				if(vPRE_LESS_NO.size() == 0){
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg003, "table1", i, 2, new String[]{row } );
					return false;
				}
			}

			// 重新排序(SKEY) 由1開始
			iSKEY = (String)htLESS_NO_SKEY.get(key);
			ret[i][1] = iSKEY;
			
			// 判斷是否為相同課程
			if(htALL_LESS_NO.containsKey(iLESS_NO) ){
				Vector vLESS_NO = (Vector)htALL_LESS_NO.get(iLESS_NO);
				
				// 所有課程的 先修課程
				for(int j = 0 ; j < vLESS_NO.size(); j++ ){
					Vector vALLPRE_LESS_NO = (Vector)vLESS_NO.get(j);
					int foundCounter = 0;

					for(int k = 0 ; k < vPRE_LESS_NO.size() ; k++ ){
						String kPRE_LESS_NO = (String)vPRE_LESS_NO.get(k);
						
						// 判斷先修課程是否重複
						if(vALLPRE_LESS_NO.contains(kPRE_LESS_NO) ){
							foundCounter++;
						}
					}

					if(vPRE_LESS_NO.size() == foundCounter){
						pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg004, "table1", i, 2, new String[]{row, String.valueOf(j + 1) });
						return false;
					}

					foundCounter = 0;
					for(int k = 0 ; k < vALLPRE_LESS_NO.size() ; k++ ){
						String kALLPRE_LESS_NO = (String)vALLPRE_LESS_NO.get(k);
						
						// 判斷先修課程是否重複
						if(vPRE_LESS_NO.contains(kALLPRE_LESS_NO) ){
							foundCounter++;
						}
					}

					if(vALLPRE_LESS_NO.size() == foundCounter){
						pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg004, "table1", i, 2, new String[]{row, String.valueOf(j + 1) });
						return false;
					}
				}
			}

			vALL_LESS_NO.add(vPRE_LESS_NO);
			htALL_LESS_NO.put(iLESS_NO, vALL_LESS_NO);

			// 整理先修課程
			for(int j = 0 ; j < vPRE_LESS_NO.size(); j++ ){
				String jPRE_LESS_NO = (String)vPRE_LESS_NO.get(j);
				String jLESSNAME = (String)htLESSNAME.get(iLESS_NO);
				String jPRE_LESSNAME = (String)htLESSNAME.get(jPRE_LESS_NO);

				key = iLESS_NO + "@@" + jPRE_LESS_NO;
				
				String LESS_NO_key = iLESS_NO + "@@" + iLESS_NO;			// 判斷課程編號和先修課程重複
				String PRE_LESS_NO_key = jPRE_LESS_NO + "@@" + iLESS_NO;	// 判斷互為先修課程重複

				boolean bLESS_NO = htLESS_NO.containsKey(key);
				boolean bPRE_LESS_NO = htPRE_LESS_NO.containsKey(key);

				boolean bDBLESS_NO = htDBLESS_NO.containsKey(key);
				boolean bDBPRE_LESS_NO = htDBPRE_LESS_NO.containsKey(key);

				int line = 2 + j;

				// 畫面 課程編號是否和先修課程重複(LESS_NO)
				if(bLESS_NO){
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg005, "table1", i, line, new String[]{row, jPRE_LESSNAME } );
					return false;
				}

				// 畫面 是否互為先修課程(PRE_LESS_NO)
				if(bPRE_LESS_NO){
					String lineNum = (String)htPRE_LESS_NO.get(key);
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg006, "table1", i, line, new String[]{row, jPRE_LESSNAME, lineNum } );
					return false;
				}

				// DB 課程編號是否和先修課程重複(LESS_NO)
				if(bDBLESS_NO){
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg007, "table1", i, line, new String[]{row, jLESSNAME, jPRE_LESSNAME } );
					return false;
				}

				// DB 是否互為先修課程(PRE_LESS_NO)
				if(bDBPRE_LESS_NO){
					pMethod.errAndFocus(this, hr.Msg.hrm8b_F023.Msg008, "table1", i, line, new String[]{row, jPRE_LESSNAME } );
					return false;
				}

				// 放入Hashtable判斷重複
				htLESS_NO.put(LESS_NO_key, row);
				htPRE_LESS_NO.put(PRE_LESS_NO_key, row);

				// 寫入
				sql = " insert into PRE_LESS ( "
					+ " LESS_NO , SKEY , PRE_LESS_NO , MUSER , MDATE , MTIME "
					+ " ) values ( "
					+ " " + "'" + convert.ToSql(iLESS_NO)	  + "' , "
					+ " " + "'" + convert.ToSql(iSKEY)		  + "' , "
					+ " " + "'" + convert.ToSql(jPRE_LESS_NO) + "' , "
					+ " " + "'" + convert.ToSql(USER)		  + "' , "
					+ " " + "'" + convert.ToSql(today)		  + "' , "
					+ " " + "'" + convert.ToSql(time)		  + "' "
					+ " ) ";

				vSQL.add(sql);

			} // forloop end

		} // forloop end

		try{
			t.execFromPool( (String[])vSQL.toArray(new String[0]) );
			
			// 重排序號
			setTableData("table1", ret);
			setTableData("table2", ret);
			message(hr.Msg.hrm8b_F023.Msg009);
		}catch(Exception e){
			e.printStackTrace();
			message(hr.Msg.hrm8b_F023.Msg010);
			return false;
		}

		return false;
	}
}

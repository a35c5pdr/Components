/*
	version	modify_date	modify_user	description
	1.00	20220302	KEVIN		複製cpnyidsuperpq修改，教育系列改卡EDUCATION.SUPER
*/
package hr;
import javax.swing.*;
import jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;
/*
	dat:hrm7d
	function name:
	note:
	20160125 JILL ELINE : 
		L.3.13. 要依集團權限管理設定 登入者可查詢的公司別權限
	20160125 JENNY ELINE:
		還有很多功能會用到這隻,所以如果有例外條件的,要再另外做判斷
*/
public class F404pq extends bQuery{
	String FUNCID = "";
	talk t = null;
	String[][] ret = null;
	String sql = "";
	String name = "";
	String MUSER = "";
	String DBN = "N";
	String ISNULL = "";
	public String getFilter() throws Throwable{
		t = getTalk();
		name = getName();
		MUSER = getUser();
		DBN = pDB.getDBN(t);
		ISNULL = pDB.getISNULL(t);
		FUNCID = getFunctionID();
		String SUPER = (String)get("EDACATION.SUPER");
		//20160122 JILL ELINE begin: 要依集團權限管理設定 登入者可查詢的公司別權限
		//return SUPER;
		//是否有買 7g (集團版)
		boolean bHrm7G = hr.common5.bHRM7G(t);
		//判斷是否可以看空白公司別的群組
		boolean bNullCPNYID = false;
		
		//20200330 DRACO PATRICK begin:174374-[V8(這一鍋)_M.8.6.其他所得轉帳作業_hrm8d.dat-patrick-20200327]
		//有些功能(例如M.8.6)想要加入一些HRCONFIG設定的公司別
		StringBuffer sbCOLICNID = new StringBuffer();
		String COLICNID = "";
		String fncname = getFunctionName();
		//20200330 DRACO PATRICK end:174374-[V8(這一鍋)_M.8.6.其他所得轉帳作業_hrm8d.dat-patrick-20200327]
		
		//取出管理的公司別
		if(bHrm7G){
			//集團版 依據可以管理的公司別 去選擇

			//如果系統有5間公司 該人員若可以管理5間公司 代表 群組可以選5間公司+空白公司的群組
			sql = " select count(*) from COMPANY";
			ret = t.queryFromPool(sql , 360);
			String allCOMPANY = ret[0][0];

			sql = " select ID from QUERY_RIGHT "
				+ " where EMPID = '" + convert .ToSql(MUSER) + "'"
				+ " and FLAG = 'CPNYID'";
			//20200330 DRACO PATRICK begin:174374-[V8(這一鍋)_M.8.6.其他所得轉帳作業_hrm8d.dat-patrick-20200327]
			//有些功能(例如M.8.6)想要加入一些HRCONFIG設定的公司別
			if(COLICNID.length()>0)
			{
				sql+= " union select CPNYID from COMPANY where COLICNID in ("+COLICNID+")";
			}
			//20200330 DRACO PATRICK end:174374-[V8(這一鍋)_M.8.6.其他所得轉帳作業_hrm8d.dat-patrick-20200327]
			
			ret = t.queryFromPool(sql , 360);

			//是否可以管全公司
			bNullCPNYID = operation.compareTo(allCOMPANY , String.valueOf(ret.length)) == 0;
		}else{
			//企業版依登入者角色 可以知道管哪些人 這些人又屬於哪間公司
			//如果可以管3間公司 代表 群組可以選3間公司 但不含空白
			//只有100001群組可以管理空白公司別的群組

			//是否是 100001 的群組
			bNullCPNYID = "Y".equals(getValue("is100001").trim());

			sql = " select distinct CPNYID from HRUSER "
				+ " where 1 = 1 " + (String) get("CONDITION_B06") ;
			//20200330 DRACO PATRICK begin:174374-[V8(這一鍋)_M.8.6.其他所得轉帳作業_hrm8d.dat-patrick-20200327]
			//有些功能(例如M.8.6)想要加入一些HRCONFIG設定的公司別
			if(COLICNID.length()>0)
			{
				sql+= " union select CPNYID from COMPANY where COLICNID in ("+COLICNID+")";
			}
			//20200330 DRACO PATRICK end:174374-[V8(這一鍋)_M.8.6.其他所得轉帳作業_hrm8d.dat-patrick-20200327]
			ret = t.queryFromPool(sql , 360);
		}
		
		//取出可管理的公司別
		StringBuffer sbCPNYID = new StringBuffer();
		if(ret.length > 0){
			sbCPNYID.append("'" + convert.ToSql(ret[0][0].trim()) + "' ");
			for(int i = 1 ; i < ret.length ; i++){
				sbCPNYID.append(", '" + convert.ToSql(ret[i][0].trim()) + "' ");
			}
		}
		
		//20160125 JENNY ELINE:還有很多功能會用到這隻,感覺都要判斷權限,如果有例外的話,再另外判斷
		//					   最原本是只需要加 SUPER
		sql = SUPER;
		//if("L.3.13".equals(FUNCID))
		//20170627 ANDY DANIEL begin:因為5版是只卡SUPER 案例為汎德是v5轉v7所以用以下判斷會與原規則不同，改用CONFIG判斷
		//insert into HRCONFIG values('L71_QUERY','Y','L71可查詢到所有公司別')
		String L71_QUERY = (String)get("HRCONFIG_L71_QUERY");
		if(L71_QUERY != null && "Y".equals(L71_QUERY)){
		}else
		//20170627 ANDY DANIEL end:因為5版是只卡SUPER 案例為汎德是v5轉v7所以用以下判斷會與原規則不同，改用CONFIG判斷
		{
			//取出可管理公司別的群組
			sql = " and ( 1 = 1 ";
			//可管理的公司別
			if(sbCPNYID.length() > 0){
				sql += " and CPNYID in (" + sbCPNYID.toString() + ") ";
			}
			//判斷是否可以看空白公司別的群組
			if(bNullCPNYID){
				if(sbCPNYID.length() > 0){
					sql += " or ";
				}else{
					sql += " and ";
				}
				sql += " isnull(CPNYID , '"+ ISNULL +"') = '"+ ISNULL +"' ";
			}
			sql += " )"
				 + " order by 1 ";
		}
		return sql;
		//20160122 JILL ELINE end: 要依集團權限管理設定 登入者可查詢的公司別權限
	}
	public String getInformation(){
		return "---------------CPNYID(\u516c\u53f8\u5225).setf_defined_where_clause()----------------";
	}
}
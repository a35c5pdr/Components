/*
 version	modify_date		modify_user		description
 1.00		20220302		KEVIN			修改CPNYID.SUPER
*/
package hr;
import jcx.jform.hproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

public class F504pl extends hproc{
	talk t = null;
	String sql = "";
	String[][] ret = null;
	String DATELIST = "";
	String BACK = "history.back();";
	String name = "";
	StringBuffer sbSQL = new StringBuffer();
	public String action(String value)throws Throwable{
		// 可自定HTML版本各欄位的預設值與按鈕的動作 
		// 傳入值 value 
		t = getTalk();
		name = getName().trim();
		DATELIST = (String)get("SYSTEM.DATELIST");
		if ( DATELIST == null ){
			sbSQL.setLength(0);
			sbSQL.append("select DATELIST from HRSYS");
			ret = t.queryFromPool( sbSQL.toString() );
			if ( ret.length > 0 ){
				DATELIST = ret[0][0].trim();
			}else{
				DATELIST = "B";
			}
			put("SYSTEM.DATELIST" , DATELIST);
		}

		if ( "FORM_STATUS".equals(name) ){
			FORM_STATUS();
		}else if ( "CLEAR1".equals(name) || "CLEAR2".equals(name) || "CLEAR3".equals(name) ){
			CLEAR();
		}else if ( "LOAD".equals(name) ){
			LOAD();
		}else if ( "ADD".equals(name) ){
			ADD(BACK);
		}else if ( "INS".equals(name) || "EXEC".equals(name) ){
			EXEC(t , ret , sbSQL , DATELIST , BACK , name);
		}else if ( "QUERY".equals(name) ){
			QUERY();
		}else if ( "table1.DETAIL".equals(name) || "table1.UPDATE".equals(name) || "table1.DEL".equals(name) ){
			DETAIL();
		}else if ( "BACK".equals(name) ){
			goBACK();
		}else if ( "SEND".equals(name) ){
			SEND(t , ret , sbSQL , DATELIST , BACK);
		}else if ( "DEL".equals(name) ){
			DEL(t , ret , sbSQL , BACK);
		}
		return value;
	}
	
	public void FORM_STATUS() throws Exception {
		String CPNYID = (String)get("iCPNYID");
		String IssueDate = datetime.getToday("YYYYmmdd");
		if ( "A".equals(DATELIST) ){
			IssueDate = datetime.getToday("yymmdd");
		}
		
		setValue("CPNYID" , CPNYID);
		setValue("IssueDate" , IssueDate);
		return;
	}
	
	public void CLEAR() throws Exception {
		if ( "CLEAR1".equals(name) ){
			setValue("CPNYID" , "");
		}else if ( "CLEAR2".equals(name) ){
			setValue("QId" , "");
		}else{
			setValue("IssueDate" , "");
		}
		return;
	}
	
	public void ADD(String BACK) throws Exception {
		String CPNYID = getValue("CPNYID").trim();
		String QId = getValue("QId").trim();
		StringBuffer sbSQL = new StringBuffer();
		if ( CPNYID.length() == 0 ){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("請輸入公司別!!" , BACK);
			message(hr.Msg.hrm8b_F504.Msg001, BACK);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		//20130102 TIM 評估問卷代號必須輸入
		if(QId.length() == 0){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("請輸入評估問卷代號", BACK);
			message(hr.Msg.hrm8b_F504.Msg002, BACK);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		setValue("IssueDate" , "");
		setVisible("INS" , true);
		setVisible("DEL" , false);
		setVisible("EXEC" , false);

		//20130102 TIM 新增時要帶的資料 begin:
		sbSQL.setLength(0);
		sbSQL.append(" select distinct LESS_KEY , ISSUEDATE");
		sbSQL.append(" from TRESULTAPPRAISAL");
		sbSQL.append(" where QID = '"+ convert.ToSql(QId) +"'");
		sbSQL.append(" and CPNYID = '"+ convert.ToSql(CPNYID) +"'");
		ret = t.queryFromPool( sbSQL.toString() );
		if ( ret.length == 0 ){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("查無資料!!", BACK);
			message(hr.Msg.hrm8b_F504.Msg003, BACK);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		
		String LESS_KEY = ret[0][0].trim();
		setValue("LESS_KEY" , LESS_KEY);
		if(DATELIST.equals("A")){
			if(ret[0][1].trim().length()!=0){
				ret[0][1] = convert.ac2roc(ret[0][1].trim());
			}
		}
		setValue("IssueDate",ret[0][1].trim());
		
		sbSQL.setLength(0);
		sbSQL.append("select a.LESS_NO , (select DEP_NAME from HRUSER_DEPT_BAS where DEP_NO = a.DEPT_NO1)");
		sbSQL.append(" , b.LESS_NO1 , b.LESS_NO2 , b.LESS_DESC , a.SDATE , a.EDATE , a.TOT_HR , a.CPNYID");
		sbSQL.append(" from PQ22 a , PQ03 b");
		sbSQL.append(" where a.LESS_NO = b.LESS_NO");
		sbSQL.append(" and LESS_KEY = '" + convert.ToSql(LESS_KEY) + "'");
		ret = t.queryFromPool( sbSQL.toString() );
		if ( ret.length > 0 ){
			setValue("LESS_NO" , ret[0][0]);
			setValue("DEP_NAME" , ret[0][1]);
			setValue("LESS_NO1" , ret[0][2]);
			setValue("LESS_NO2" , ret[0][3]);
			setValue("LESS_DESC" , ret[0][4]);
			String SDATE = ret[0][5];
			String EDATE = ret[0][6];
			if(DATELIST.equals("A")){
				if(SDATE.trim().length()!=0){
					SDATE = convert.ac2roc(SDATE);
				}
				if(EDATE.trim().length()!=0){
					EDATE = convert.ac2roc(EDATE);
				}
			}
			setValue("SDATE" , SDATE);
			setValue("EDATE" , EDATE);
			setValue("TOT_HR" , ret[0][7]);
		}
			
		sbSQL.setLength(0);
		sbSQL.append(" select distinct ISSUEDATE from TRESULTAPPRAISAL where  QID = '"+ convert.ToSql(QId) +"'");
		ret = t.queryFromPool( sbSQL.toString() );
		if( ret.length != 0 && ret[0][0].trim().length() != 0 ){
			if(DATELIST.equals("A")){
				ret[0][0] = convert.ac2roc(ret[0][0].trim());
			}
			setValue("IssueDate",ret[0][0]);
			setValue("Msg" , "本問卷已發出");
			setVisible("SEND" , false);
		}else{
			setValue("IssueDate","");
			setValue("Msg" , "");
			setVisible("SEND" , true);
		}
		//20130102 TIM 新增時要帶的資料 end

		return;
	}
	
	public void goBACK() throws Throwable {
		call("FORM_STATUS");
		call("CLEAR2");
		return;
	}
	
	public void LOAD() throws Exception {
		String QId = getValue("QId").trim();
		if(QId.trim().length()==0){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("請先選擇問卷代號");
			message(hr.Msg.hrm8b_F504.Msg004);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		
		String LESS_KEY = getValue("LESS_KEY").trim();
		
		String[][] table1 = getTableData("table1");
		Vector v_showData = new Vector();
		Vector v_empidTmp = new Vector();
		for(int i=0;i<table1.length;i++){
			Vector v_data = new Vector();
			v_data.add(QId);
			if(v_empidTmp.indexOf(table1[i][1])!=-1){//暫存 用來比對是否輸入過
				continue;
			}
			v_empidTmp.add(table1[i][1].trim());
			v_data.add(table1[i][1].trim());
			v_data.add(table1[i][2].trim());
			v_data.add(table1[i][3].trim());
			v_showData.add((String[])v_data.toArray(new String[0]));
		}
		
		sbSQL.setLength(0);
		sbSQL.append("select EMPID,(select HECNAME from HRUSER where EMPID = PQ23.EMPID),(select DEPT_NO from HRUSER where EMPID = PQ23.EMPID)");
		sbSQL.append(" from PQ23 where LESS_KEY = '"+ convert.ToSql(LESS_KEY) +"'");
		ret = t.queryFromPool( sbSQL.toString() );
		for(int i=0;i<ret.length;i++){
			Vector v_data = new Vector();
			v_data.add(QId);
			if(v_empidTmp.indexOf(ret[i][0])!=-1){//暫存 用來比對是否輸入過
				continue;
			}
			v_empidTmp.add(ret[i][0].trim());
			v_data.add(ret[i][0].trim());
			v_data.add(ret[i][1].trim());
			v_data.add(ret[i][2].trim());
			v_showData.add((String[])v_data.toArray(new String[0]));
			System.err.println("EMPID: "+ ret[i][0].trim() + ", HECNMAE: " + ret[i][1].trim());
		}
		
		setTableData("table1",(String[][])v_showData.toArray(new String[0][0]));
		return;
	}
	
	public void EXEC(talk t , String[][] ret , StringBuffer sbSQL , String DATELIST , String BACK , String name) throws Exception {
		String[][] table1 = getTableData("table1");
		String QId = getValue("QId");
		String CPNYID = getValue("CPNYID");
		String LESS_KEY = getValue("LESS_KEY");
		
		if( QId.trim().length() == 0 ){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("請選擇評估問卷代號" , BACK);
			message(hr.Msg.hrm8b_F504.Msg005, BACK);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		if( table1.length == 0 ){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("至少設定一筆受調人員" , BACK);
			message(hr.Msg.hrm8b_F504.Msg006, BACK);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		
		if ( "INS".equals(name) ){
			sbSQL.setLength(0);
			sbSQL.append("select ISSUEDATE from TRESULTANSWER a,TRESULTAPPRAISAL b");
			sbSQL.append(" where a.QID = b.QID and a.QID = '"+ convert.ToSql(QId) +"'");
			ret = t.queryFromPool( sbSQL.toString() );
			if(ret.length > 0 ){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message("該筆評估問卷已新增!!" , BACK);
				message(hr.Msg.hrm8b_F504.Msg007, BACK);
				//20190314 LICK COULSON end 訊息翻譯
				return;
			}
		}
		
		HashMap hm_depChief = new HashMap();
		sbSQL.setLength(0);
		
		//20171122 DRACO ELINE begin:主管往上找到有為止
		/*
		sbSQL.append("select a.DEP_NO , a.DEP_CHIEF , (select DEP_CHIEF from HRUSER_DEPT_BAS where DEP_NO = a.PARENT_NO)");
		sbSQL.append(" from HRUSER_DEPT_BAS a");
		ret = t.queryFromPool( sbSQL.toString() );
		for(int i=0;i<ret.length;i++){
			hm_depChief.put(ret[i][0].trim(),ret[i]);
		}
		*/
		sbSQL.append(" select a.DEP_NO , a.DEP_CHIEF , '' , a.DEP_REL");//2:留空給程式用，3:用來往上找主管
		sbSQL.append(" from HRUSER_DEPT_BAS a order by DEP_REL");//這個ORDER很重要，要改請謹慎
		ret = t.queryFromPool( sbSQL.toString() );
		for(int i=0 ; i<ret.length ; i++)
		{
			String iDEPNO = ret[i][0].trim();
			String iCHIEF = ret[i][1].trim();
			String iREL = ret[i][3].trim();
			if(iCHIEF.length() == 0)
			{
				for(int j=i-1 ; j>=0 ; j--)
				{
					String jDEPNO = ret[j][0].trim();
					String jCHIEF = ret[j][1].trim();
					String jREL = ret[j][3].trim();
					if( ! iREL.startsWith(jREL))//往上面找不是本單位的上級主管了，所以停下
					{
						break;
					}
					else if(jCHIEF.length()>0)//找到有主管的上級單位了，所以指定好主管停下
					{
						ret[i][1] = jCHIEF;
						break;
					}
				}
			}
			hm_depChief.put(ret[i][0].trim(),ret[i]);
		}
		//20171122 DRACO ELINE end:主管往上找到有為止
		
		Vector vSQL = new Vector();
		
		sbSQL.setLength(0);
		sbSQL.append("delete from TRESULTANSWER");
		sbSQL.append(" where QID = '"+ convert.ToSql(QId) +"'");
		vSQL.add( sbSQL.toString() );
		
		Hashtable htChk = new Hashtable();
		
		for(int i = 0; i<table1.length; i++){
			String iQId = table1[i][0].trim();
			String iEMPID = table1[i][1].trim();
			String iHECNAME = table1[i][2].trim();
			String iDEPT_NO = table1[i][3].trim();
			
			if ( htChk.containsKey(iEMPID) ){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message("第"+(i+1)+"筆 與 第"+(htChk.get(iEMPID))+"筆資料重覆,請檢查!!" , BACK);
				message(hr.Msg.hrm8b_F504.Msg008, new String[]{String.valueOf(i+1), (String)htChk.get(iEMPID)} , BACK);
				//20190314 LICK COULSON end 訊息翻譯
				return;
			}else{
				htChk.put(iEMPID , ""+(i+1));
			}
			
			String[] dep_chief_Arr = (String[])hm_depChief.get(iDEPT_NO);
			
			if ( dep_chief_Arr == null ){
				continue;
			}
			
			String dep_chief = dep_chief_Arr[1];
			if( dep_chief.trim().equals(table1[i][1].trim()) ){
				dep_chief = dep_chief_Arr[2]; 
			}
			
			sbSQL.setLength(0);
			sbSQL.append("insert into TRESULTANSWER(QID,CPNYID,RATEDEMPID,RATINGEMPID)");
			sbSQL.append(" values(");
			sbSQL.append("'"+ convert.ToSql(iQId)+"' , '"+ convert.ToSql(CPNYID) +"'");
			sbSQL.append(" , '"+ convert.ToSql(iEMPID) +"' , '"+ convert.ToSql(dep_chief) + "')");
			vSQL.add( sbSQL.toString() );
		}
		
		try{
			t.execFromPool( (String[])vSQL.toArray(new String[0]) );
			if ( "INS".equals(name) ){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message("資料新增成功!!");
				message(hr.Msg.hrm8b_F504.Msg009);
				//20190314 LICK COULSON end 訊息翻譯
			}else{
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message("資料修改成功!!");
				message(hr.Msg.hrm8b_F504.Msg010);
				//20190314 LICK COULSON end 訊息翻譯
			}
		}catch (Exception e){
			
			if ( "INS".equals(name) ){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message("資料新增失敗!!" , BACK);
				message(hr.Msg.hrm8b_F504.Msg011, BACK);
				//20190314 LICK COULSON end 訊息翻譯
			}else{
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message("資料修改失敗!!" , BACK);
				message(hr.Msg.hrm8b_F504.Msg012, BACK);
				//20190314 LICK COULSON end 訊息翻譯
			}
		}
		return;
	}
	
	public void QUERY() throws Exception {
		String CPNYID = getValue("CPNYID").trim();
		String QId = getValue("QId").trim();
		String IssueDate = getValue("IssueDate").trim();
		//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
		//String SUPERA = (String)get("CPNYID.SUPERA");
		String SUPERA = (String)get("EDUCATION.SUPERA");
		//20220302 KEVIN ELIZA end:修改CPNYID.SUPER

		
		if( IssueDate.trim().length() != 0 ){
			if( DATELIST.trim().equals("A") ){
				if( !check.isDate(IssueDate,"yymmdd") ){
					
					//20190314 LICK COULSON begin 訊息翻譯
					//message("發出日期格式錯誤");
					message(hr.Msg.hrm8b_F504.Msg013);
					//20190314 LICK COULSON end 訊息翻譯
					return;
				}
				IssueDate = convert.roc2ac(IssueDate);
			}else{
				if(!check.isDate(IssueDate,"YYYYmmdd")){
					//20190314 LICK COULSON begin 訊息翻譯
					//message("發出日期格式錯誤");
					message(hr.Msg.hrm8b_F504.Msg013);
					//20190314 LICK COULSON end 訊息翻譯
					return;
				}
			}
		}
/*
		sbSQL.setLength(0);
		sbSQL.append("select distinct a.CPNYID , a.QId , a.LESS_KEY , a.IssueDate , '詳細資料' , '修改' , '刪除'");
		sbSQL.append(" from TResultAppraisal a , TResultAnswer b");
		sbSQL.append(" where a.a.QId = b.QId");
*/
		//20121221 TIM 不需要TResultAnswer b的樣子阿 begin:
		/*
		sbSQL.append("select distinct a.CPNYID , (select x.COCNAME from COMPANY x where x.CPNYID = a.CPNYID)");
		sbSQL.append(" , a.QId , a.LESS_KEY , (select y.LESSNAME from PQ03 y where y.LESS_NO = c.LESS_NO)");
		sbSQL.append(" , a.IssueDate , '詳細資料' , '修改' , '刪除'");
		sbSQL.append(" from TResultAppraisal a , TResultAnswer b , PQ22 c");
		sbSQL.append(" where a.QId = b.QId");
		sbSQL.append(" and a.LESS_KEY = c.LESS_KEY");
		*/
		sbSQL.append("select distinct a.CPNYID , (select x.COCNAME from COMPANY x where x.CPNYID = a.CPNYID)");
		sbSQL.append(" , a.QID , a.LESS_KEY , (select y.LESSNAME from PQ03 y where y.LESS_NO = b.LESS_NO)");
		sbSQL.append(" , a.ISSUEDATE , '詳細資料' , '修改' , '刪除'");
		sbSQL.append(" from TRESULTAPPRAISAL a , PQ22 b");
		sbSQL.append(" where a.LESS_KEY = b.LESS_KEY and a.CPNYID=b.CPNYID");
		if( CPNYID.trim().length() != 0){
			sbSQL.append(" and a.CPNYID = '"+ convert.ToSql(CPNYID) +"'");
		}
		if( QId.trim().length() != 0 ){
			sbSQL.append(" and a.QID = '"+ convert.ToSql(QId) +"'");
		}
		if( IssueDate.trim().length() != 0 ){
			sbSQL.append(" and a.ISSUEDATE = '"+ convert.ToSql(IssueDate) +"'");
		}
		
		ret = t.queryFromPool( sbSQL.toString() );
		if( ret.length != 0 ){
			for(int i=0; i<ret.length; i++){
				String iIssueDate = ret[i][5].trim();
				if( iIssueDate.length() != 0 ){
					if( "A".equals(DATELIST) ){
						ret[i][5] = convert.ac2roc(iIssueDate);
					}
					ret[i][7] = "";
					ret[i][8] = "";
				}
			}
		}else{
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("查無資料!!");
			message(hr.Msg.hrm8b_F504.Msg003);
			//20190314 LICK COULSON end 訊息翻譯
		}
		setTableData("table1" , ret);
		return;
	}
	
	public void DETAIL() throws Exception {
		String tCPNYID = getValue("tCPNYID");
		String tQId = getValue("tQId");
		String tLESS_KEY = getValue("tLESS_KEY");
		String tIssueDate = getValue("tIssueDate");

		setValue("CPNYID" , tCPNYID);
		setValue("QId" , tQId);
		setValue("LESS_KEY" , tLESS_KEY);
		setValue("IssueDate" , tIssueDate);
		
		sbSQL.setLength(0);
		sbSQL.append("select a.LESS_NO , (select DEP_NAME from HRUSER_DEPT_BAS where DEP_NO = a.DEPT_NO1)");
		sbSQL.append(" , b.LESS_NO1 , b.LESS_NO2 , b.LESS_DESC , a.SDATE , a.EDATE , a.TOT_HR , a.CPNYID");
		sbSQL.append(" from PQ22 a,PQ03 b");
		sbSQL.append(" where a.LESS_NO = b.LESS_NO");
		sbSQL.append(" and LESS_KEY='"+ convert.ToSql(tLESS_KEY) +"'");
		ret = t.queryFromPool( sbSQL.toString() );
		if ( ret.length > 0 ){
			setValue("LESS_NO" , ret[0][0]);
			setValue("DEP_NAME" , ret[0][1]);
			setValue("LESS_NO1" , ret[0][2]);
			setValue("LESS_NO2" , ret[0][3]);
			setValue("LESS_DESC" , ret[0][4]);
			String SDATE = ret[0][5];
			String EDATE = ret[0][6];
			if(DATELIST.equals("A")){
				if(SDATE.trim().length()!=0){
					SDATE = convert.ac2roc(SDATE);
				}
				if(EDATE.trim().length()!=0){
					EDATE = convert.ac2roc(EDATE);
				}
			}
			setValue("SDATE" , SDATE);
			setValue("EDATE" , EDATE);
			setValue("TOT_HR" , ret[0][7]);
		}
		
		sbSQL.setLength(0);
		sbSQL.append("select isnull(ISSUEDATE,'') from TRESULTAPPRAISAL where QID = '"+ convert.ToSql(tQId) +"'");
		ret = t.queryFromPool( sbSQL.toString() );
		
		if( ret.length != 0 && ret[0][0].trim().length() != 0 ){
			if(DATELIST.equals("A")){
				ret[0][0] = convert.ac2roc(ret[0][0].trim());
			}
			setValue("IssueDate",ret[0][0]);
			setValue("Msg" , "本問卷已發出");
			setVisible("SEND" , false);
			setVisible("LOAD",false);
		}else{
			setValue("IssueDate","");
			setValue("Msg" , "");
			setVisible("SEND" , true);
		}
		
		sbSQL.setLength(0);
		sbSQL.append("select '"+ tQId +"' , RATEDEMPID , b.HECNAME , b.DEPT_NO");
		sbSQL.append(" from TRESULTANSWER a , HRUSER b");
		sbSQL.append(" where QID = '"+ convert.ToSql(tQId) +"'");
		sbSQL.append(" and a.RATEDEMPID = b.EMPID");
		ret = t.queryFromPool( sbSQL.toString() );
		setTableData("table1" , ret);
		
		if ( "table1.DETAIL".equals(name) || "table1.DEL".equals(name) ){
			getTableButton("table1" , 0).setVisible(false);
			getTableButton("table1" , 1).setVisible(false);
			getTableButton("table1" , 2).setVisible(false); 
			setVisible("INS" , false);
			setVisible("EXEC" , false);
			setVisible("LOAD", false);		//20121221 TIM 詳細資料的話按鈕要隱藏
			setEditable("table1", false);	//20121221 TIM 詳細資料的話表格不可編輯
			setEditable("QId" , false);
			if ( "table1.DETAIL".equals(name) ){
				setVisible("DEL" , false);
			}else{
				setVisible("DEL" , true);
			}
		}else if ( "table1.UPDATE".equals(name) ){
			setEditable("QId" , false);
			setVisible("INS" , false);
			setVisible("DEL" , false);
			setVisible("EXEC" , true);
		}
		return;
	}
	
	public void SEND(talk t , String[][] ret , StringBuffer sbSQL , String DATELIST , String name) throws Exception {
		String QId = getValue("QId").trim();
		String today = datetime.getToday("YYYYmmdd");
		String today1 = datetime.getToday("YYYYmmdd");//顯示用
		if ( "A".equals(DATELIST) ){
			today1 = datetime.getToday("yymmdd");
		}
		
		sbSQL.setLength(0);
		sbSQL.append("select QID , RATEDEMPID , CPNYID");
		sbSQL.append(" from TRESULTANSWER");
		sbSQL.append(" where QID = '" + convert.ToSql(QId) + "'");
		ret = t.queryFromPool( sbSQL.toString() );
		if ( ret.length == 0 ){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("請先執行新增處理");
			message(hr.Msg.hrm8b_F504.Msg014);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		
		Vector vSQL = new Vector();
		for (int i=0; i<ret.length; i++){
			String iQId = ret[i][0].trim();
			String iRatedEmpid = ret[i][1].trim();
			String iCPNYID = ret[i][2].trim();
			String now = getNow();
			
			sbSQL.setLength(0);
			sbSQL.append("insert into TRESULTANSWER_FLOWC");
			sbSQL.append(" values('"+ convert.ToSql(iQId) +"' , '"+ convert.ToSql(iRatedEmpid) +"'");
			sbSQL.append(" , '待處理' , '"+ convert.ToSql(iRatedEmpid) +"','" + now + "','待處理') ");
			vSQL.add( sbSQL.toString() );
			
			Date dd1 = new Date();
			now = getNow("AC",new Date(dd1.getTime() + 1100));
			
			sbSQL.setLength(0);
			sbSQL.append("insert into TRESULTANSWER_FLOWC_HIS");
			sbSQL.append(" values('"+ convert.ToSql(iQId) +"' , '"+ convert.ToSql(iRatedEmpid) +"'");
			sbSQL.append(" , '待處理' , '"+ convert.ToSql(iRatedEmpid) +"' , '" + now + "' , '待處理') ");
			vSQL.add( sbSQL.toString() );
		}
		
		sbSQL.setLength(0);
		sbSQL.append("update TRESULTAPPRAISAL");
		sbSQL.append(" set ISSUEDATE = '" + today + "'");
		sbSQL.append(" where QID = '"+ convert.ToSql(QId) +"'");
		vSQL.add( sbSQL.toString() );
		
		t.execFromPool( (String[])vSQL.toArray(new String[0]) );
		
		setValue("IssueDate",today1);
		setValue("Msg" , "本問卷已發出");
		setVisible("LOAD",false);
		setVisible("SEND" , false);
		
		//20190314 LICK COULSON begin 訊息翻譯
		//message("問卷已發出");
		message(hr.Msg.hrm8b_F504.Msg018);
		//20190314 LICK COULSON end 訊息翻譯
		return;
	}
	
	public void DEL(talk t , String[][] ret , StringBuffer sbSQL , String BACK) throws Throwable {
	/*
		int option = showConfirmDialog("是否刪除此筆資料?");
		if(option != 0){
			return ;
		}
	*/
		String QId = getValue("QId").trim();
		String CPNYID = getValue("CPNYID").trim();
		String LESS_KEY = getValue("LESS_KEY").trim();
		String IssueDate = getValue("IssueDate").trim();
		
		if(QId.trim().length()==0){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("請選擇評估問卷代號");
			message(hr.Msg.hrm8b_F504.Msg005);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		
		if(IssueDate.length()!=0 ){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("該筆評估問卷代號已發送，不可刪除!!");
			message(hr.Msg.hrm8b_F504.Msg015);
			//20190314 LICK COULSON end 訊息翻譯
			return;
		}
		
		Vector vSQL = new Vector();
		sbSQL.setLength(0);
		sbSQL.append("delete from TRESULTANSWER where QID = '"+ convert.ToSql(QId) +"'");
		vSQL.add( sbSQL.toString() );
		
		try{
			t.execFromPool( (String[])vSQL.toArray(new String[0]) );
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("資料刪除成功!!");
			message(hr.Msg.hrm8b_F504.Msg016);
			//20190314 LICK COULSON end 訊息翻譯
		}catch (Exception e){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("資料刪除失敗!!" , BACK);	
			message(hr.Msg.hrm8b_F504.Msg017, BACK);
			//20190314 LICK COULSON end 訊息翻譯
		}
		return;
	}
	
	public String getNow(String type,Date gc)throws Exception
	{
		if(type.equals("ROC"))
		{
			//new Exception().printStackTrace();
			return "" + ((gc.getYear() * 10000 + (gc.getMonth() + 1) * 100 + gc.getDate()) - 110000) + " " + getTime1(gc);
		} else {
			return "" + ((gc.getYear() * 10000 + (gc.getMonth() + 1) * 100 + gc.getDate()) + 19000000) + " " + getTime1(gc);
			//return jcx.util.datetime.getToday("YYYYmmdd")+" "+jcx.util.datetime.getTime("h:m:s");
		}
	}
	
	private String getTime1(Date d1)
	{
		int hours = d1.getHours() + d1.getTimezoneOffset() / 60 + 8;
		if(hours > 23) hours = hours - 24;
		int min = d1.getMinutes();
		int sec = d1.getSeconds();

		String a = "" + hours;
		String b = "" + min;
		String c = "" + sec;
		if(a.length() == 1) a = "0" + a;
		if(b.length() == 1) b = "0" + b;
		if(c.length() == 1) c = "0" + c;
		//int time = hours * 10000 + min * 100 + sec;
		return a + ":" + b + ":" + c;
	}
	
	public String getInformation(){
		return "---------------ADD(\u65b0\u589e).html_action()----------------";
	}
}

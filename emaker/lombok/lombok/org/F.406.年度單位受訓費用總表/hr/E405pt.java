/*
 version	modify_date		modify_user		description
 1.00		20220302		KEVIN			修改CPNYID.SUPER
*/
package hr;
import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;
/*
		程式設計師：PEGGY
		程式功能：E.6.6.年度單位受訓費用總表(E5006)_查詢按鈕程式
		區域變數：
		函式：
		用到的外部程式：
		修改記錄：
						2005031101PEGGY：初次上線
						20150107  JENNY：改成符合現在規範
*/
public class E405pt extends bTransaction{
	talk t = null;
	String[][] ret = null;
	String sql = "";
	String DBN = "N";
	String PLUS = "+";
	String DATELIST = "B";
	String MUSER = "";
	public boolean action(String value)throws Throwable{
		// 回傳值為 true 表示執行接下來的資料庫異動或查詢
		// 回傳值為 false 表示接下來不執行任何指令
		// 傳入值 value 為 "新增","查詢","修改","刪除","列印","PRINT" (列印預覽的列印按鈕),"PRINTALL" (列印預覽的全部列印按鈕) 其中之一
		message("");
		t = getTalk();
		MUSER = getUser();
		DATELIST = (String)get("SYSTEM.DATELIST");
		if("oracle".equals(t.f_db_type)){
			DBN = "";
			PLUS = "||";
		}
		
		//查詢
		if ("查詢".equals(value)){
			return QUERY();
		}
		return true;
	}

	//查詢
	public boolean QUERY() throws Exception{
		
		StringBuffer sbSQL = new StringBuffer();
		String sql = "";
		String YY1 = "";
		String YY = getQueryValue("YY").trim();    //年度
		String CPNYID = getQueryValue("CPNYID");
		//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
		//String SUPERA = (String)get("CPNYID.SUPERA");
		String SUPERA = (String)get("EDUCATION.SUPERA");
		//20220302 KEVIN ELIZA end:修改CPNYID.SUPER
		//20190213 DANIEL PATRICK begin:單位複選
		String DEP_NO = getQueryValue("DEP_NO");
		if(DEP_NO.length() > 0){
			DEP_NO = DEP_NO.replace("'","");
			DEP_NO = DEP_NO.replace(",","','");
		}
		//20190213 DANIEL PATRICK end:單位複選

		setTableData("table1", new String[][]{});

		//20120215 draco================================
		//後面都是用在數字相加 應該不需要||
		//if(DBTYPE.trim().equals("O")) PLUS = "||";
		//================================20120215 draco
		//檢查年度
		if (YY.length() == 0){
			//message(translate("查詢年度不可空白!"));
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("%1%2!" , new String[]{translate("年度"),translate("不可空白")});
			message(hr.Msg.hrm8b_F406.Msg001);
			//20190314 LICK COULSON end 訊息翻譯
			return false;
		}
		if ("A".equals(DATELIST) && !check.isRocDay(YY+"0101")){
			//message(translate("年度格式輸入錯誤(正確格式yy)"));
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("%1%2，%3%4",new String[]{translate("年度"),translate("格式錯誤"),translate("應為"),translate("民國年格式")});
			message(hr.Msg.hrm8b_F406.Msg002);
			//20190314 LICK COULSON end 訊息翻譯
			return false;
		}else if ("B".equals(DATELIST) && !check.isACDay(YY+"0101")){
			//message(translate("年度格式輸入錯誤(正確格式YYYY)"));
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message("%1%2，%3%4",new String[]{translate("年度"),translate("格式錯誤"),translate("應為"),translate("西元年格式")});
			message(hr.Msg.hrm8b_F406.Msg003);
			//20190314 LICK COULSON end 訊息翻譯
			return false;
		}
		YY1 = YY;
		setValue("YY" , YY);
		if ("A".equals(DATELIST)){
			YY1 = operation.floatAdd(YY1,"1911",0);			
		}
		
		//先讀取部門資料
		Hashtable htDEPT_NAME = new Hashtable();
		//20190213 DANIEL PATRICK begin:單位複選
		//sql = " select DEP_NO, DEP_NAME from HRUSER_DEPT_BAS "
			//+ " where DEP_NO<100000 "
			//+ " and CPNYID = '" + convert.ToSql(CPNYID) + "' "
			//+ " order by DEP_NO";
		//String rent[][] = t.queryFromPool(sql);
		//if (rent.length == 0){
			//message(translate("查無部門基本資料!"));
			//return false;
		//}		
		//for (int i=0; i<rent.length; i++){
			//String DEP_NO = rent[i][0].trim();
			//String DEP_NAME = rent[i][1].trim();
			//htDEPT_NAME.put(DEP_NO , DEP_NAME);
		//}
		//20190718 DANIEL PATRICK begin:多顯示是否停用在名稱後面
		//sql = " select DEP_NO, DEP_NAME from HRUSER_DEPT_BAS "
		sql = " select DEP_NO, DEP_NAME, DEP_DISABLE "
			+ " from HRUSER_DEPT_BAS "
		//20190718 DANIEL PATRICK end:多顯示是否停用在名稱後面
			+ " where DEP_NO<100000 "
			+ " and CPNYID = '" + convert.ToSql(CPNYID) + "' ";
		if(DEP_NO.length() > 0){
			sql+= " and DEP_NO in ('"+DEP_NO+"') ";
		}
		sql+= " order by DEP_NO";
		String rent[][] = t.queryFromPool(sql);
		if (rent.length == 0){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message(translate("查無部門基本資料!"));
			message(hr.Msg.hrm8b_F406.Msg004);
			//20190314 LICK COULSON end 訊息翻譯
			return false;
		}		
		for (int i=0; i<rent.length; i++){
			String iDEP_NO = rent[i][0].trim();
			String iDEP_NAME = rent[i][1].trim();
			//20190718 DANIEL PATRICK begin:多顯示是否停用在名稱後面
			String iDEP_DISABLE = rent[i][2].trim();
			if("Y".equals(iDEP_DISABLE)){
				iDEP_NAME += "("+translate("停用")+")";
			}
			//20190718 DANIEL PATRICK end:多顯示是否停用在名稱後面
			htDEPT_NAME.put(iDEP_NO , iDEP_NAME);
		}
		//20190213 DANIEL PATRICK end:單位複選

		//20150107 JENNY LULU:先確定各個年月的備份檔在不在
		String[] arrTABLE = new String[12];
		Hashtable htHRUSER = new Hashtable();//存放 EMPID@YYMM , DEPT_NO
		for(int i = 0 ; i < arrTABLE.length ; i++){
			arrTABLE[i] = "HRUSER_" + YY1 + pConvert.fill(String.valueOf(i+1) , "00");
		}
		boolean[] bTABLE = pDB.checkTable(t , arrTABLE);

		//要秀出來的TABLE
		Hashtable deptRow = new Hashtable();
		String newTab[][] = new String[rent.length+1][5];
		for (int i = 0 ; i < newTab.length ; i++){
			if ( i != newTab.length-1){
				newTab[i][0] = rent[i][0].trim(); //部門
				deptRow.put(newTab[i][0].trim(), "" + i);//放入第幾行
			}else{
				newTab[i][0] = translate("總計")+"：";
			}
			//全放0
			for (int j = 1 ; j < newTab[i].length ; j++){
				newTab[i][j] = "0";
			}
		}
		//讀取資料
		//String YYMMend = getToday("YYYYmm")+"01";
		//YYMMend = datetime.dateAdd(datetime.dateAdd(YYMMend,"m",1),"d",-1);
		String YYMMend = YY1 + "1231";
		//20150107 JENNY LULU:新增一個開始的日期
		String YYMMstart = YY1 + "0101";
		//2012/05/02:edward:begin:這段在抓公司費用時有問題,故改
		/*
			sql = " select c.DEPT_NO"//0
				+ ",isnull(sum(a.TOT_HR),0)"//1
				+ ",isnull(sum(a.COMP_PAY),0)"//2
				+ ",isnull(sum(a.USER_PAY),0)"//3
				+ ",isnull(sum(a.COMP_PAY),0)"+PLUS+"isnull(sum(a.USER_PAY),0)"//4
				+ ",1"//5
				+ " from PQ22 a, PQ23 b, HRUSER c"
				+ " where a.LESS_KEY = b.LESS_KEY"
				+ " and b.EMPID = c.EMPID and b.STUDY_CLOSE not in ('1' , '2')"
				+ " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
				+ " and a.SDATE like '" + convert.ToSql(YY1) + "%' and a.SDATE<='"+YYMMend+"'"
				+ " group by c.DEPT_NO"
				+ " \r\n union all \r\n"
				+ " select c.DEPT_NO"//0
				+ ",isnull(sum(a.TOT_HR),0)"//1
				+ ",isnull(sum(a.COMP_PAY),0)"//2
				+ ",isnull(sum(a.USER_PAY),0)"//3
				+ ",isnull(sum(a.COMP_PAY),0)"+PLUS+"isnull(sum(a.USER_PAY),0)"//4
				+ ",2"//5
				+ " from PQ32 a,HRUSER c"
				+ " where a.EMPID = c.EMPID"
				+ " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
				//+ " and a.PDATE like '" + convert.ToSql(YY1) + "%'"
				+ " and a.SDATE like '" + convert.ToSql(YY1) + "%' and a.SDATE<='"+YYMMend+"'"
				+ " group by c.DEPT_NO"
				+ " \r\n union all \r\n"
				+ " select c.DEPT_NO"
				+ ",isnull(sum(a.TOT_HR),0)"
				+ ",isnull(sum(a.COMP_PAY),0)"
				+ ",isnull(sum(a.USER_PAY),0)"
				+ ",isnull(sum(a.COMP_PAY),0)"+PLUS+"isnull(sum(a.USER_PAY),0)"
				+ ",3"
				+ " from PQ33 a,PQ33_D0 b,HRUSER c"
				+ " where a.LESS_KEY = b.LESS_KEY"
				+ " and b.EMPID = c.EMPID"
				+ " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
				//+ " and a.PDATE like '" + convert.ToSql(YY1) + "%'"
				+ " and a.SDATE like '" + convert.ToSql(YY1) + "%' and a.SDATE<='"+YYMMend+"'"
				+ " group by c.DEPT_NO"
				+ " order by 1";
			String rTab[][] = t.queryFromPool(sql);
			Hashtable htPQ32CompPay = new Hashtable();
			int iRow = 0;
			for (int i = 0 ; i < rTab.length ; i++){
				iRow = Integer.parseInt(convert.replace(""+deptRow.get(rTab[i][0].trim()),"null","-1"));
				if (iRow < 0) continue;
				newTab[iRow][1] = operation.floatAdd(newTab[iRow][1].trim(), rTab[i][1].trim(), 1);
				//20120215 draco============================================================
				//newTab[iRow][2] = operation.floatAdd(newTab[iRow][2].trim(), rTab[i][2].trim(), 0);
				if(rTab[i][5].trim().equals("2"))
				{
					htPQ32CompPay.put(rTab[i][0].trim(),rTab[i][2].trim());
					if(rTab[i][0].trim().equals("6")) System.err.println(" put "+rTab[i][2].trim());
				}
				else
				{
					newTab[iRow][2] = operation.floatAdd(newTab[iRow][2].trim(), rTab[i][2].trim(), 0);
					if(rTab[i][0].trim().equals("6")) System.err.println(" add "+rTab[i][2].trim());
				}
				if(rTab[i][0].trim().equals("6")) System.err.println(" 6 = "+newTab[iRow][2]);
				//============================================================20120215 draco
				newTab[iRow][3] = operation.floatAdd(newTab[iRow][3].trim(), rTab[i][3].trim(), 0);
				newTab[iRow][4] = operation.floatAdd(newTab[iRow][4].trim(), rTab[i][4].trim(), 0);
				newTab[newTab.length-1][1] = operation.floatAdd(newTab[newTab.length-1][1].trim(), rTab[i][1].trim(), 0);
				newTab[newTab.length-1][2] = operation.floatAdd(newTab[newTab.length-1][2].trim(), rTab[i][2].trim(), 0);
				newTab[newTab.length-1][3] = operation.floatAdd(newTab[newTab.length-1][3].trim(), rTab[i][3].trim(), 0);
				newTab[newTab.length-1][4] = operation.floatAdd(newTab[newTab.length-1][4].trim(), rTab[i][4].trim(), 0);
			}
		*/

		//20150107 JENNY LULU begin:先取出 外訓和內訓上課的名單 把值放到暫存的TEMP裡
		Vector vSQL = new Vector();
		sql = "delete from TEMP_EMPID where ID = '"+convert.ToSql(MUSER)+"' ";
		vSQL.addElement(sql);
		//外訓
		sbSQL.setLength(0);
		sbSQL.append(" select a.EMPID ");
		sbSQL.append(" from PQ32 a");
		sbSQL.append(" where a.SDATE between '" + convert.ToSql(YYMMstart) + "' and '" + convert.ToSql(YYMMend) + "'");
		sbSQL.append(" union all");
		//內訓
		sbSQL.append(" select a.EMPID ");
		sbSQL.append(" from PQ23 a");
		sbSQL.append(" where a.STUDY_CLOSE not in ('1' , '2')");
		sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMMstart) + "' and '" + convert.ToSql(YYMMend) + "' )");
		ret = t.queryFromPool( sbSQL.toString() );
		for(int i = 0 ; i < ret.length ; i++){
			String iEMPID = ret[i][0].trim();
			if(!htHRUSER.containsKey(iEMPID + "@insert")){
				sql = " insert into TEMP_EMPID ("
					+ " ID , EMPID"
					+ " ) values ("
					+ " '" + convert.ToSql(MUSER) + "' "
					+ " , '" + convert.ToSql(iEMPID)+"' "
					+ " ) ";
				vSQL.addElement(sql);
				htHRUSER.put(iEMPID + "@insert" , "Y");
			}
		}
		try{
			t.execFromPool((String[]) vSQL.toArray(new String[0]));
		}catch(Exception ex){
			
			//20190314 LICK COULSON begin 訊息翻譯
			//message(translate("資料庫發生異常，請重新執行"));
			message(hr.Msg.hrm8b_F406.Msg005);
			//20190314 LICK COULSON end 訊息翻譯
			System.err.println("資料庫發生異常，請重新執行:"+ex);
			return false;
		}
		//跑各個月份的資料，
		sbSQL.setLength(0);
		sbSQL.append(" select '' , EMPID , DEPT_NO ");
		sbSQL.append(" from HRUSER");
		sbSQL.append(" where EMPID in (select EMPID from TEMP_EMPID where ID = '" + MUSER + "')");
		for(int i = 0 ; i < arrTABLE.length ; i++){
			//取出年月
			String iYYMM = YY1 + pConvert.fill(String.valueOf(i+1) , "00");
			String iTableName = "HRUSER";//TABLE名稱
			if(bTABLE[i]){//如果有LOG檔,就取LOG檔,沒有就取現況
				iTableName += "_" + iYYMM;
			}else{
				continue;
			}
			sbSQL.append(" union all");
			sbSQL.append(" select '" + iYYMM + "' , EMPID , DEPT_NO ");
			sbSQL.append(" from " + iTableName);
			sbSQL.append(" where EMPID in (select EMPID from TEMP_EMPID where ID = '" + MUSER + "')");
		}
		ret = t.queryFromPool( sbSQL.toString() );
		for (int i=0;i<ret.length;i++){
			String iYYMM = ret[i][0].trim();//年月
			String iEMPID = ret[i][1].trim();//工號
			String iDEPT_NO = ret[i][2].trim();//單位
			//依年月+工號放資料
			htHRUSER.put(iYYMM + "@" + iEMPID , iDEPT_NO);
		}
		//20150107 JENNY LULU end:先取出 外訓和內訓上課的名單 把值放到暫存的TEMP裡

		//外訓
		sbSQL.setLength(0);
		//20150107 JENNY LULU begin:改抓出申請日期來用，另外計算在後面再計算
		//sbSQL.append(" select c.DEPT_NO , isnull(sum(a.TOT_HR),0) , isnull(sum(a.COMP_PAY),0)");
		//sbSQL.append(" , isnull(sum(a.USER_PAY),0) , isnull(sum(a.COMP_PAY),0)+isnull(sum(a.USER_PAY),0)");
		//sbSQL.append(" from PQ32 a,HRUSER c");
		//sbSQL.append(" where a.EMPID = c.EMPID");
		//sbSQL.append(" and a.CPNYID = '" + convert.ToSql(CPNYID) + "'").append(SUPERA);
		sbSQL.append(" select a.SDATE , isnull(a.TOT_HR,0) , isnull(a.COMP_PAY,0)");//2
		sbSQL.append(" , isnull(a.USER_PAY,0) , isnull(a.COMP_PAY,0)+isnull(a.USER_PAY,0)");//4
		sbSQL.append(" , a.EMPID ");//5
		sbSQL.append(" from PQ32 a");
		//20150107 JENNY LULU:因為公司別可能會改，所以不能加，反正後面會有擋了
		//sbSQL.append(" where a.CPNYID = '" + convert.ToSql(CPNYID) + "'").append(SUPERA);
		sbSQL.append(" where 1 = 1 ").append(SUPERA);
		//20150107 JENNY LULU:開始日改成用變數
		//sbSQL.append(" and a.SDATE between '" + convert.ToSql(YY1) + "0101' and '" + convert.ToSql(YYMMend) + "'");
		sbSQL.append(" and a.SDATE between '" + convert.ToSql(YYMMstart) + "' and '" + convert.ToSql(YYMMend) + "'");
		//sbSQL.append(" group by c.DEPT_NO");
		//20150107 JENNY LULU end:改抓出申請日期來用，另外計算在後面再計算
		String rTab[][] = t.queryFromPool( sbSQL.toString() );
		int iRow = 0;
		for (int i = 0 ; i < rTab.length ; i++){
			//20150107 JENNY LULU being:修改抓取的欄位
			//							第0欄改成抓申請日期，多抓工號，再依申請日的年月加工號來抓單位
			//String iDEPT_NO = rTab[i][0].trim();//單位
			String iSDATE = rTab[i][0].trim();//20150107 申請日
			String iTOT_HR = rTab[i][1].trim();
			String iCOMP_PAY = rTab[i][2].trim();
			String iUSER_PAY = rTab[i][3].trim();
			String iPAY = rTab[i][4].trim();
			String iEMPID = rTab[i][5].trim();//20150107 工號
			String iYYMM = "";
			if(pCheck.checkValue(iSDATE , "DATE:YYYYmmdd")){//資料庫來的，應該要是西元年
				int mm = 0;
				try{
					mm = Integer.parseInt(iSDATE.substring(4,6)) - 1;
					if(bTABLE[mm]){//依月份去抓bTABLE的陣列，確定是否有LOG TABLE
						iYYMM = iSDATE.substring(0,6);//如果有備份檔,則抓備份檔
					}else{
						iYYMM = "";//沒有的話抓現況檔
					}
				}catch(Exception ex){
					iYYMM = "";
				}
			}

			String iDEPT_NO = (String) htHRUSER.get(iYYMM + "@" + iEMPID);
			if(iDEPT_NO == null){
				iDEPT_NO = "";
			}
			//20150107 JENNY LULU end:修改抓取的欄位
			//取出該單位在第幾行
			iRow = Integer.parseInt(convert.replace(""+deptRow.get(iDEPT_NO),"null","-1"));
			if (iRow < 0){//抓不出來到的部門就continue掉
				continue;
			}
			//加到各行的數字
			newTab[iRow][1] = operation.floatAdd(newTab[iRow][1].trim(), iTOT_HR, 10);
			newTab[iRow][2] = operation.floatAdd(newTab[iRow][2].trim(), iCOMP_PAY, 10);
			newTab[iRow][3] = operation.floatAdd(newTab[iRow][3].trim(), iUSER_PAY, 10);
			newTab[iRow][4] = operation.floatAdd(newTab[iRow][4].trim(), iPAY, 10);
			//加到加的數字
			newTab[newTab.length-1][1] = operation.floatAdd(newTab[newTab.length-1][1].trim(), iTOT_HR, 10);
			newTab[newTab.length-1][2] = operation.floatAdd(newTab[newTab.length-1][2].trim(), iCOMP_PAY, 10);
			newTab[newTab.length-1][3] = operation.floatAdd(newTab[newTab.length-1][3].trim(), iUSER_PAY, 10);
			newTab[newTab.length-1][4] = operation.floatAdd(newTab[newTab.length-1][4].trim(), iPAY, 10);
		}
		
		String[][] data = newTab;
		//2011/10/21:edward:begin:Eline:公司負擔金額：捉取PQ22.COM_PAY但要再除以報名人數(不含請假和報名取消人員)四捨五入得到整數位再看這個單位有哪些人報名這個課程PQ23 把費用分攤到個人,再用單位做費用加總

		Hashtable hData = new Hashtable();
		//內訓
		sbSQL.setLength(0);
		sbSQL.append(" select count(*) , LESS_KEY from PQ23");
		sbSQL.append(" where CPNYID = '"+convert.ToSql(CPNYID)+"'");
		sbSQL.append(" and STUDY_CLOSE not in ('1' , '2')");
		//20150107 JENNY LULU:開始日改成用變數
		//sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YY1) + "0101' and '" + convert.ToSql(YYMMend) + "' )");
		sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YYMMstart) + "' and '" + convert.ToSql(YYMMend) + "' )");
		sbSQL.append(" group by LESS_KEY");
		ret = t.queryFromPool( sbSQL.toString() );
		for (int i=0;i<ret.length;i++){
			String key = ret[i][1].trim() + "@LESS_KEY";
			hData.put(key , ret[i][0].trim());
		}
		//先取得內訓的上課時數
		sbSQL.setLength(0);
		sbSQL.append(" select isnull(sum(a.COMP_PAY),0) , a.LESS_KEY ");//因為是BY課程，所以直接SUM
		sbSQL.append(" , isnull(sum(a.USER_PAY),0) , isnull(sum(a.TOT_HR),0)");
		sbSQL.append(" from PQ22 a");
		sbSQL.append(" where a.CPNYID = '"+ convert.ToSql(CPNYID) +"'").append(SUPERA);
		//20150107 JENNY LULU:開始日改成用變數
		//sbSQL.append(" and a.SDATE between '" + convert.ToSql(YY1) + "01' and '" + convert.ToSql(YYMMend) + "'");
		sbSQL.append(" and a.SDATE between '" + convert.ToSql(YYMMstart) + "' and '" + convert.ToSql(YYMMend) + "'");
		sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22_CLOSE x)");
		sbSQL.append(" group by a.LESS_KEY");
		//ret = t.queryFromPool("select COMP_PAY , LESS_KEY , USER_PAY  from PQ22 where CPNYID = '"+convert.ToSql(CPNYID)+"' and SDATE like '" + convert.ToSql(YYMM1) + "%'");
		ret = t.queryFromPool( sbSQL.toString() );
		for (int i=0;i<ret.length;i++){
			String iLESS_KEY = ret[i][1].trim();
			String key1 = iLESS_KEY + "@LESS_KEY";
			String COMP_PAY = ret[i][0].trim();
			if ( hData.containsKey(key1) ){
				String pCount = (String)hData.get(key1);
				try{
					COMP_PAY = operation.floatDivide(COMP_PAY , pCount , 10);
				}catch (Exception e){}
				if ( COMP_PAY == null ){
					COMP_PAY = "0";
				}
				String key2 = iLESS_KEY + "@COMP_PAY";
				hData.put(key2 , COMP_PAY);
				
				String key3 = iLESS_KEY + "@USER_PAY";
				hData.put(key3 , ret[i][2].trim());
				
				String key4 = iLESS_KEY + "@TOT_HR";
				hData.put(key4 , ret[i][3].trim());
			}
		}
		//20170107 JENNY LULU :要依報名日期去取LOG的單位資料，所以重寫
		//Hashtable htHRUSER = new Hashtable();
		//sbSQL.setLength(0);
		//sbSQL.append(" select EMPID , DEPT_NO from HRUSER");
		//sbSQL.append(" where CPNYID = '"+ convert.ToSql(CPNYID) +"'");
		//ret = t.queryFromPool( sbSQL.toString() );
		//for (int i=0;i<ret.length;i++){
		//	String EMPID = ret[i][0].trim();
		//	String DEPT_NO = ret[i][1].trim();
		//	htHRUSER.put(EMPID , DEPT_NO);
		//}

		Hashtable htLESS_KEY = new Hashtable();
		Hashtable DEP_COUNT = new Hashtable();
		sbSQL.setLength(0);
		//20150107 JENNY LULU begin:多抓出 PDATE , 要來取得報名當時的部門用
		//sbSQL.append(" select EMPID , LESS_KEY");
		//sbSQL.append(" select EMPID , LESS_KEY , PDATE");
		//sbSQL.append(" from PQ23");
		//sbSQL.append(" where CPNYID = '"+convert.ToSql(CPNYID)+"'");
		//sbSQL.append(" and STUDY_CLOSE not in ('1' , '2')");
		//sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YY1) + "01' and '" + convert.ToSql(YYMMend) + "')");
		sbSQL.append(" select a.EMPID , a.LESS_KEY , b.SDATE");
		sbSQL.append(" from PQ23 a , PQ22 b");
		sbSQL.append(" where a.LESS_KEY = b.LESS_KEY");
		sbSQL.append(" and a.STUDY_CLOSE not in ('1' , '2')");
		sbSQL.append(" and b.SDATE between '" + convert.ToSql(YYMMstart) + "' and '" + convert.ToSql(YYMMend) + "'");
		//20150107 JENNY LULU end:多抓出 SDATE , 要來取得報名當時的部門用
		ret = t.queryFromPool( sbSQL.toString() );
		int counterr = 0;
		for (int i=0;i<ret.length;i++){			
			//先記錄每個部門參與此課程的人數
			String EMPID = ret[i][0].trim();
			String LESS_KEY = ret[i][1].trim();
			//20150107 JENNY LULU:多抓報名日
			String SDATE = ret[i][2].trim();
			//20150107 JENNY LULU:KEY值多放報名的年月
			String iYYMM = "";
			if(pCheck.checkValue(SDATE , "DATE:YYYYmmdd")){//資料庫來的，應該要是西元年
				int mm = 0;
				try{
					mm = Integer.parseInt(SDATE.substring(4,6)) - 1;
					if(bTABLE[mm]){//依月份去抓bTABLE的陣列，確定是否有LOG TABLE
						iYYMM = SDATE.substring(0,6);//如果有備份檔,則抓備份檔
					}else{
						iYYMM = "";//沒有的話抓現況檔
					}
				}catch(Exception ex){
					iYYMM = "";
				}
			}
			//String iKEY = EMPID;
			String iKEY = iYYMM + "@" + EMPID;

			String DEPT_NO = (String)htHRUSER.get( iKEY );
			
			if ( DEPT_NO != null ){
				if ( htLESS_KEY.containsKey(LESS_KEY) ){
					DEP_COUNT = (Hashtable)htLESS_KEY.get( LESS_KEY );
					if ( DEP_COUNT.containsKey(DEPT_NO) ){
						String tmp = (String)DEP_COUNT.get(DEPT_NO);
						DEP_COUNT.put(DEPT_NO , operation.add(tmp , "1"));
					}else{
						DEP_COUNT.put(DEPT_NO , "1");
					}
					htLESS_KEY.put(LESS_KEY , DEP_COUNT);
				}else{
					DEP_COUNT = new Hashtable();
					DEP_COUNT.put(DEPT_NO , "1");
					htLESS_KEY.put(LESS_KEY , DEP_COUNT);
				}
			}
		}

		Hashtable htChk = new Hashtable();
		for (int i=0;i<ret.length;i++){
			String LESS_KEY = ret[i][1].trim();

			if ( htChk.containsKey(LESS_KEY) ){
				continue;
			}else{
				htChk.put(LESS_KEY , "");
			}
			
			String key1 = LESS_KEY + "@COMP_PAY";
			String key3 = LESS_KEY + "@USER_PAY";
			String key4 = LESS_KEY + "@TOT_HR";
			
			DEP_COUNT = (Hashtable)htLESS_KEY.get(LESS_KEY);
			if ( DEP_COUNT == null ){
				continue;
			}

			Vector vKey = new Vector(DEP_COUNT.keySet());
			for (int j=0; j<vKey.size(); j++){
				String jDEPT_NO = (String)vKey.elementAt(j);
				String DEP_COUNTR = (String)DEP_COUNT.get(jDEPT_NO);
				if ( DEP_COUNTR == null ){//應該是不會
					continue;
				}

				if ( hData.containsKey(key1) ){
					String jKey1 = jDEPT_NO + "@DEP_PAY";
					String COMP_PAY = (String)hData.get(key1);
					String DEP_PAY = operation.floatMultiply(COMP_PAY , DEP_COUNTR , 10);
					if ( hData.containsKey(jKey1) ){
						String DEP_PAY_ALL = (String)hData.get(jKey1);
						DEP_PAY_ALL = operation.floatAdd(DEP_PAY_ALL , DEP_PAY , 10);
						hData.put(jKey1 , DEP_PAY_ALL);
					}else{
						hData.put(jKey1 , DEP_PAY);
					}
				}
				
				if ( hData.containsKey(key3) ){
					String jKey2 = jDEPT_NO + "@USER_PAY";
					String USER_PAY = (String)hData.get(key3);
					String DEP_USER_PAY = operation.floatMultiply(USER_PAY , DEP_COUNTR , 10);
					if ( hData.containsKey(jKey2) ){
						String USER_PAY_ALL = (String)hData.get(jKey2);
						USER_PAY_ALL = operation.floatAdd(USER_PAY_ALL , DEP_USER_PAY , 10);
						hData.put(jKey2 , USER_PAY_ALL);
					}else{
						hData.put(jKey2 , DEP_USER_PAY);
					}
				}
				
				if ( hData.containsKey(key4) ){
					String jKey3 = jDEPT_NO + "@TOT_HR";
					String TOT_HR = (String)hData.get(key4);
					String DEP_TOT_HR = operation.floatMultiply(TOT_HR , DEP_COUNTR , 10);
					if ( hData.containsKey(jKey3) ){
						String TOT_HR_ALL = (String)hData.get(jKey3);
						TOT_HR_ALL = operation.floatAdd(TOT_HR_ALL , DEP_TOT_HR , 10);
						hData.put(jKey3 , TOT_HR_ALL);
					}else{
						hData.put(jKey3 , DEP_TOT_HR);
					}
				}
			}
		}
		String TOT1 = "0";//TOT_HR
		String TOT2 = "0";//COMP_PAY
		String TOT3 = "0";//USER_PAY
		String TOT4 = "0";//TOT_PAY
		for (int i=0;i<data.length;i++)
		{
				String key1 = data[i][0].trim() + "@DEP_PAY";
				String key2 = data[i][0].trim() + "@USER_PAY";
				String key3 = data[i][0].trim() + "@TOT_HR";

				if ( hData.containsKey(key1) ){
					String tmp = (String)hData.get(key1);
					if ( operation.compareTo(tmp , "0") != 0 ){
						data[i][2] = operation.floatAdd(data[i][2] , tmp , 10);
						
					}
				}

				if ( hData.containsKey(key2) ){
					String tmp = (String)hData.get(key2);
					if ( operation.compareTo(tmp , "0") != 0 ){
						data[i][3] = operation.floatAdd(data[i][3] , tmp , 10);
					}
				}
				
				if ( hData.containsKey(key3) ){
					String tmp = (String)hData.get(key3);
					if ( operation.compareTo(tmp , "0") != 0 ){
						data[i][1] = operation.floatAdd(data[i][1] , tmp , 10);
					}
				}

			data[i][4] = operation.add(data[i][2] , data[i][3] , 10);
			if (i == (newTab.length-1)){
				data[i][1] = operation.add(TOT1 , "0" , 10);
				data[i][2] = operation.add(TOT2 , "0" , 10);
				data[i][3] = operation.add(TOT3 , "0" , 10);
				data[i][4] = operation.add(TOT4 , "0" , 10);
			}else{
				TOT1 = operation.add(TOT1 , data[i][1] , 10);
				TOT2 = operation.add(TOT2 , data[i][2] , 10);
				TOT3 = operation.add(TOT3 , data[i][3] , 10);
				TOT4 = operation.add(TOT4 , data[i][4] , 10);

			}
		}

		for (int i=0; i<data.length; i++){
			String DEPT_NO = data[i][0].trim();
			String DEPT_NAME = (String)htDEPT_NAME.get(DEPT_NO);
			if ( DEPT_NAME != null ){
				data[i][0] = DEPT_NAME;
			}
			
			data[i][1] = operation.floatAdd(data[i][1] , "0" , 1);
			data[i][2] = operation.floatAdd(data[i][2] , "0" , 2);
			data[i][3] = operation.floatAdd(data[i][3] , "0" , 2);
			data[i][4] = operation.floatAdd(data[i][4] , "0" , 2);
		}

		//2011/10/21:edward:end
		setTableData("table1", data);
		setValue("CPNYID", CPNYID);
		return false;
	}

	public String getInformation(){
		return "---------------\u67e5\u8a62\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
	}
}

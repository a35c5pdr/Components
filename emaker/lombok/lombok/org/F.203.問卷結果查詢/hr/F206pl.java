/*
	version		modify_date		modify_user		description
	1.00				20220302			ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
package hr;
import jcx.jform.hproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

public class F206pl extends hproc{
	public String action(String value)throws Throwable{
		// 可自定HTML版本各欄位的預設值與按鈕的動作 
		// 傳入值 value 
		talk t = getTalk();
		String sql = "";
		String[][] ret = null;
		String CPNYID = getValue("CPNYID").trim();
		String QID = getValue("QID").trim();
		String EMPID = getValue("EMPID").trim();
		String where = "";
		if(!CPNYID.equals("")){
			where += " and a.CPNYID = '" + convert.ToSql(CPNYID) + "'";
		}
		if(!QID.equals("")){
			where += " and a.QID = '" + convert.ToSql(QID) + "'";
		}
		if(!EMPID.equals(""))	{
			where += " and b.EMPID = '" + convert.ToSql(EMPID) + "'";
		}
		//20121220 Tim 增加查詢權限 begin:
		
		//20220302 ANDYHOU ELIZE begin:權限改使用EDUCATION.SUPER與CONDITION_B06
		//EMPID += get("CPNYID.SUPERB") + " and b.EMPID in (select EMPID from HRUSER where 1=1 "+get("CONDITION_B06")+")";
		EMPID += get("EDUCATION..SUPERB") + " and b.EMPID in (select EMPID from HRUSER where 1=1 "+get("CONDITION_B06")+")";
		//20220302 ANDYHOU ELIZE end:權限改使用EDUCATION.SUPER與CONDITION_B06
		//20121220 Tim 增加查詢權限 end
		/*
		sql=" select a.CPNYID , a.QID , a.QNAME , b.EMPID"
						+ " from QSURVEY a,QSURVEYREPORT b,QSURVEY_ITEM c"
						+ " where a.CPNYID = b.CPNYID and a.QID = b.QID and a.NUM  = b.NUM "
						+ " and b.CPNYID = c.CPNYID and b.QID = c.QID and b.NUM = c.NUM"
						+ " and b.AnswerItemId = c.AnswerItemId"
						+ WhereStr1
						+ " group by a.CPNYID,a.QID,a.QNAME,b.AnswerEmpid ";
		*/
		sql =" select distinct a.CPNYID , a.QID , a.QNAME , b.EMPID"
			+ " from QSURVEY a,QSURVER_REPORT b"
			+ " where a.CPNYID = b.CPNYID and a.QID = b.QID and a.NUM  = b.NUM "
			+ where ;
		ret = t.queryFromPool(sql);
		setTableData("table1" , ret);
		if (ret.length == 0) {
			message("查詢無資料");
			setTableData("table2", new String[0][0]);
		}

		 return value;
	}
	public String getInformation(){
		return "---------------button1(\u67e5\u8a62).html_action()----------------";
	}
}

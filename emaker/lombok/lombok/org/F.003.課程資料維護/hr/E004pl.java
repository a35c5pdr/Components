/*
	version		modify_date		modify_user		description
	1.00		20220301		ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
package hr;
import jcx.jform.hproc;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
import jcx.lib.*;
/*
	dat: hrm7b.dat
	function name: E.004. 課程資料維護
	note: 
		RINS ELINE ADD 20160830
*/
public class E004pl extends hproc{
	String MDATE = "";
	String MTIME = "";
	String MUSER = "";
	String sql = "";
	String [][] ret = null;
	talk t = null;
	String DBN = "";
	String name = "";
	String ISNULL = "";
	public String action(String value)throws Throwable{
		MDATE = getToday("YYYYmmdd");
		MTIME = getTime("hms");
		MUSER = getUser();
		t = getTalk();
		name = getName().trim();
		ISNULL = pDB.getISNULL(t);
		DBN = pDB.getDBN(t);
		if("SETEDIT".equals(name)){
			SETEDIT();
		}
		return value;
	}


	//  已開課之課程，應不可維護課程資料，避免造成後續課程對不到的狀況
	//		說明：課程資料維護{{PQ03}}時，需檢核{{PQ15}}、{{PQ16}}、
	//		{{PQ22}}、{{PQ23}}、{{PQ32}}是否已使用該課程代號，若有則不允
	//		許修改【公司別】、【課程大類別】、【課程小類別】、【課程代號】共四個欄位
	//	20170308 ROGER ELINE : 修改成欄位都可以EDIT E004pt中會Update
	public void SETEDIT() throws Exception{
		String ONJOBTRAN = getValue("ONJOBTRAN").trim();
		String LESSNAME = getValue("LESSNAME").trim();
		String LESS_CODE3 = getValue("LESS_CODE3").trim();
		String LESS_NO1 = getValue("LESS_NO1").trim();
		
		
		sql = " select distinct LESS_NO "
			+ " from PQ03 "
			+ " where 1 = 1 "
		//20220301 ANDYHOU begin:權限改使用EDUCATION.SUPER與CONDITION_B06
			//+ " and (( 1 = 1 " + (String)get("CPNYID.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) ";
			+ " and (( 1 = 1 " + (String)get("EDUCATION.SUPER") + ") or ( isnull(CPNYID,'" + ISNULL + "') = '" + ISNULL + "')) ";
		//20220301 ANDYHOU end:權限改使用EDUCATION.SUPER與CONDITION_B06
		if(LESS_CODE3.length() > 0){
			sql += " and LESS_CODE3 like " + DBN + "'%" + convert.ToSql(LESS_CODE3) + "%'";
		}
		if(LESSNAME.length() > 0){
			sql += " and LESSNAME like " + DBN + "'%" + convert.ToSql(LESSNAME) + "%'";
		}
		if(ONJOBTRAN.length() > 0){
			sql += " and ONJOBTRAN = '" + convert.ToSql(ONJOBTRAN) + "'";
		}
		if(LESS_NO1.length() > 0){
			sql += " and LESS_NO1 = '" + convert.ToSql(LESS_NO1) + "'";
		}
		String[] arrTABLE = {"PQ15","PQ16","PQ22","PQ23","PQ32"};
		StringBuffer sbSQL = new StringBuffer();
		for( int i = 0 ; i < arrTABLE.length ; i++) {
			String iTABLENAME = arrTABLE[i].trim();
			sbSQL.append(" select LESS_NO");	
			sbSQL.append(" from ").append(iTABLENAME);	
			sbSQL.append(" where 1 = 1");
			sbSQL.append(" and LESS_NO in (").append(sql).append(")");
			if(i != arrTABLE.length - 1){
				sbSQL.append(" union ");
			}
		}
		ret = t.queryFromPool(sbSQL.toString());
		if(ret.length == 0){
		}
		Hashtable htExist = new Hashtable();
		for(int i = 0 ; i < ret.length ; i++){
			String iLESS_NO = ret[i][0].trim();
			htExist.put(iLESS_NO,"Y");
		}
		ret = getTableData("table1");
		addScript("EMC['table1']._in_commiting = true;");
		for(int i = 0 ; i < ret.length ; i++){
			String iLESS_NO = ret[i][4].trim();
			//20170308 ROGER ELINE : 修改成欄位都可以EDIT E004pt中會Update
			//boolean bEdit = !htExist.containsKey(iLESS_NO);
			boolean bEdit = true;
			setEditable("table1" , i, 0 , bEdit);  //	公司別
			setEditable("table1" , i, 1 , bEdit);  //	課程大類別
			setEditable("table1" , i, 2 , bEdit);  //	課程小類別
			setEditable("table1" , i, 3 , bEdit);  //	課程代號
		}
		addScript("EMC['table1']._in_commiting = false;");
	}

	public String getInformation(){
		return "---------------FORM_STATUS(FORM_STATUS).html_action()----------------";
	}
}

/*
	version modify_date modify_user description
	1.00	20220302	KEVIN		修改CPNYID.SUPER
*/
package hr;
import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
/*
		程式設計師：PEGGY
		程式功能：E.6.1.年度訓練時數與成本分析報表(E5001)_查詢按鈕程式
		區域變數：
		函式：
		用到的外部程式：
		修改記錄：
						2005031401PEGGY：初次上線
*/
public class E401pt extends bTransaction{
	public boolean action(String value)throws Throwable{
		// 回傳值為 true 表示執行接下來的資料庫異動或查詢
		// 回傳值為 false 表示接下來不執行任何指令
		// 傳入值 value 為 "新增","查詢","修改","刪除","列印","PRINT" (列印預覽的列印按鈕),"PRINTALL" (列印預覽的全部列印按鈕) 其中之一
		message("");
		//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
		//String SUPER = (String)get("CPNYID.SUPER");
		//String SUPERA = (String)get("CPNYID.SUPERA");
		String SUPER = (String)get("EDUCATION.SUPER");
		String SUPERA = (String)get("EDUCATION.SUPERA");
		//20220302 KEVIN ELIZA end:修改CPNYID.SUPER
		//查詢
		if ( value.equals("查詢")) { 
			talk t = getTalk();
			String[][] ret = null;
			StringBuffer sbSQL = new StringBuffer();
			String DATELIST = (String)get("SYSTEM.DATELIST");
			String YY = getQueryValue("PDATE").trim() ;   //年度
			String YY1 = "" ; 
			String sql = "";
			String CPNYID = (String)getQueryValue("CPNYID");
			String ISNULL = "";
			String db_type = t.f_db_type;
			if (db_type.trim().toUpperCase().equals("ORACLE")){
				ISNULL = "NULL";
			}

			//檢查年度
			if (YY.length()==0) { 
				
				//20190312 LICK COULSON begin 訊息翻譯
				//message(translate("查詢年度不可空白")) ; 
				message(hr.Msg.hrm8b_F402.Msg001);
				//20190312 LICK COULSON end 訊息翻譯
				return false ; 
			} else{
				if (DATELIST.equals("A") && !check.isRocDay(YY + "0101") ) { 
					
					//20190312 LICK COULSON begin 訊息翻譯
					//message(translate("查詢年度輸入錯誤，請輸入yy")) ; 
					message(hr.Msg.hrm8b_F402.Msg002);
					//20190312 LICK COULSON end 訊息翻譯
					return false ; 
				} else if (DATELIST.equals("B") && !check.isACDay(YY + "0101") ) {  
					
					//20190312 LICK COULSON begin 訊息翻譯
					//message(translate("查詢年度輸入錯誤，請輸入YYYY")) ; 
					message(hr.Msg.hrm8b_F402.Msg003);
					//20190312 LICK COULSON end 訊息翻譯
					return false ; 
				} 				
			}
			if ( check.isRocDay(YY+"0101")) { 
				YY1 = operation.floatAdd(YY,"1911",0) ; 
			} else { 
				YY1 = YY ; 
			}   
			
			Hashtable getYM = new Hashtable();
			String rTab[][] = new String[13][11];
			for (int i = 0 ; i < rTab.length ; i ++){
				if ( i != rTab.length -1){
					rTab[i][0] = YY + convert.add0(""+(i+1),"2");
					getYM.put(rTab[i][0].trim(), "" + i); //記錄各月份列數
				}else{
					rTab[i][0] = translate("總計")+"：";
				}
				for (int j = 1 ; j < rTab[i].length ; j++){
					rTab[i][j] = "0";
				}
			}
					
			//讀取資料 
			//sql = " select case '" + DATELIST + "' when 'A'  then cast(substring(a.PDATE,1,6) as int)-191100 else cast(substring(a.PDATE,1,6) as int) end"
			
			//2012/04/25:edward:begin:這段,在計算公司費用時,PQ22的部份有誤,整段重改
			/*
				sql = " select a.PDATE"
					 + ",a.EMPID"
					 + ",b.WORKTYPE"
					 + ",c.CHIEF"
					 + ",a.TOT_HR"
					 + ",a.COMP_PAY"
					 + " from PQ32 a, HRUSER b,POSITION c"
					 + " where a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
					 + " and a.PDATE like '" + convert.ToSql(YY1) + "%'"
					 + " and a.EMPID = b.EMPID"
					 + " and b.POSSIE = c.POSSIE"
					 + " \r union all \r"
					 //+ " select case '" + DATELIST + "' when 'A'  then cast(substring(b.SDATE,1,6) as int)-191100 else cast(substring(b.SDATE,1,6) as int) end"
					 + " select b.SDATE"
					 + ",a.EMPID"
					 + ",c.WORKTYPE"
					 + ",d.CHIEF"
					 + ",a.TRAIN_HR" //4
					 + ",b.COMP_PAY"
					 + " from PQ23 a, PQ22 b,HRUSER c,POSITION d"
					 + " where a.CPNYID = '" + convert.ToSql(CPNYID) + "'"
					 + " and b.SDATE like '" + convert.ToSql(YY1) + "%'"
					 + " and a.LESS_KEY = b.LESS_KEY"
					 + " and a.EMPID = c.EMPID"
					 + " and c.POSSIE = d.POSSIE";
					 String rent[][] = t.queryFromPool(sql);

				String YYMM = "";   //年月
				String EMPID = "";  //員工編號
				String WORKTYPE = ""; //直接/間接員工
				String CHIEF = "";  //基層/中高階主管
				String TOT_HR = ""; //上課時數
				String COMP_PAY = ""; //公司負擔
				int iRow = 0;          //列數
				int lastRow = rTab.length - 1;
				for (int i = 0 ; i < rent.length ; i++){
					YYMM = rent[i][0].trim();  //年月
					EMPID = rent[i][1].trim(); //員工編號
					WORKTYPE = rent[i][2].trim(); //直接/間接員工
					CHIEF = rent[i][3].trim();          //基層/中高階主管
					TOT_HR = rent[i][4].trim();      //上課時數
					COMP_PAY = rent[i][5].trim(); //公司負擔
					//20120418 tobey kalis begin: 修改 判斷若TRAIN_HR若為null or 空，就塞0
					if (TOT_HR == null || TOT_HR.length() == 0) {
						TOT_HR = "0";
					}
					//20120418 tobey kalis end: 修改 判斷若TRAIN_HR若為null or 空，就塞0
					if(YYMM.length() >= 6){
						YYMM = YYMM.substring(0,6);
					}
					if("A".equals(DATELIST)){
						YYMM = operation.sub(YYMM,"191100");
					}
					rent[i][0] = YYMM;
					//20120419 tobey kalis begin: 修改 不要用check.isNum() 因為有可能是小數
					//if(TOT_HR == null || !check.isNum(TOT_HR)){
					//	TOT_HR = "0";
					//}
					//20120419 tobey kalis end: 修改 不要用check.isNum() 因為有可能是小數
					iRow = Integer.parseInt(convert.replace(""+getYM.get(YYMM),"null","-1"));
					
					if (iRow < 0) continue;
					//直接員工
					if (WORKTYPE.equals("B")){
						rTab[iRow][1] = operation.floatAdd(rTab[iRow][1].trim(), TOT_HR, 1);      //時數
						rTab[iRow][2] = operation.floatAdd(rTab[iRow][2].trim(), COMP_PAY, 0); //公司負擔
						rTab[lastRow][1] = operation.floatAdd(rTab[lastRow][1].trim(), TOT_HR, 1);      //時數
						rTab[lastRow][2] = operation.floatAdd(rTab[lastRow][2].trim(), COMP_PAY, 0); //公司負擔
					//間接員工
					}else if (WORKTYPE.equals("A")){
						rTab[iRow][3] = operation.floatAdd(rTab[iRow][3].trim(), TOT_HR, 1);      //時數
						rTab[iRow][4] = operation.floatAdd(rTab[iRow][4].trim(), COMP_PAY, 0); //公司負擔
						rTab[lastRow][3] = operation.floatAdd(rTab[lastRow][3].trim(), TOT_HR, 1);      //時數
						rTab[lastRow][4] = operation.floatAdd(rTab[lastRow][4].trim(), COMP_PAY, 0); //公司負擔
					}
					
					//基層員工
					if (CHIEF.equals("C")){
						rTab[iRow][5] = operation.floatAdd(rTab[iRow][5].trim(), TOT_HR, 1);      //時數
						rTab[iRow][6] = operation.floatAdd(rTab[iRow][6].trim(), COMP_PAY, 0); //公司負擔
						rTab[lastRow][5] = operation.floatAdd(rTab[lastRow][5].trim(), TOT_HR, 1);      //時數
						rTab[lastRow][6] = operation.floatAdd(rTab[lastRow][6].trim(), COMP_PAY, 0); //公司負擔
					}else if ((CHIEF.equals("A") || CHIEF.equals("B"))){
						rTab[iRow][7] = operation.floatAdd(rTab[iRow][7].trim(), TOT_HR, 1);      //時數
						rTab[iRow][8] = operation.floatAdd(rTab[iRow][8].trim(), COMP_PAY, 0); //公司負擔
						rTab[lastRow][7] = operation.floatAdd(rTab[lastRow][7].trim(), TOT_HR, 1);      //時數
						rTab[lastRow][8] = operation.floatAdd(rTab[lastRow][8].trim(), COMP_PAY, 0); //公司負擔
					}
					rTab[iRow][9] = operation.floatAdd(rTab[iRow][9].trim(), TOT_HR, 1);          //時數
					rTab[iRow][10] = operation.floatAdd(rTab[iRow][10].trim(), COMP_PAY, 0); //公司負擔
					rTab[lastRow][9] = operation.floatAdd(rTab[lastRow][9].trim(), TOT_HR, 1);          //時數
					rTab[lastRow][10] = operation.floatAdd(rTab[lastRow][10].trim(), COMP_PAY, 0); //公司負擔
				}
			*/
			//PQ32 各人的時數及費用是屬各人,所以要加總
			sbSQL.setLength(0);
			sbSQL.append("select distinct a.SDATE , a.EMPID , b.WORKTYPE , isnull(c.CHIEF , '"+ ISNULL +"') , sum(a.TOT_HR) , sum(a.COMP_PAY) ");
			sbSQL.append(" from PQ32 a , HRUSER b left outer join POSITION c on b.POSSIE = c.POSSIE");
			sbSQL.append(" where a.SDATE between '" + convert.ToSql(YY1) + "0101' and '"+ convert.ToSql(YY1) +"1231'");
			sbSQL.append(" and a.EMPID = b.EMPID ").append(SUPERA);
			if(CPNYID.length() != 0){
				sbSQL.append(" and a.CPNYID = '"+ convert.ToSql(CPNYID) +"'");
			}
			sbSQL.append(" group by a.SDATE , a.EMPID , b.WORKTYPE , c.CHIEF");
			ret = t.queryFromPool( sbSQL.toString() );
			String YYMM = "";   //年月
			String EMPID = "";  //員工編號
			String WORKTYPE = ""; //直接/間接員工
			String CHIEF = "";  //基層/中高階主管
			String TOT_HR = ""; //上課時數
			String COMP_PAY = ""; //公司負擔
			int iRow = 0;          //列數
			int lastRow = rTab.length - 1;
			
			for (int i = 0 ; i < ret.length ; i++){
				YYMM = ret[i][0].trim();  //年月
				EMPID = ret[i][1].trim(); //員工編號
				WORKTYPE = ret[i][2].trim(); //直接/間接員工
				CHIEF = ret[i][3].trim();          //基層/中高階主管
				TOT_HR = ret[i][4].trim();      //上課時數
				COMP_PAY = ret[i][5].trim(); //公司負擔

				if (TOT_HR == null || TOT_HR.length() == 0) {
					TOT_HR = "0";
				}

				if(YYMM.length() >= 6){
					YYMM = YYMM.substring(0,6);
				}
				
				if("A".equals(DATELIST)){
					YYMM = operation.sub(YYMM,"191100");
				}
				
				ret[i][0] = YYMM;

				iRow = Integer.parseInt(convert.replace(""+getYM.get(YYMM),"null","-1"));
				
				if (iRow < 0) continue;
				//直接員工
				if (WORKTYPE.equals("B")){
					rTab[iRow][1] = operation.floatAdd(rTab[iRow][1].trim(), TOT_HR, 10);      //時數
					rTab[iRow][2] = operation.floatAdd(rTab[iRow][2].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][1] = operation.floatAdd(rTab[lastRow][1].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][2] = operation.floatAdd(rTab[lastRow][2].trim(), COMP_PAY, 10); //公司負擔
				//間接員工
				}else if (WORKTYPE.equals("A")){
					rTab[iRow][3] = operation.floatAdd(rTab[iRow][3].trim(), TOT_HR, 10);      //時數
					rTab[iRow][4] = operation.floatAdd(rTab[iRow][4].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][3] = operation.floatAdd(rTab[lastRow][3].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][4] = operation.floatAdd(rTab[lastRow][4].trim(), COMP_PAY, 10); //公司負擔
				}
				
				//20141121 JENNY ELINE begin:改寫法，如果不是A 主管職，就是非主管職
				//基層員工
				//if (CHIEF.equals("C")){
				//主管職 (A)
				if (CHIEF.length() > 0 && "A".equals(CHIEF)){
					rTab[iRow][5] = operation.floatAdd(rTab[iRow][5].trim(), TOT_HR, 10);      //時數
					rTab[iRow][6] = operation.floatAdd(rTab[iRow][6].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][5] = operation.floatAdd(rTab[lastRow][5].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][6] = operation.floatAdd(rTab[lastRow][6].trim(), COMP_PAY, 10); //公司負擔
				//}else if ((CHIEF.equals("A") || CHIEF.equals("B"))){
				}else{//非主管職 
					rTab[iRow][7] = operation.floatAdd(rTab[iRow][7].trim(), TOT_HR, 10);      //時數
					rTab[iRow][8] = operation.floatAdd(rTab[iRow][8].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][7] = operation.floatAdd(rTab[lastRow][7].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][8] = operation.floatAdd(rTab[lastRow][8].trim(), COMP_PAY, 10); //公司負擔
				}
				//20141121 JENNY ELINE end:改寫法，如果不是A 主管職，就是非主管職
				rTab[iRow][9] = operation.floatAdd(rTab[iRow][9].trim(), TOT_HR, 10);          //時數
				rTab[iRow][10] = operation.floatAdd(rTab[iRow][10].trim(), COMP_PAY, 10); //公司負擔
				rTab[lastRow][9] = operation.floatAdd(rTab[lastRow][9].trim(), TOT_HR, 10);          //時數
				rTab[lastRow][10] = operation.floatAdd(rTab[lastRow][10].trim(), COMP_PAY, 10); //公司負擔
			}

			//PQ22
			Hashtable hData = new Hashtable();
			sbSQL.setLength(0);
			sbSQL.append(" select distinct sum(a.COMP_PAY) , sum(a.TOT_HR) , a.LESS_KEY , a.SDATE");
			sbSQL.append(" from PQ22 a");
			sbSQL.append(" where a.SDATE between '" + convert.ToSql(YY1) + "0101' and '"+ convert.ToSql(YY1) +"1231'");
			sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22_CLOSE x) ");
			sbSQL.append(SUPERA);
			if(CPNYID.length() != 0){
				sbSQL.append(" and a.CPNYID = '"+ convert.ToSql(CPNYID) +"' ");
			}
			sbSQL.append(" group by a.LESS_KEY , a.SDATE");
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0;i<ret.length;i++){
				String iCOMP_PAY = ret[i][0].trim();
				String iTOT_HR = ret[i][1].trim();
				String iLESS_KEY = ret[i][2].trim();
				String iSDATE = ret[i][3].trim();

				String key1 = iLESS_KEY + "@COMP_PAY";
				String key2 = iLESS_KEY + "@TOT_HR";
				String key3 = iLESS_KEY + "@SDATE";

				hData.put(key1 , iCOMP_PAY);
				hData.put(key2 , iTOT_HR);
				hData.put(key3 , iSDATE);
			}

			//先算出每個課程總共有幾個人上課
			sbSQL.setLength(0);
			sbSQL.append("select distinct count(*) , LESS_KEY from PQ23");
			sbSQL.append(" where STUDY_CLOSE not in ('1' , '2') ").append(SUPER);
			sbSQL.append(" and LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YY1) + "0101' and '"+ convert.ToSql(YY1) +"1231') ");
			if(CPNYID.length() != 0){
				sbSQL.append(" and CPNYID = '"+convert.ToSql(CPNYID)+"'");
			}
			
			sbSQL.append(" group by LESS_KEY");
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i=0; i<ret.length; i++){
				String icount = ret[i][0].trim();
				String LESS_KEY = ret[i][1].trim();
				String key = LESS_KEY + "@LESS_KEY";
				
				hData.put(key , icount);
			}
			//PQ23
			sbSQL.setLength(0);
			sbSQL.append("select distinct a.LESS_KEY , a.EMPID , b.WORKTYPE , isnull(c.CHIEF , '"+ ISNULL +"')");
			sbSQL.append(" from PQ23 a , HRUSER b left outer join POSITION c on b.POSSIE = c.POSSIE");
			sbSQL.append(" where a.EMPID = b.EMPID");
			sbSQL.append(" and a.CPNYID = b.CPNYID");
			sbSQL.append(SUPERA);
			sbSQL.append(" and a.STUDY_CLOSE not in ('1' , '2')");
			sbSQL.append(" and a.LESS_KEY in (select x.LESS_KEY from PQ22 x where x.SDATE between '" + convert.ToSql(YY1) + "0101' and '"+ convert.ToSql(YY1) +"1231')");
			if(CPNYID.length() != 0){
				sbSQL.append(" and a.CPNYID = '" + convert.ToSql(CPNYID) + "'");
			}
			ret = t.queryFromPool( sbSQL.toString() );
			for (int i = 0 ; i < ret.length ; i++){
				String iLESS_KEY = ret[i][0].trim();
				YYMM = (String)hData.get( iLESS_KEY+"@SDATE" );  //年月
				if ( YYMM == null ){
					continue;
				}
				EMPID = ret[i][1].trim(); //員工編號
				WORKTYPE = ret[i][2].trim(); //直接/間接員工
				CHIEF = ret[i][3].trim();          //基層/中高階主管
				TOT_HR = (String)hData.get( iLESS_KEY+"@TOT_HR" );      //上課時數
				COMP_PAY = (String)hData.get( iLESS_KEY+"@COMP_PAY" ); //公司負擔
				//公司負擔應為 ==> 公司負擔/這個課程有幾個人上課
				String pCount = (String)hData.get( iLESS_KEY+"@LESS_KEY" );
				//2012/05/02edward:該課程上課人數必須>=1
				if ( operation.compareTo(pCount , "0") <= 0 ){
					continue;
				}

				COMP_PAY = operation.floatDivide(COMP_PAY , pCount , 10);

				if (TOT_HR == null || TOT_HR.length() == 0) {
					TOT_HR = "0";
				}

				if(YYMM.length() >= 6){
					YYMM = YYMM.substring(0,6);
				}
				
				if("A".equals(DATELIST)){
					YYMM = operation.sub(YYMM,"191100");
				}
				
				ret[i][0] = YYMM;

				iRow = Integer.parseInt(convert.replace(""+getYM.get(YYMM),"null","-1"));
				
				if (iRow < 0) continue;
				//直接員工
				if (WORKTYPE.equals("B")){
					rTab[iRow][1] = operation.floatAdd(rTab[iRow][1].trim(), TOT_HR, 10);      //時數
					rTab[iRow][2] = operation.floatAdd(rTab[iRow][2].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][1] = operation.floatAdd(rTab[lastRow][1].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][2] = operation.floatAdd(rTab[lastRow][2].trim(), COMP_PAY, 10); //公司負擔
				//間接員工
				}else if (WORKTYPE.equals("A")){
					rTab[iRow][3] = operation.floatAdd(rTab[iRow][3].trim(), TOT_HR, 10);      //時數
					rTab[iRow][4] = operation.floatAdd(rTab[iRow][4].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][3] = operation.floatAdd(rTab[lastRow][3].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][4] = operation.floatAdd(rTab[lastRow][4].trim(), COMP_PAY, 10); //公司負擔
				}
				
				//20141121 JENNY ELINE begin:改寫法，如果不是A 主管職，就是非主管職
				//基層員工
				//if (CHIEF.equals("C")){
				//主管職 (A)
				if (CHIEF.length() > 0 && "A".equals(CHIEF)){
					rTab[iRow][5] = operation.floatAdd(rTab[iRow][5].trim(), TOT_HR, 10);      //時數
					rTab[iRow][6] = operation.floatAdd(rTab[iRow][6].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][5] = operation.floatAdd(rTab[lastRow][5].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][6] = operation.floatAdd(rTab[lastRow][6].trim(), COMP_PAY, 10); //公司負擔
				//}else if ((CHIEF.equals("A") || CHIEF.equals("B"))){
				}else{//非主管職 
					rTab[iRow][7] = operation.floatAdd(rTab[iRow][7].trim(), TOT_HR, 10);      //時數
					rTab[iRow][8] = operation.floatAdd(rTab[iRow][8].trim(), COMP_PAY, 10); //公司負擔
					rTab[lastRow][7] = operation.floatAdd(rTab[lastRow][7].trim(), TOT_HR, 10);      //時數
					rTab[lastRow][8] = operation.floatAdd(rTab[lastRow][8].trim(), COMP_PAY, 10); //公司負擔
				}
				//20141121 JENNY ELINE end:改寫法，如果不是A 主管職，就是非主管職
				rTab[iRow][9] = operation.floatAdd(rTab[iRow][9].trim(), TOT_HR, 10);          //時數
				rTab[iRow][10] = operation.floatAdd(rTab[iRow][10].trim(), COMP_PAY, 10); //公司負擔
				rTab[lastRow][9] = operation.floatAdd(rTab[lastRow][9].trim(), TOT_HR, 10);          //時數
				rTab[lastRow][10] = operation.floatAdd(rTab[lastRow][10].trim(), COMP_PAY, 10); //公司負擔
			}

			//最後整理
			for (int i=0; i<rTab.length; i++){
				rTab[i][1] = operation.floatAdd(rTab[i][1].trim(), "0", 1);
				rTab[i][2] = operation.floatAdd(rTab[i][2].trim(), "0", 2);
				rTab[i][3] = operation.floatAdd(rTab[i][3].trim(), "0", 1);
				rTab[i][4] = operation.floatAdd(rTab[i][4].trim(), "0", 2);
				rTab[i][5] = operation.floatAdd(rTab[i][5].trim(), "0", 1);
				rTab[i][6] = operation.floatAdd(rTab[i][6].trim(), "0", 2);
				rTab[i][7] = operation.floatAdd(rTab[i][7].trim(), "0", 1);
				rTab[i][8] = operation.floatAdd(rTab[i][8].trim(), "0", 2);
				rTab[i][9] = operation.floatAdd(rTab[i][9].trim(), "0", 1);
				rTab[i][10] = operation.floatAdd(rTab[i][10].trim(), "0", 2);
			}
			//2012/04/25:edward:end:這段,在計算公司費用時,PQ22的部份有誤,整段重改			
			
			setTableData("table1",rTab);
			setValue("PDATE", YY);
			setValue("CPNYID", CPNYID);
			return false;
		}
		return true;
	}
	public String getInformation(){
		return "---------------\u67e5\u8a62\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
	}
}

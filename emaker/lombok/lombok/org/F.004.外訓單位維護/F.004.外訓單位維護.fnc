�� sr java.util.Vectorٗ}[�;� I capacityIncrementI elementCount[ elementDatat [Ljava/lang/Object;xp       ur [Ljava.lang.Object;��X�s)l  xp   
sr java.util.Hashtable�%!J� F 
loadFactorI 	thresholdxp?@     !w   -   !t html_uniqueIDsr java.lang.Long;��̏#� J valuexr java.lang.Number������  xp   �މq�t form_numbersr java.lang.Integer⠤���8 I valuexq ~ 	   
t spec2t�<html>
<head>
</head>
<body>
<p>E.2.5.外訓單位維護
</p><p>【功能說明】
</p><p> 1. 此作業提供使用者建立及維護外訓單位資料，以利同仁外訓申請。
</p><p> 2. 此作業預設會先計算已經存在的外訓單位資料，系統自動計算最大的外訓單位編號，顯示於畫面上，提供使用者參考，不可修改。
</p><p> 3. 若要新增課程資料，使用者可輸入包含外訓單位編號、外訓單位中文名稱、外訓單位英文名稱、外訓單位中文簡稱、外訓單位英文簡稱
</p><p>    發票中文地址、發票英文地址、負責人、連絡人、統一編號、電話(1)、電話(2)、傳真機、電子信箱、郵遞區號、通訊地址等資料，再點選畫面上的"新增按鈕" ，
</p><p>    若資料無誤，系統會出現"資料庫異動完成" ；
</p><p>    若所輸入的資料不夠完整，系統新增時檢核：外訓單位中文名稱：不可空白
</p><p>　					    發票中文地址：不可空白
</p><p>　					    電話1：不可空白
</p><p>　					    統一編號：不可空白
</p><p>　					    通訊地址：不可空白
</p><p>    否則系統會出現提示訊息，新增作業不成功。
</p><p> 4. 若要查詢外訓單位資料，請先點選查詢按鈕，下拉式清單選擇或是輸入您想要選擇的"外訓單位編號" 、"外訓單位中文名稱"等查詢條件，
</p><p>    空白表示全部資料，按下"確定"按鈕後，系統會帶出符合查詢條件的資料。   
</p><p> 5. 若要修改外訓單位資料，請先點選查詢，帶出符合條件的資料後，可以點選右上方"詳細列表"的功能鍵，一覽查詢的內容資料，選擇要修改的該筆紀錄，點選詳細列表的"CLOSE"按鈕，
</p><p>    系統會將該筆記錄帶入畫面中，修改完資料後，點選"修改按鈕"， 若資料無誤，系統會出現"資料庫異動完成"   
</p><p>    若所輸入的資料不夠完整，系統修改時同樣也會檢核：外訓單位中文名稱：不可空白
</p><p>　					    	   發票中文地址：不可空白
</p><p>　					           電話1：不可空白
</p><p>　					           統一編號：不可空白
</p><p>　					           通訊地址：不可空白
</p><p>   否則系統會出現提示訊息，修改作業不成功。
</p><p>   
</p><p> 6. 若要刪除外訓單位資料，請先點選查詢，帶出符合條件的資料後，可以點選右上方"詳細列表"的功能鍵，一覽查詢的內容資料，選擇要刪除的該筆紀錄，點選詳細列表的"CLOSE"按鈕，
</p><p>    系統會將該筆記錄帶入畫面中，點選"刪除按鈕"，系統會再次確認"是否刪除?" 若選擇"是"，則系統會刪除該筆外訓單位資料，成功後出現"資料庫異動完成"；若選擇"否"，則跳回原畫面。
</p><p>    若現在有5 筆外訓資料，若刪除掉，課程編號為4 號的資料，下一次新增外訓單位資料時，系統仍會取最大號+1 ，也就是外訓單位編號為6 號，而不會補之前已刪除的4 號空號。
</p><p> 7. 可列印畫面上所顯示的外訓單位資料內容。
</p><p> 8. 欄位說明 :
</p><p>    外訓單位編號(COMPID) : 系統自動給號，讀取外訓單位資料表(PQ06) 編號最大值，加1 顯示於畫面上
</p><p>    外訓單位中文名稱(COCNAME):輸入外訓單位中文名稱，不可空白
</p><p>    外訓單位英文名稱(COENAME):輸入外訓單位英文名稱
</p><p>    外訓單位中文簡稱(COSCNAME):輸入外訓單位中文簡稱
</p><p>    外訓單位英文簡稱(COSENAME):輸入外訓單位英文簡稱
</p><p>    發票中文地址(CINVADDR):輸入發票中文地址，不可空白
</p><p>    發票英文地址(EINVADDR):輸入發票英文地址
</p><p>    負責人(BOSSNAME):輸入負責人姓名
</p><p>    連絡人(CON_NAME):輸入連絡人姓名
</p><p>    統一編號(COLICNID):輸入外訓單位統一編號，不可空白
</p><p>    電話(1)(TELNO1):輸入聯絡電話，不可空白
</p><p>    電話(2)(TELNO2):輸入聯絡電話
</p><p>    傳真機 (FAXNO):輸入傳真電話
</p><p>    電子信箱 (EMAIL):輸入聯絡電子信箱
</p><p>    郵遞區號 (POSTAL):輸入郵遞區號
</p><p>    通訊地址(COMADDR):輸入通訊地址，不可空白
</p><p>    申請人(MUSER):隱藏欄位
</p><p>    申請日(MDATE): 隱藏欄位
</p><p>    申請時間(MTIME):隱藏欄位 
</p><p> 
</p><p>
</p><p>
</p><p>【新增】
</p><p> 1.新增檢核：
</p><p>　 外訓單位中文名稱：不可空白
</p><p>　 發票中文地址：不可空白
</p><p>　 電話1：不可空白
</p><p>　 統一編號：不可空白
</p><p>　 通訊地址：不可空白
</p><p>   否則系統會出現提示訊息，新增作業不成功。
</p><p> 2. 同時寫入外訓單位資料(PQ06) 的申請人(MUSER)=登錄帳號、申請日(MDATE)=系統日今天、申請時間(MTIME)=系統時間。
</p><p>
</p><p>【查詢】
</p><p> 查詢條件 :
</p><p> 1.外訓單位編號(LIKE)：下拉式清單帶出外訓單位編號，可關鍵字輸入，空白表查詢全部
</p><p> 2.外訓單位中文名稱(LIKE)：下拉式清單帶出外訓中文名稱，可關鍵字輸入，空白表查詢全部
</p><p>
</p><p>  
</p><p>【修改】
</p><p>  1.修改檢核：
</p><p>　  外訓單位中文名稱：不可空白
</p><p>　  發票中文地址：不可空白
</p><p>　  電話1：不可空白
</p><p>　  統一編號：不可空白
</p><p>　  通訊地址：不可空白 
</p><p>    否則系統會出現提示訊息，修改作業不成功。
</p><p> 2. 同時異動外訓單位資料(PQ06) 的申請人(MUSER)=登錄帳號、申請日(MDATE)=系統日今天、申請時間(MTIME)=系統時間。
</p><p>
</p><p>【刪除】
</p><p> 1. 允許刪除外訓單位資料。
</p><p>
</p><p>【列印】
</p><p> 1.可列印畫面上所顯示的外訓單位資料內容。
</p><p>
</p><p>【主表欄位說明】
</p><p> 外訓單位資料(PQ06)
</p><p> 1.外訓單位編號(COMPID) : 系統自動給號，讀取外訓單位資料表(PQ06) 編號最大值，加1 顯示於畫面上
</p><p> 2.外訓單位中文名稱(COCNAME):輸入外訓單位中文名稱，不可空白
</p><p> 3.外訓單位英文名稱(COENAME):輸入外訓單位英文名稱
</p><p> 4.外訓單位中文簡稱(COSCNAME):輸入外訓單位中文簡稱
</p><p> 5.外訓單位英文簡稱(COSENAME):輸入外訓單位英文簡稱
</p><p> 6.發票中文地址(CINVADDR):輸入發票中文地址，不可空白
</p><p> 7.發票英文地址(EINVADDR):輸入發票英文地址
</p><p> 8.負責人(BOSSNAME):輸入負責人姓名
</p><p> 9.連絡人(CON_NAME):輸入連絡人姓名
</p><p> 10.統一編號(COLICNID):輸入外訓單位統一編號，不可空白
</p><p> 11.電話(1)(TELNO1):輸入聯絡電話，不可空白
</p><p> 12.電話(2)(TELNO2):輸入聯絡電話
</p><p> 13.傳真機 (FAXNO):輸入傳真電話
</p><p> 14.電子信箱 (EMAIL):輸入聯絡電子信箱
</p><p> 15.郵遞區號 (POSTAL):輸入郵遞區號
</p><p> 16.通訊地址(COMADDR):輸入通訊地址，不可空白
</p><p> 17.申請人(MUSER):隱藏欄位
</p><p> 18.申請日(MDATE): 隱藏欄位
</p><p> 19.申請時間(MTIME):隱藏欄位 
</p><p> 
</p><p>
</p><p>
</p><p>
</p></body>
</html>t 
allowPrintsr java.lang.Boolean� r�՜�� Z valuexp t portal_datasq ~ ?@     w      t config2sq ~         2uq ~    Psq ~         uq ~    
t  q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxsq ~         uq ~    
q ~ q ~ ppppppppxppppppppppppppppppppppppppppppxt config1sq ~ ?@     w      t widthsur [IM�`&v겥  xp         xt config3sq ~         uq ~    sq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
q ~ q ~ q ~ q ~ ppppppxsq ~         uq ~    
t  q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxsq ~         uq ~    
q ~ �q ~ �q ~ �q ~ �ppppppxxxt allowExportsq ~  t uniqueIDsq ~   8&�;�t clazzt  t allowDeletesq ~  t 	allowEditsq ~  t form_uniqueIDsq ~    �њ�t control_detailsq ~ t report_uniqueIDsq ~    ��6�t 	authorizesq ~ ?@     w   #   t (100049)sq ~ ?@     w      t allowDeletesq ~  t allowAddsq ~  t 	allowEditsq ~  t 
allowPrintsq ~  t 
allowQuerysq ~  t allowExportsq ~  xt (100016)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100002)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100069)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100022)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100003)sq ~ ?@     w      q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ xt (110001)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100018)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100004)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (110002)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt A3SETsq ~ ?@     w      q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ xt (110003)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100063)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100030)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100006)sq ~ ?@     w      q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~  q ~ �sq ~ q ~ �sq ~ xt (110004)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100064)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100012)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100007)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100065)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100027)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100008)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100066)sq ~ ?@     w      q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  q ~ �sq ~  xt (100001)sq ~ ?@     w      q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ q ~ �sq ~ xxt formsq ~          uq ~    
ppppppppppxt html_numbersq ~    
t levelt 3t flowControlsq ~  t urlt http://t 
spec_caretsq ~   �t batcht  t field_checksq ~ ?@     w      t COLICNIDsq ~ ?@     w      t check1t  t nameq ~�t msgt  t check2q ~�xt FAXNOsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt COMADDRsq ~ ?@     w      q ~�t 不可空白q ~�q ~�q ~�q ~�q ~�q ~�xt MUSERsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt EINVADDRsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt POSTALsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt COMPIDsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�t  xt MTIMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt COENAMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt TELNO2sq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt TELNO1sq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt CON_NAMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt EMAILsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt COCNAMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt COSENAMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt COSCNAMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt MDATEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt CINVADDRsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xt BOSSNAMEsq ~ ?@     w      q ~�q ~�q ~�q ~�q ~�q ~�q ~�q ~�xxt 查詢條件設定sq ~ ?@     w      t COMADDRsq ~ ?@     w      t captiont 通  訊  地  址t enabledsq ~ t op1t andt defaultvaluesq ~ ?@     w       xt 	inputtypet 文字輸入t assistsq ~ ?@     w       xt opt =t 欄位檢核sq ~ ?@     w       xxt COSCNAMEsq ~ ?@     w      q ~�t 外訓單位中文簡稱q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt COSENAMEsq ~ ?@     w      q ~�t 外訓單位英文簡稱q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt CON_NAMEsq ~ ?@     w      q ~�t 連     絡     人q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt COLICNIDsq ~ ?@     w      q ~�t 統  一  編  號q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt MUSERsq ~ ?@     w      q ~�t MUSERq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt EINVADDRsq ~ ?@     w      q ~�t 發票英文地址q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt TELNO2sq ~ ?@     w      q ~�t 電          話(2)q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt FAXNOsq ~ ?@     w      q ~�t 傳     真     機q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt TELNO1sq ~ ?@     w      q ~�t 電          話(1)q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt BOSSNAMEsq ~ ?@     w      q ~�t 負     責     人q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt EMAILsq ~ ?@     w      q ~�t 電  子  信  箱 q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt COCNAMEsq ~ ?@     w      t 
allowRadiosq ~  t allowSpaceWhenSelectsq ~ q ~�sq ~ ?@     w       xq ~�t 外訓單位中文名稱t 自定查詢條件sq ~ ?@     w      t importt wimport jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t timesq ~   E-��t prgnamet PQ1646133100791t 	classHeadt  extends bQuery{
t idt admint 
methodHeadt +public String getFilter()throws Throwable{
t contentt%/*
	version		modify_date		modify_user		description
	1.00				20220301			ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
String ISNULL = (String) get("SYSTEM.ISNULL");
//20220301 ANDYHOU ELIZA begin:權限改使用EDUCATION.SUPER與CONDITION_B06
//return " and (1=1 "+(String) get("CPNYID.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"')  order by 1";	
return " and (1=1 "+(String) get("EDUCATION.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"')  order by 1";
//20220301 ANDYHOU ELIZA end:權限改使用EDUCATION.SUPER與CONDITION_B06
t classEndt }
t oldsq ~         uq ~    
sq ~ ?@     w      t timesq ~   :��*�t contentt�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
String ISNULL = (String) get("SYSTEM.ISNULL");
return " and (1=1 "+(String) get("CPNYID.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"') order by 1";
t idt admint prgnamet PQ1352775969078xsq ~ ?@     w      t timesq ~   :��	t prgnamet PQ1156328636633t contentt// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by 1";t idt adminxsq ~ ?@     w      t timesq ~   䅰t prgnamet PQ1108519188046t contentt// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by COCNAME";t idt adminxpppppppxt caretsq ~   �t 	methodEndq ~?xt inputtype_text_4t )PQ06[{!@#}]COCNAME[{!@#}]COCNAME[{!@#}]hrq ~�sq ~ q ~�t &下拉選單(自其它資料表帶出)t 
allowInputsq ~ q ~�sq ~ ?@     w       xt keyword_searchsq ~  q ~�q ~�q ~�t likeq ~�sq ~ ?@     w       xxt COENAMEsq ~ ?@     w      q ~�t 外訓單位英文名稱q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt POSTALsq ~ ?@     w      q ~�t 郵  遞  區  號 q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt CINVADDRsq ~ ?@     w      q ~�t 發票中文地址q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt MTIMEsq ~ ?@     w      q ~�t MTIMEq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt MDATEsq ~ ?@     w      q ~�t MDATEq ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt (showTextCOSCNAME)sq ~ ?@     w      t captiont  t enabledsq ~ t op1t andt defaultvaluesq ~ ?@     w       xt 	inputtypet 文字輸入t assistsq ~ ?@     w       xt opt =t 欄位檢核sq ~ ?@     w       xxt CPNYIDsq ~ ?@     w      q ~�t 	公司別q ~�sq ~ q ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xq ~�q ~�q ~�sq ~ ?@     w       xxt COMPIDsq ~ ?@     w      q ~(sq ~  q ~*sq ~ q ~�sq ~ ?@     w       xq ~�t 外訓單位編號q ~.sq ~ ?@     w      q ~0q ~1q ~2sq ~   E-(Xq ~4t PQ1646133059512q ~6q ~7q ~8q ~9q ~:q ~;q ~<t%/*
	version		modify_date		modify_user		description
	1.00				20220301			ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
String ISNULL = (String) get("SYSTEM.ISNULL");
//20220301 ANDYHOU ELIZA begin:權限改使用EDUCATION.SUPER與CONDITION_B06
//return " and (1=1 "+(String) get("CPNYID.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"')  order by 1";	
return " and (1=1 "+(String) get("EDUCATION.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"')  order by 1";
//20220301 ANDYHOU ELIZA end:權限改使用EDUCATION.SUPER與CONDITION_B06
q ~>q ~?q ~@sq ~         uq ~    
sq ~ ?@     w      q ~Dsq ~   :����q ~Ft�// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
String ISNULL = (String) get("SYSTEM.ISNULL");
return " and (1=1 "+(String) get("CPNYID.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"')  order by 1";q ~Hq ~Iq ~Jt PQ1352775945828xsq ~ ?@     w      q ~Msq ~   :��q ~Ot PQ1156328627400q ~Qt// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by 1";q ~Sq ~Txsq ~ ?@     w      q ~Vsq ~   ��Zq ~Xt PQ1108519040140q ~Zt// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
return " order by COMPID";q ~\q ~]xpppppppxq ~^sq ~   �q ~`q ~?xq ~at 'PQ06[{!@#}]COMPID[{!@#}]COMPID[{!@#}]hrq ~�sq ~ q ~�q ~dq ~esq ~ q ~�sq ~ ?@     w       xq ~hsq ~  q ~�q ~�q ~�q ~jq ~�sq ~ ?@     w       xxxt allowAddsq ~  t keyt F.004t namet F.004.外訓單位維護t stypet htmlt 列表設定sq ~ ?@     w      t Everyonesq ~ ?@     w      t captiont 外訓單位維護t listsq ~         uq ~    
sq ~         uq ~    
t COMPID(COMPID)t 外訓單位編號t 置中sq ~    nppppppxsq ~         uq ~    
t COCNAME(COCNAME)t 外訓單位中文名稱t 置左sq ~    �ppppppxsq ~         uq ~    
t COSCNAME(COSCNAME)sq ~ ?@     w      t ct 外訓單位中文簡稱xq ~�sq ~    �ppppppxsq ~         uq ~    
t CON_NAME(CON_NAME)sq ~ ?@     w      q ~�t 	連絡人xq ~�sq ~    bppppppxsq ~         uq ~    
t TELNO1(TELNO1)sq ~ ?@     w      q ~�t 電話xq ~�sq ~    hppppppxsq ~         uq ~    
t COLICNID(COLICNID)sq ~ ?@     w      q ~�t 統 一編號xq ~�sq ~    ^ppppppxppppxxxt spect�E.2.5.外訓單位維護
【功能說明】
 1. 此作業提供使用者建立及維護外訓單位資料，以利同仁外訓申請。
 2. 此作業預設會先計算已經存在的外訓單位資料，系統自動計算最大的外訓單位編號，顯示於畫面上，提供使用者參考，不可修改。
 3. 若要新增課程資料，使用者可輸入包含外訓單位編號、外訓單位中文名稱、外訓單位英文名稱、外訓單位中文簡稱、外訓單位英文簡稱
    發票中文地址、發票英文地址、負責人、連絡人、統一編號、電話(1)、電話(2)、傳真機、電子信箱、郵遞區號、通訊地址等資料，再點選畫面上的"新增按鈕" ，
    若資料無誤，系統會出現"資料庫異動完成" ；
    若所輸入的資料不夠完整，系統新增時檢核：外訓單位中文名稱：不可空白
　					    發票中文地址：不可空白
　					    電話1：不可空白
　					    統一編號：不可空白
　					    通訊地址：不可空白
    否則系統會出現提示訊息，新增作業不成功。
 4. 若要查詢外訓單位資料，請先點選查詢按鈕，下拉式清單選擇或是輸入您想要選擇的"外訓單位編號" 、"外訓單位中文名稱"等查詢條件，
    空白表示全部資料，按下"確定"按鈕後，系統會帶出符合查詢條件的資料。   
 5. 若要修改外訓單位資料，請先點選查詢，帶出符合條件的資料後，可以點選右上方"詳細列表"的功能鍵，一覽查詢的內容資料，選擇要修改的該筆紀錄，點選詳細列表的"CLOSE"按鈕，
    系統會將該筆記錄帶入畫面中，修改完資料後，點選"修改按鈕"， 若資料無誤，系統會出現"資料庫異動完成"   
    若所輸入的資料不夠完整，系統修改時同樣也會檢核：外訓單位中文名稱：不可空白
　					    	   發票中文地址：不可空白
　					           電話1：不可空白
　					           統一編號：不可空白
　					           通訊地址：不可空白
   否則系統會出現提示訊息，修改作業不成功。
   
 6. 若要刪除外訓單位資料，請先點選查詢，帶出符合條件的資料後，可以點選右上方"詳細列表"的功能鍵，一覽查詢的內容資料，選擇要刪除的該筆紀錄，點選詳細列表的"CLOSE"按鈕，
    系統會將該筆記錄帶入畫面中，點選"刪除按鈕"，系統會再次確認"是否刪除?" 若選擇"是"，則系統會刪除該筆外訓單位資料，成功後出現"資料庫異動完成"；若選擇"否"，則跳回原畫面。
    若現在有5 筆外訓資料，若刪除掉，課程編號為4 號的資料，下一次新增外訓單位資料時，系統仍會取最大號+1 ，也就是外訓單位編號為6 號，而不會補之前已刪除的4 號空號。
 7. 可列印畫面上所顯示的外訓單位資料內容。
 8. 欄位說明 :
    外訓單位編號(COMPID) : 系統自動給號，讀取外訓單位資料表(PQ06) 編號最大值，加1 顯示於畫面上
    外訓單位中文名稱(COCNAME):輸入外訓單位中文名稱，不可空白
    外訓單位英文名稱(COENAME):輸入外訓單位英文名稱
    外訓單位中文簡稱(COSCNAME):輸入外訓單位中文簡稱
    外訓單位英文簡稱(COSENAME):輸入外訓單位英文簡稱
    發票中文地址(CINVADDR):輸入發票中文地址，不可空白
    發票英文地址(EINVADDR):輸入發票英文地址
    負責人(BOSSNAME):輸入負責人姓名
    連絡人(CON_NAME):輸入連絡人姓名
    統一編號(COLICNID):輸入外訓單位統一編號，不可空白
    電話(1)(TELNO1):輸入聯絡電話，不可空白
    電話(2)(TELNO2):輸入聯絡電話
    傳真機 (FAXNO):輸入傳真電話
    電子信箱 (EMAIL):輸入聯絡電子信箱
    郵遞區號 (POSTAL):輸入郵遞區號
    通訊地址(COMADDR):輸入通訊地址，不可空白
    申請人(MUSER):隱藏欄位
    申請日(MDATE): 隱藏欄位
    申請時間(MTIME):隱藏欄位 
 


【新增】
 1.新增檢核：
　 外訓單位中文名稱：不可空白
　 發票中文地址：不可空白
　 電話1：不可空白
　 統一編號：不可空白
　 通訊地址：不可空白
   否則系統會出現提示訊息，新增作業不成功。
 2. 同時寫入外訓單位資料(PQ06) 的申請人(MUSER)=登錄帳號、申請日(MDATE)=系統日今天、申請時間(MTIME)=系統時間。

【查詢】
 查詢條件 :
 1.外訓單位編號(LIKE)：下拉式清單帶出外訓單位編號，可關鍵字輸入，空白表查詢全部
 2.外訓單位中文名稱(LIKE)：下拉式清單帶出外訓中文名稱，可關鍵字輸入，空白表查詢全部

  
【修改】
  1.修改檢核：
　  外訓單位中文名稱：不可空白
　  發票中文地址：不可空白
　  電話1：不可空白
　  統一編號：不可空白
　  通訊地址：不可空白 
    否則系統會出現提示訊息，修改作業不成功。
 2. 同時異動外訓單位資料(PQ06) 的申請人(MUSER)=登錄帳號、申請日(MDATE)=系統日今天、申請時間(MTIME)=系統時間。

【刪除】
 1. 允許刪除外訓單位資料。

【列印】
 1.可列印畫面上所顯示的外訓單位資料內容。

【主表欄位說明】
 外訓單位資料(PQ06)
 1.外訓單位編號(COMPID) : 系統自動給號，讀取外訓單位資料表(PQ06) 編號最大值，加1 顯示於畫面上
 2.外訓單位中文名稱(COCNAME):輸入外訓單位中文名稱，不可空白
 3.外訓單位英文名稱(COENAME):輸入外訓單位英文名稱
 4.外訓單位中文簡稱(COSCNAME):輸入外訓單位中文簡稱
 5.外訓單位英文簡稱(COSENAME):輸入外訓單位英文簡稱
 6.發票中文地址(CINVADDR):輸入發票中文地址，不可空白
 7.發票英文地址(EINVADDR):輸入發票英文地址
 8.負責人(BOSSNAME):輸入負責人姓名
 9.連絡人(CON_NAME):輸入連絡人姓名
 10.統一編號(COLICNID):輸入外訓單位統一編號，不可空白
 11.電話(1)(TELNO1):輸入聯絡電話，不可空白
 12.電話(2)(TELNO2):輸入聯絡電話
 13.傳真機 (FAXNO):輸入傳真電話
 14.電子信箱 (EMAIL):輸入聯絡電子信箱
 15.郵遞區號 (POSTAL):輸入郵遞區號
 16.通訊地址(COMADDR):輸入通訊地址，不可空白
 17.申請人(MUSER):隱藏欄位
 18.申請日(MDATE): 隱藏欄位
 19.申請時間(MTIME):隱藏欄位 
 


t 
allowQuerysq ~  t 
advancecfgsq ~ ?@     8w   K   5t 
print_viewt  t 刪除按鈕標題t 	[default]t progress_queryt  t 新增按鈕標題q ~t allow_batch_approvet Yest !刪除發生錯誤才顯示訊息sq ~  t 
qwin_widthq ~ �t query_triggersq ~ ?@     w       xt allowCreatorModifyt Not 刪除按鈕程式sq ~ ?@     w       xt 新增按鈕程式sq ~ ?@     w      t idt admint oldsq ~         uq ~    
sq ~ ?@     w      t timesq ~   /Ga�nt versq ~    t contentt  t idt admint prgnamet 	hr/E005ptxsq ~ ?@     w      t timesq ~   /GG$Ot versq ~     t contentt  t idt admint prgnamet hr6/E1005ptxsq ~ ?@     w      t timesq ~   �'t versq ~    t contentq ~�t idq ~]t prgnamet hr6/E1005ptxpppppppxt contentt  t 	methodEndt }
t classEndq ~>t infot �public String getInformation(){
	return "---------------\u65b0\u589e\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
}
t 	classHeadt  extends bTransaction{
t timesq ~   :���xt embeddedsq ~  t verq ~ t caretsq ~   �t importt �import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t prgnamet 	hr/E005ptt old_prgnamet hr4/E1005ptt 
methodHeadt 6public boolean action(String value)throws Throwable{
xt allow_flow_list_queryt Not 
key_updatet by global configt flow_mirrort  t print_currentq ~Tt field_checksq ~ ?@     w       xt 列印按鈕標題q ~t progress_addq ~t 查詢條件設定sq ~ ?@     w       xt insert_triggersq ~ ?@     w       xt query_scopet 
Owned rowst flow_table2q ~t flow_table1q ~t 
firstStaget 	待處理t update_triggersq ~ ?@     w       xt progress_deleteq ~t delete_triggersq ~ ?@     w       xt print_triggersq ~ ?@     w       xt !修改發生錯誤才顯示訊息sq ~  t 列印按鈕程式sq ~ ?@     w       xt 
flow_ordert  t helpt  t 
whiteBoardt Not qmaxq ~t 刪除前先顯示確認視窗sq ~ t progress_editq ~t checkKeyt 外訓單位編號不可重覆t 	queryModet Not 查詢按鈕標題q ~t allowFlowModifyt Yest 自定查詢條件sq ~ ?@     w      q ~Ot PQ1646132983709q ~Msq ~   E,[t 	methodEndt }
q ~Qt/*
	version		modify_date		modify_user		description
	1.00				20220301			ANDYHOU		 	權限改使用EDUCATION.SUPER與CONDITION_B06
*/
// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100

String ISNULL = (String) get("SYSTEM.ISNULL");
//20220301 ANDYHOU ELIZA begin:權限改使用EDUCATION.SUPER與CONDITION_B
//return " and (1=1 "+(String) get("CPNYID.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"') ";
return " and (1=1 "+(String)get("EDUCATION.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"') ";
//20220301 ANDYHOU ELIZA end:權限改使用EDUCATION.SUPER與CONDITION_B06
t oldsq ~         uq ~    
sq ~ ?@     w      q ~Dsq ~   :��6�q ~Ft// 回傳值為自定查詢條件
// 回傳值必須是空白或以 and 開始,如 "and FIELD1='ABC'"
// 也可以回傳完整的 SQL 語法取代原設定的值 如 select distinct display_field,data_field from table1 where type=100
String ISNULL = (String) get("SYSTEM.ISNULL");
return " and (1=1 "+(String) get("CPNYID.SUPER")+" or isnull(CPNYID,'"+ISNULL+"')='"+ISNULL+"') ";q ~Hq ~Iq ~Jt PQ1352775709843xpppppppppxt importt ~import jcx.jform.bQuery;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;

t 
methodHeadt ,public String getFilter()throws Throwable{
t classEndq ~�q ~Sq ~9t 	classHeadt  extends bQuery{
t infot �public String getInformation(){
	return "---------------Function.\u81ea\u5b9a\u67e5\u8a62\u689d\u4ef6.Query_filter()----------------";
}
t caretsq ~   �xt 	flow_viewq ~ �t 修改按鈕標題q ~t q_typet isolatet qwin_heightq ~ �t 	flow_typet Stateful Flowt 查詢按鈕程式sq ~ ?@     w       xt SAt  t !新增發生錯誤才顯示訊息sq ~  t 修改按鈕程式sq ~ ?@     w      q ~Mt 	hr/E005ptq ~2sq ~   :���q ~;q ~<q ~=q ~>t oldsq ~         uq ~    
sq ~ ?@     w      q ~sq ~   /GGagq ~sq ~     q ~!q ~-q ~#q ~/q ~%t 	hr/E005ptxsq ~ ?@     w      q ~3sq ~   � �q ~5sq ~    q ~7q ~�q ~8q ~]q ~9t hr6/E1005ptxppppppppxq ~Kq ~Lq ~Qq ~Rq ~?q ~>q ~8q ~q ~Bq ~Cq ~@t �public String getInformation(){
	return "---------------\u4fee\u6539\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
}
q ~Isq ~   �q ~Fsq ~  t verq ~�xt 	only_flowt Not 新增後清除所有欄位sq ~ t customize_listt Yesxt detailsq ~ ?@     w      t Everyonesq ~ ?@     w   )   t MUSERsq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~ xt text3sq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt showTextCOSCNAMEsq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt POSTALsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt text1sq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt TELNO2sq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt EMAILsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt COSCNAMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~ q ~�sq ~  q ~�sq ~  xt TELNO1sq ~ ?@     w      q ~�sq ~  q ~�sq ~ q ~�sq ~  q ~�sq ~  xt image1sq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt EINVADDRsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt text11sq ~ ?@     w      q ~sq ~  q ~	sq ~  q ~sq ~  q ~sq ~  xt MDATEsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~ xt text10sq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt MTIMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~ xt COSENAMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt COMPIDsq ~ ?@     w      q ~�sq ~ q ~�sq ~ q ~�sq ~ q ~�sq ~  xt FORM_STATUSsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~ xt box6sq ~ ?@     w      q ~sq ~  q ~	sq ~  q ~sq ~  q ~sq ~  xt default_companysq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt CINVADDRsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt COLICNIDsq ~ ?@     w      q ~�sq ~  q ~�sq ~ q ~�sq ~  q ~�sq ~  xt FAXNOsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt COCNAMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~ q ~�sq ~ q ~�sq ~  xt CON_NAMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~ q ~�sq ~  q ~�sq ~  xt COMADDRsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt text9sq ~ ?@     w      q ~#sq ~  q ~%sq ~  q ~'sq ~  q ~)sq ~  xt COENAMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xt CPNYIDsq ~ ?@     w      t cfg1sq ~  t columnsq ~  t querysq ~  t hidesq ~  xt BOSSNAMEsq ~ ?@     w      q ~�sq ~  q ~�sq ~  q ~�sq ~  q ~�sq ~  xxxt typet formxpppppppppx
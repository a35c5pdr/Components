/*
 version	modify_date		modify_user		description
 1.00		20220302		KEVIN			修改CPNYID.SUPER
*/
package hr;
import jcx.jform.bTransaction;
import java.io.*;
import java.util.*;
import jcx.util.*;
import jcx.html.*;
import jcx.db.*;
/*
		程式設計師：PEGGY
		程式功能：E.6.8.訓練合格人員一覽表(E5008)_查詢按鈕程式
		區域變數：
		函式：
		用到的外部程式：
		修改記錄：
						2005031101PEGGY：初次上線
*/
public class E407pt extends bTransaction{
	public boolean action(String value)throws Throwable{
		// 回傳值為 true 表示執行接下來的資料庫異動或查詢
		// 回傳值為 false 表示接下來不執行任何指令
		// 傳入值 value 為 "新增","查詢","修改","刪除","列印","PRINT" (列印預覽的列印按鈕),"PRINTALL" (列印預覽的全部列印按鈕) 其中之一
		message("");
		
		//查詢狀態
		if (value.equals("查詢")){
			String sql = "";
			String YYMM1 = "";
			String [] YYMM = convert.separStr(getQueryValue("YYMM") , "and");    //年月
			String CPNYID = getQueryValue("CPNYID").trim();
			//20220302 KEVIN ELIZA begin:修改CPNYID.SUPER
			//String SUPER = (String)get("CPNYID.SUPER");
			String SUPER = (String)get("EDUCATION.SUPER");
			//20220302 KEVIN ELIZA end:修改CPNYID.SUPER
			//String CPNYID = (String)get("iCPNYID");              //公司別
			String DATELIST = (String)get("SYSTEM.DATELIST");
			talk t = getTalk();
			setTableData("table1", new String[][]{});
			setTableData("table2", new String[][]{});
			
			//檢查年月
			if (YYMM.length != 2){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("查詢年月不可空白!"));
				message(hr.Msg.hrm8b_F408.Msg001);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			if (DATELIST.equals("A") && !check.isRocDay(YYMM[0].trim()+"01")){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("起始年月格式輸入錯誤(正確格式yymm)"));
				message(hr.Msg.hrm8b_F408.Msg002);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}else if (DATELIST.equals("B") && !check.isACDay(YYMM[0].trim()+"01")){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("起始年月格式輸入錯誤(正確格式YYYYmm)"));
				message(hr.Msg.hrm8b_F408.Msg003);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			if (DATELIST.equals("A") && !check.isRocDay(YYMM[1].trim()+"01")){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("結束年月格式輸入錯誤(正確格式yymm)"));
				message(hr.Msg.hrm8b_F408.Msg004);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}else if (DATELIST.equals("B") && !check.isACDay(YYMM[1].trim()+"01")){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("結束年月格式輸入錯誤(正確格式YYYYmm)"));
				message(hr.Msg.hrm8b_F408.Msg005);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			
			YYMM1 = YYMM[0].trim()+"~"+YYMM[1].trim();
			setValue("YYMM" ,YYMM1);
			if (DATELIST.equals("A")) {
				YYMM[0] = operation.floatAdd(YYMM[0].trim(),"191100",0);
				YYMM[1] = operation.floatAdd(YYMM[1].trim(),"191100",0);
			}
			
			//讀取資料
			//20140729 ANN ELINE LESS_NO改(select LESS_CODE3 from PQ03 where PQ03.LESS_NO = PQ22_D.LESS_NO)
			sql = " select LESS_KEY,(select LESS_CODE3 from PQ03 where PQ03.LESS_NO = PQ22_D.LESS_NO),LESS_NO,TRAIN_DATE,STIME,ETIME,TRAIN_HR from PQ22_D"
			     + " where CPNYID = '" + convert.ToSql(CPNYID) + "' " + SUPER
				 + " and substring(TRAIN_DATE , 1 , 6) between '"+convert.ToSql(YYMM[0].trim())+"' and '"+convert.ToSql(YYMM[1].trim())+"' "
				 + " order by 1,2,TRAIN_DATE,STIME";
			String rTab[][] = t.queryFromPool(sql);
			if (rTab.length == 0){
				
				//20190314 LICK COULSON begin 訊息翻譯
				//message(translate("查無相關資料!"));
				message(hr.Msg.hrm8b_F408.Msg006);
				//20190314 LICK COULSON end 訊息翻譯
				return false;
			}
			setTableData("table1", rTab);
			setValue("CPNYID", CPNYID);
			return false;
		}
		return true;
	}
	public String getInformation(){
		return "---------------\u67e5\u8a62\u6309\u9215\u7a0b\u5f0f.preProcess()----------------";
	}
}


// 當前日期
var currentDate = new Date();
var currentYear = currentDate.getFullYear();
var currentMonth = currentDate.getMonth() + 1;
var mainYear = currentYear;
var mainMonth = currentMonth;


// 紀錄所選日期
var RecordMonth = 0; // 驗證(1上個月 2這個月 3下個月)
var allMonthData = {}; // 日期資料
var isRecordMonth = false; //  紀錄所選日期是否有值並符合現在所選的日期
// 上個月資料
var lastMonthData;
// 這個月資料
var thisMonthData;
// 下個月資料
var nextMonth;

async function fetchIpoQueryCalendData(year, month) {
  try {
    const response = await fetch(
      `${url}/server-java/apiM/ipo/interfaces/IpoQueryCalend?requestBody={"year":"${year}","month":"${month}"}&requestHeader={}`,
      {
        method: "POST",
        redirect: "follow",
      }
    );
    const responseData = await response.json();
     
    return responseData.responseBody.allType;
  } catch (error) {
    console.error("Error:", error);
  }
}

// set Date Language
function DateLanguageSetting() {
  const week = ["日", "一", "二", "三", "四", "五", "六"];
  const month = [
    "一月",
    "二月",
    "三月",
    "四月",
    "五月",
    "六月",
    "七月",
    "八月",
    "九月",
    "十月",
    "十一月",
    "十二月",
  ];
  if ("#calendar .vanilla-calendar-week > b") {
    let allWeek = document.querySelectorAll(".vanilla-calendar-week > b");
    for (var i = 0; i < allWeek.length; i++) {
      allWeek[i].innerText = week[i];
    }
  }
  if (
    "#calendar .vanilla-calendar-header .vanilla-calendar-header__content .vanilla-calendar-month"
  ) {
    let title = document.querySelector(
      "#calendar .vanilla-calendar-header .vanilla-calendar-header__content .vanilla-calendar-month"
    );
    title.innerText =
      month[Number(title.getAttribute("data-calendar-selected-month"))];
  }
  if (
    "#calendar .vanilla-calendar-wrapper .vanilla-calendar-content .vanilla-calendar-months_selecting"
  ) {
    let allmonth = document.querySelectorAll(
      "#calendar .vanilla-calendar-wrapper .vanilla-calendar-content .vanilla-calendar-months_selecting .vanilla-calendar-months__month"
    );
    for (var i = 0; i < allmonth.length; i++) {
      allmonth[i].innerText = month[i];
    }
  }
}


// 取得所選日期資料
async function getMsg(...args) {
 
  let titleNode = document.createDocumentFragment();
  let allData = args[0];

  let calendarMsgContent = document.createElement("div");
  calendarMsgContent.className = "ipoMsgBlock";
  // 申購(subscription) 競拍(auction) 詢圈(seekingCircle) 掛牌(listing)
  // 承銷行事曆
  // 使用split方法將日期字串根據 '-' 分割成年、月、日
  let dateParts = allData[0].split("-");

  // 取得年、月、日
  let year = parseInt(dateParts[0]);
  let month = parseInt(dateParts[1]);
  mainYear = year;
  mainMonth = month;


  let data;
  let yearMonth = `${year}-${month}`;

  if (allMonthData.last === yearMonth) {
    data = lastMonthData;
  } else if (allMonthData.now === yearMonth) {
    data = thisMonthData;
  } else if (allMonthData.next === yearMonth) {
    data = nextMonth;
  } else {
    data = await fetchIpoQueryCalendData(mainYear, mainMonth);
  }

  // navbar
  let navbarElemenet = document.createElement("div");
  navbarElemenet.className = "IpoCalendarNavbar";

  let categoriesDiv = document.createElement("div");
  categoriesDiv.textContent = "分類";
  navbarElemenet.appendChild(categoriesDiv);

  let stockCodeDiv = document.createElement("div");
  stockCodeDiv.textContent = "股票代號";
  navbarElemenet.appendChild(stockCodeDiv);

  let companyAbbrDiv = document.createElement("div");
  companyAbbrDiv.textContent = "公司簡稱";
  navbarElemenet.appendChild(companyAbbrDiv);

  let listingContentDiv = document.createElement("div");
  listingContentDiv.textContent = "內容";
  navbarElemenet.appendChild(listingContentDiv);
  allData.forEach((element) => {
    let result ;

    let h3Element = document.createElement("h3");
    let dateObject = new Date(element);
    let dayOfWeek = dateObject.getDay();
    let daysOfWeek = ["(日)", "(一)", "(二)", "(三)", "(四)", "(五)", "(六)"];
    let dayOfWeekString = daysOfWeek[dayOfWeek];

    h3Element.innerText = element.replace(/-/g, "/") + " " + dayOfWeekString;
    titleNode.appendChild(h3Element);


    if(data !== Object){
      data.then((res)=>{

        result = res.find((item) => item.date === element)

        if (result) {
          titleNode.appendChild(navbarElemenet);
          // body
    
          result.allData.forEach((item) => {
            let itemElement = document.createElement("div");
            itemElement.className = "itemElement";
            if (item.data[0] === "申購") {
              itemElement.setAttribute("data-type", "subscription");
            }
            if (item.data[0] === "競拍") {
              itemElement.setAttribute("data-type", "auction");
            }
            if (item.data[0] === "詢圈") {
              itemElement.setAttribute("data-type", "seekingCircle");
            }
            if (item.data[0] === "掛牌") {
              itemElement.setAttribute("data-type", "listing");
            }
            let categoriesDiv = document.createElement("div");
            categoriesDiv.textContent = item.data[0];
            categoriesDiv.setAttribute("data-title", "分類 : ");
            itemElement.appendChild(categoriesDiv);
    
            let stockCodeDiv = document.createElement("div");
            stockCodeDiv.textContent = item.data[1];
            stockCodeDiv.setAttribute("data-title", "股票代號 : ");
            itemElement.appendChild(stockCodeDiv);
    
            let companyAbbrDiv = document.createElement("div");
            companyAbbrDiv.textContent = item.data[2];
            companyAbbrDiv.setAttribute("data-title", "公司簡稱 : ");
            itemElement.appendChild(companyAbbrDiv);
    
            let listingContentDiv = document.createElement("div");
            listingContentDiv.textContent = item.data[3];
            listingContentDiv.setAttribute("data-title", "內容 : ");
            itemElement.appendChild(listingContentDiv);
    
            calendarMsgContent.appendChild(itemElement);
          });
        } else {
          let pElement = document.createElement("h4");
          pElement.innerText = "本日無排程";
          // pElement.setAttribute("style","display: flex;height: 100%;width: 100%;justify-content: center;align-items: center;");
    
          pElement.setAttribute("style", "padding: 10px;");
    
          calendarMsgContent.appendChild(pElement);
          return;
        }
      })
    }else{
      result = data.find((item) => item.date === element)

      if (result) {
        titleNode.appendChild(navbarElemenet);
        // body
  
        result.allData.forEach((item) => {
          let itemElement = document.createElement("div");
          itemElement.className = "itemElement";
          if (item.data[0] === "申購") {
            itemElement.setAttribute("data-type", "subscription");
          }
          if (item.data[0] === "競拍") {
            itemElement.setAttribute("data-type", "auction");
          }
          if (item.data[0] === "詢圈") {
            itemElement.setAttribute("data-type", "seekingCircle");
          }
          if (item.data[0] === "掛牌") {
            itemElement.setAttribute("data-type", "listing");
          }
          let categoriesDiv = document.createElement("div");
          categoriesDiv.textContent = item.data[0];
          categoriesDiv.setAttribute("data-title", "分類 : ");
          itemElement.appendChild(categoriesDiv);
  
          let stockCodeDiv = document.createElement("div");
          stockCodeDiv.textContent = item.data[1];
          stockCodeDiv.setAttribute("data-title", "股票代號 : ");
          itemElement.appendChild(stockCodeDiv);
  
          let companyAbbrDiv = document.createElement("div");
          companyAbbrDiv.textContent = item.data[2];
          companyAbbrDiv.setAttribute("data-title", "公司簡稱 : ");
          itemElement.appendChild(companyAbbrDiv);
  
          let listingContentDiv = document.createElement("div");
          listingContentDiv.textContent = item.data[3];
          listingContentDiv.setAttribute("data-title", "內容 : ");
          itemElement.appendChild(listingContentDiv);
  
          calendarMsgContent.appendChild(itemElement);
        });
      } else {
        let pElement = document.createElement("h4");
        pElement.innerText = "本日無排程";
        // pElement.setAttribute("style","display: flex;height: 100%;width: 100%;justify-content: center;align-items: center;");
  
        pElement.setAttribute("style", "padding: 10px;");
  
        calendarMsgContent.appendChild(pElement);
        return;
      }
    }
  

    
  });
  document.querySelector("#calendarMsg").textContent = "";
  titleNode.appendChild(calendarMsgContent);
  document.querySelector("#calendarMsg").appendChild(titleNode);
}



var newDate = null;
var oldDate = "1";
var Ndata;



const options = {
  settings: {


    visibility: {
      weekend: false,
      today: true,
      theme: "light",
    },
    iso8601: false,
  },
  actions: {
    clickDay(event, dates) {
      getMsg(dates);

      DateLanguageSetting();
      let vanillaCalendarHeader = document.querySelector(
        ".vanilla-calendar-header__content .vanilla-calendar-month"
      );
      observer.observe(vanillaCalendarHeader, {
        attributes: true,
        attributeFilter: ["data-calendar-selected-month"],
      });
    },
    clickArrow(event, year, month) {
      DateLanguageSetting();
    },
    clickYear(event, year) {
      DateLanguageSetting();
    },
    getDays(day, date, HTMLElement, HTMLButtonElement) {
      let dateParts = date.split("-");
      // 取得年、月、日
      let year = parseInt(dateParts[0]);
      let month = parseInt(dateParts[1]);
      var parseDate = date.slice(0, date.lastIndexOf("-"));

      newDate = parseDate;

      if(newDate !== oldDate){
        isRecordMonth = false
      }
      if (isRecordMonth == false) {

        if(RecordMonth == 0 && allMonthData.last && allMonthData.last === `${year}-${month}`){
          Ndata = lastMonthData;
          isRecordMonth = true
          RecordMonth ++
          oldDate = date.slice(0, date.lastIndexOf("-"));
        }
        if(RecordMonth == 1 && allMonthData.now && allMonthData.now === `${year}-${month}`){
          Ndata = thisMonthData;
          isRecordMonth = true
          RecordMonth ++
          oldDate = date.slice(0, date.lastIndexOf("-"));
        }
        if(RecordMonth == 2 && allMonthData.next && allMonthData.next === `${year}-${month}`){
          Ndata = nextMonth;
          isRecordMonth = true
          RecordMonth = 0
          oldDate = date.slice(0, date.lastIndexOf("-"));
        }
      }
      // 檢查佔存是否有對應的日期數據
      if(isRecordMonth == false){

        if (newDate !== oldDate ) {
          // 將數據佔存
          Ndata = fetchIpoQueryCalendData(year, month);
          // 紀錄本次顯示資料
          RecordMonth++;
          if (RecordMonth == 1) {
            lastMonthData = Ndata;
            allMonthData[`last`] = `${year}-${month}`;

          }
          if (RecordMonth == 2) {
            thisMonthData = Ndata;
            allMonthData[`now`] = `${year}-${month}`;
            mainYear = year;
            mainMonth = month;
          }
          if (RecordMonth == 3) {
            nextMonth = Ndata;
            allMonthData[`next`] = `${year}-${month}`;
            RecordMonth = 0;
       
          }
          oldDate = date.slice(0, date.lastIndexOf("-"));
        }
      }

      Ndata.then((res) => {

        let result = res.find((item) => item.date === date);

        if (!result) return;

        HTMLButtonElement.setAttribute("class", "vanilla-calendar-day__btn");
        HTMLButtonElement.style.flexDirection = "column";
        // 申購(subscription) 競拍(auction) 詢圈(seekingCircle) 掛牌(listing)
        // 取得所有不重複的 type 屬性值
        let types = [...new Set(result.allData.map((item) => item.data[0]))];
        const typeMapping = {
          申購: "subscription",
          競拍: "auction",
          詢圈: "seekingCircle",
          掛牌: "listing",
        };
        // 將中文類型名稱替換為英文代號

        let typesInEnglish = types.map((type) => typeMapping[type]);

        // 創建外部 div 元素
        let pointElement = document.createElement("div");
        pointElement.classList.add("point");

        // 逐一創建 img 元素並設定屬性後插入到外部 div 元素中
        typesInEnglish.forEach((type) => {
          if (`${type}` !== "undefined") {
            let img = document.createElement("img");
            img.src = `./img/${type}.png`; // 假設圖片位於 imgs 資料夾下，副檔名為 .png
            img.alt = "";
            pointElement.appendChild(img);
          }
        });
        let NewItem = document.createElement("span");
        NewItem.className = "NewItem";
        NewItem.innerText = `${day}`;
        NewItem.append(pointElement);
        HTMLButtonElement.innerText = "";
        HTMLButtonElement.append(NewItem);
      });
    },
  },
};
const calendar = new VanillaCalendar("#calendar", options);

calendar.init();
DateLanguageSetting();

let mainCalender = document.querySelector("#calendar");
let vanillaCalendarHeader = document.querySelector(
  ".vanilla-calendar-header__content .vanilla-calendar-month"
);
const observer = new MutationObserver(() => {
  DateLanguageSetting();
});
observer.observe(mainCalender, {
  attributes: true,
  attributeFilter: ["class"],
});
observer.observe(vanillaCalendarHeader, {
  attributes: true,
  attributeFilter: ["data-calendar-selected-month"],
});
try {
  document
    .querySelector('[data-specifypage="home"] .vanilla-calendar-day__btn_today')
    .click();
} catch (error) {}
function calendarReset() {
  calendar.reset();
  try {
    document
      .querySelector(
        '[data-specifypage="home"] .vanilla-calendar-day__btn_today'
      )
      .click();
  } catch (error) {
    document.querySelector("#calendarMsg").innerHTML = "";
  }
}
